
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:06PM

See merge request itentialopensource/adapters/adapter-ciena_mcp!15

---

## 0.5.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-ciena_mcp!13

---

## 0.5.2 [08-15-2024]

* Changes made at 2024.08.14_18:14PM

See merge request itentialopensource/adapters/adapter-ciena_mcp!12

---

## 0.5.1 [08-06-2024]

* Changes made at 2024.08.06_19:27PM

See merge request itentialopensource/adapters/adapter-ciena_mcp!11

---

## 0.5.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!10

---

## 0.4.6 [03-28-2024]

* Changes made at 2024.03.28_13:36PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!9

---

## 0.4.5 [03-21-2024]

* Changes made at 2024.03.21_14:05PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!8

---

## 0.4.4 [03-11-2024]

* Changes made at 2024.03.11_15:55PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!7

---

## 0.4.3 [02-28-2024]

* Changes made at 2024.02.28_11:23AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!6

---

## 0.4.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!5

---

## 0.4.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!4

---

## 0.4.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!3

---

## 0.3.3 [07-27-2022]

* Change schemas to escape . so that objectization is not done

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!2

---

## 0.3.2 [06-03-2022]

* Fix Datatype on token

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!1

---

## 0.3.1 [05-26-2022]

* Bug fixes and performance improvements

See commit 64ba74c

---
