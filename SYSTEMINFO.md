# Ciena MCP

Vendor: Ciena
Homepage: https://www.ciena.com/

Product: Manage, Control and Plan (MPC)
Product Page: https://www.ciena.com/products/navigator-ncs

## Introduction
We classify Ciena MCP into the Data Center domain as Ciena MCP provides the capability to get information about and make changes to infrastructure.

## Why Integrate
The Ciena MCP adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Ciena MCP. With this adapter you have the ability to perform operations such as:

- Automate service & network provisioning, compliance, and service assurance and fulfillment.

## Additional Product Documentation
The [OAuth API documents for Ciena MCP](https://my.ciena.com/CienaPortal/s/article/MCP-OAuth-20-Token-on-MCP-API)