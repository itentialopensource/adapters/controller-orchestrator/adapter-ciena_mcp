# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the CienaMcp System. The API that was used to build the adapter for CienaMcp is usually available in the report directory of this adapter. The adapter utilizes the CienaMcp API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Ciena MCP adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Ciena MCP. With this adapter you have the ability to perform operations such as:

- Automate service & network provisioning, compliance, and service assurance and fulfillment.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
