## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Ciena MCP. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Ciena MCP.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Ciena MCP. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getTotalRecordsCount(callback)</td>
    <td style="padding:15px">Get the total number of Alarm records in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/alarmRecordsCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypes(callback)</td>
    <td style="padding:15px">Get a collection of Alarmed device types.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/device-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttributes(id, callback)</td>
    <td style="padding:15px">Get the device specific attributes for a device session id.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/deviceAttributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeOldHistoricalAlarms(retentionDate, callback)</td>
    <td style="padding:15px">Remove historical alarms of the day specified in yyyymmdd format.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/historicalAlarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoricalChangePartition(callback)</td>
    <td style="padding:15px">Get configuration of if historical alarms change partition.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/historicalChangePartition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoricalDataRetentionDays(callback)</td>
    <td style="padding:15px">Get configuration of historical data retention days.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/historicalDataRetentionDays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setHistoricalDataRetentionDays(retentionDays, callback)</td>
    <td style="padding:15px">Set configuration of historical data retention days.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/historicalDataRetentionDays/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acknowledgeAlarmV1(id, callback)</td>
    <td style="padding:15px">Acknowledge an active Alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/{pathv1}/acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceClearAlarmV1(id, callback)</td>
    <td style="padding:15px">Force clear an active Alarm based on its identifier (irrespective of its clearable flag).</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/{pathv1}/force-clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualClearAlarmV1(id, callback)</td>
    <td style="padding:15px">Manually clear an active Alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/{pathv1}/manual-clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unacknowledgeAlarmV1(id, callback)</td>
    <td style="padding:15px">Unacknowledge an active Alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/{pathv1}/unacknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlarmCounts(callback)</td>
    <td style="padding:15px">Query active alarm counts of severity CRITICAL, MAJOR, MINOR and WARNING.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/filter/activeAlarmCountsBasic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchActiveAlarms(severity, serviceAffecting, deviceType, acknowledgeState, keytext, range, sorting, offset, pageSize, callback)</td>
    <td style="padding:15px">Search/Filter active Alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/filter/activeAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchHistoricalAlarms(severity, serviceAffecting, deviceType, keytext, range, sorting, offset, pageSize, callback)</td>
    <td style="padding:15px">Search/Filter historical cleared Alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v1/alarms/filter/historicalAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSyncStates(body, callback)</td>
    <td style="padding:15px">Get a collection of alarm sync states of given nodes</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/alarm-sync-states?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetRequestIdsForServerId(serverId, callback)</td>
    <td style="padding:15px">Reset request IDs for all alarms/events for this server ID to zero</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/resetRequestId/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acknowledgeAlarm(id, sequenceId, callback)</td>
    <td style="padding:15px">Acknowledge an active alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/{pathv1}/acknowledge/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">annotateAlarm(id, body, callback)</td>
    <td style="padding:15px">Annotate an active alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/{pathv1}/annotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceClearAlarm(id, sequenceId, callback)</td>
    <td style="padding:15px">Force clear an active alarm based on its identifier (irrespective of its clearable flag).</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/{pathv1}/force-clear/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualClearAlarm(id, sequenceId, callback)</td>
    <td style="padding:15px">Manually clear an active alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/{pathv1}/manual-clear/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAnnotation(id, callback)</td>
    <td style="padding:15px">Remove Annotation of an active alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/{pathv1}/removeAnnotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unacknowledgeAlarm(id, sequenceId, callback)</td>
    <td style="padding:15px">Unacknowledge an active alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/{pathv1}/unacknowledge/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFilters(callback)</td>
    <td style="padding:15px">Get all alarm filters active on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFilter(body, callback)</td>
    <td style="padding:15px">Create a unique Alarm filter.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsaApiV20AlarmsFilterActiveAlarmCounts(callback)</td>
    <td style="padding:15px">Query active alarm counts of severity CRITICAL, MAJOR, MINOR and WARNING.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/activeAlarmCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchActiveAlarmsV2(filterSeverity, filterServiceAffecting, filterDeviceType, filterAcknowledgeState, filterDeviceId, filterDeviceName, filterIpAddress, filterMacAddress, filterAdditionalText, filterAnnotation, filterNativeConditionType, filterResource, filterSubnetName, filterCardType, filterClfi, filterFic, filterKeytext, filterLastRaisedTime, sorting, offset, pageSize, callback)</td>
    <td style="padding:15px">Search/Filter active Alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/activeAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdditionalTexts(filterAdditionalText, selectedAdditionalText, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for additional texts in context. Returns a subset of the total additional texts in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/additionalTextAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceNames(filterDeviceName, selectedDeviceName, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for device names in context. Returns a subset of the total device names in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/deviceNamesAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchFilteredAlarms(filterAlarmId, filterState, filterContextState, filterSeverity, filterServiceAffecting, filterDeviceType, filterAcknowledgeState, filterDeviceId, filterDeviceName, filterIpAddress, filterMacAddress, filterDeviceTag, filterAdditionalText, filterAnnotation, filterNativeConditionType, filterResource, filterResourceList, filterResourceId, filterSubnetName, filterCardType, filterClfi, filterFic, filterPartition, filterKeytext, filterLastRaisedTime, filterLastRaisedTimeFrom, filterLastRaisedTimeTo, filterClearTime, filterClearTimeFrom, filterClearTimeTo, sort, filterRefinedRaisedTimeFrom, filterRefinedRaisedTimeTo, filterRefinedClearTimeFrom, filterRefinedClearTimeTo, filterAllTime, offset, pageSize, callback)</td>
    <td style="padding:15px">Search/Filter all Alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/filteredAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchFilteredAlarmsWithBody(body, callback)</td>
    <td style="padding:15px">Search/Filter all Alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/filteredAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredAlarm(id, callback)</td>
    <td style="padding:15px">Get an Alarm based on its identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/filteredAlarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchCorrelatedAlarms(serviceId, filterSeverity, filterServiceAffecting, filterDeviceType, filterAcknowledgeState, filterDeviceId, filterDeviceName, filterIpAddress, filterMacAddress, filterDeviceTag, filterAdditionalText, filterAnnotation, filterNativeConditionType, filterResource, filterResourceList, filterResourceId, filterSubnetName, filterCardType, filterClfi, filterFic, filterPartition, filterKeytext, filterLastRaisedTime, filterLastRaisedTimeFrom, filterLastRaisedTimeTo, filterClearTime, filterClearTimeFrom, filterClearTimeTo, sort, filterRefinedRaisedTimeFrom, filterRefinedRaisedTimeTo, filterRefinedClearTimeFrom, filterRefinedClearTimeTo, offset, pageSize, callback)</td>
    <td style="padding:15px">Search/Filter all correlated alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/getCorrelatedAlarmsByService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchHistoricalAlarmsV2(filterSeverity, filterServiceAffecting, filterDeviceType, filterDeviceId, filterDeviceName, filterIpAddress, filterMacAddress, filterAdditionalText, filterNativeConditionType, filterResource, filterSubnetName, filterCardType, filterClfi, filterFic, filterKeytext, filterClearTime, sorting, offset, pageSize, callback)</td>
    <td style="padding:15px">Search/Filter historical cleared Alarms on the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/historicalAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpAddresses(filterIpAddress, selectedIpAddress, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for IP addresses in context. Returns a subset of the total IP addresses in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/ipAddressesAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacAddresses(filterMacAddress, selectedMacAddress, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for MAC addresses in context. Returns a subset of the total MAC addresses in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/macAddressesAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNativeConditionTypes(filterNativeConditionType, selectedNativeConditionType, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for native condition types in context. Returns a subset of the total native condition types i</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/nativeConditionTypeAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResources(filterResource, selectedResource, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for resources in context. Returns a subset of the total resources in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/resourceAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetNames(filterSubnetName, selectedSubnetName, filterContextState, filterKeytext, filterLastRaisedTime, filterClearTime, callback)</td>
    <td style="padding:15px">Query for subnet names in context. Returns a subset of the total subnet names in the system.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/subnetNamesAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFilter(channel, callback)</td>
    <td style="padding:15px">Manually delete an alarm filter from the system by filter channel.</td>
    <td style="padding:15px">{base_path}/{version}/nsa/api/v2_0/alarms/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectedInventoryIds(alarmId, callback)</td>
    <td style="padding:15px">Get the inventory ids affected by an active alarm</td>
    <td style="padding:15px">{base_path}/{version}/nsa-correlation/api/v1/correlation/getAffectedInventoryIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectingAlarmIds(inventoryId, callback)</td>
    <td style="padding:15px">Get the active alarm ids affecting an inventory</td>
    <td style="padding:15px">{base_path}/{version}/nsa-correlation/api/v1/correlation/getAffectingAlarmIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsaCorrelationApiV1CorrelationGetAffectingAlarmIds(body, callback)</td>
    <td style="padding:15px">Get the active alarm ids affecting a list of inventory</td>
    <td style="padding:15px">{base_path}/{version}/nsa-correlation/api/v1/correlation/getAffectingAlarmIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectingAlarms(inventoryId, callback)</td>
    <td style="padding:15px">Get the active alarms affecting an inventory</td>
    <td style="padding:15px">{base_path}/{version}/nsa-correlation/api/v1/correlation/getAffectingAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsaCorrelationApiV1CorrelationGetAffectingAlarms(body, callback)</td>
    <td style="padding:15px">Get the active alarms affecting a list of inventory</td>
    <td style="padding:15px">{base_path}/{version}/nsa-correlation/api/v1/correlation/getAffectingAlarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ping(callback)</td>
    <td style="padding:15px">Check if the inventory correlation service is up</td>
    <td style="padding:15px">{base_path}/{version}/nsa-correlation/api/v1/correlation/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentUser(callback)</td>
    <td style="padding:15px">Retrieve current flattened user info</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/admin/current-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentSessionData(callback)</td>
    <td style="padding:15px">Retrieve current account info</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/admin/current-account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentPermissions(callback)</td>
    <td style="padding:15px">Retrieve current permissions based on account and geored status</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/admin/current-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacEApiV1AdminResources(callback)</td>
    <td style="padding:15px">Retrieves a list of all cached resources, associated with their roles.</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/admin/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessions(callback)</td>
    <td style="padding:15px">Retrieve all sessions currently in the cache</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/admin/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearUserCache(callback)</td>
    <td style="padding:15px">Deletes the internal user cache.</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/admin/usercache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureAudit(enable, callback)</td>
    <td style="padding:15px">Configure the audit feature</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/audit/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateRestResource(body, callback)</td>
    <td style="padding:15px">Checks resource access permission.</td>
    <td style="padding:15px">{base_path}/{version}/rbac-e/api/v1/rest/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFile(areaName, pathParam, commitHash, callback)</td>
    <td style="padding:15px">Get a specific file specified by a path managed by an area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetFile(areaName, pathParam, commitHash, callback)</td>
    <td style="padding:15px">Get a specific file specified by a path managed by an area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAreas(callback)</td>
    <td style="padding:15px">List all of the areas in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListAreas(callback)</td>
    <td style="padding:15px">List all of the areas in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countAreas(limit, callback)</td>
    <td style="padding:15px">Count the areas in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountAreas(limit, callback)</td>
    <td style="padding:15px">Count the areas in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRecentChanges(areaName, callback)</td>
    <td style="padding:15px">List all of the recent changes to an area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListRecentChanges(areaName, callback)</td>
    <td style="padding:15px">List all of the recent changes to an area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArea(areaName, fields, callback)</td>
    <td style="padding:15px">Get information for a specific area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetArea(areaName, fields, callback)</td>
    <td style="padding:15px">Get information for a specific area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPullRequests(areaName, pendingOnly, q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the pull requests for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListPullRequests(areaName, pendingOnly, q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the pull requests for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPullRequest(areaName, submission, callback)</td>
    <td style="padding:15px">Submit a pull request to pull a specified branch into a specified area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countPullRequests(areaName, pendingOnly, q, limit, callback)</td>
    <td style="padding:15px">Count of the pull requests for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountPullRequests(areaName, pendingOnly, q, limit, callback)</td>
    <td style="padding:15px">Count of the pull requests for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullRequest(areaName, requestId, callback)</td>
    <td style="padding:15px">Get a specific pull request for an area based on the request identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetPullRequest(areaName, requestId, callback)</td>
    <td style="padding:15px">Get a specific pull request for an area based on the request identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/pullrequests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileBytes(areaName, pathParam, commitHash, callback)</td>
    <td style="padding:15px">Get the bytes on disk of a specific file by path managed by an area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/raw-files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetFileBytes(areaName, pathParam, commitHash, callback)</td>
    <td style="padding:15px">Get the bytes on disk of a specific file by path managed by an area in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/raw-files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAreaUpgrades(areaName, pendingOnly, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List the upgrade history for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListAreaUpgrades(areaName, pendingOnly, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List the upgrade history for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAreaUpgrade(areaName, sourceDir, callback)</td>
    <td style="padding:15px">Submit a request to upgrade an area to the currently installed release version</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countAreaUpgrades(areaName, pendingOnly, q, limit, callback)</td>
    <td style="padding:15px">Count the upgrade history events for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountAreaUpgrades(areaName, pendingOnly, q, limit, callback)</td>
    <td style="padding:15px">Count the upgrade history events for an area</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAreaUpgrade(areaName, requestId, callback)</td>
    <td style="padding:15px">Get a specific upgrade request for an area based on the request identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetAreaUpgrade(areaName, requestId, callback)</td>
    <td style="padding:15px">Get a specific upgrade request for an area based on the request identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/upgrades/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFiles(areaName, commitHash, callback)</td>
    <td style="padding:15px">Get the list of files managed by an area in the AssetManager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListFiles(areaName, commitHash, callback)</td>
    <td style="padding:15px">Get the list of files managed by an area in the AssetManager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/areas/{pathv1}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listKeys(offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the SSH public keys authorized for git access in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListKeys(offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the SSH public keys authorized for git access in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createKey(key, callback)</td>
    <td style="padding:15px">Add an SSH public key to the list of the SSH public keys authorized for git access in the Asset Man</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countKeys(limit, callback)</td>
    <td style="padding:15px">List the SSH public keys authorized for git access in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/keys/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountKeys(limit, callback)</td>
    <td style="padding:15px">List the SSH public keys authorized for git access in the Asset Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/keys/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKey(keyId, callback)</td>
    <td style="padding:15px">Remove an SSH public key from the list of the SSH public keys authorized for git access in the Asse</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPing(callback)</td>
    <td style="padding:15px">Ping the system to see whether the component is ready to handle requests or not</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetPing(callback)</td>
    <td style="padding:15px">Ping the system to see whether the component is ready to handle requests or not</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/asset-manager/api/v1/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBpocorePoliciesApiV1Ping(callback)</td>
    <td style="padding:15px">Ping the system to see whether the component is ready to handle requests or not</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headBpocorePoliciesApiV1Ping(callback)</td>
    <td style="padding:15px">Ping the system to see whether the component is ready to handle requests or not</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLekhaApiV1(from, to, lastDays, callback)</td>
    <td style="padding:15px">Exports audit logs based on query params.</td>
    <td style="padding:15px">{base_path}/{version}/lekha/api/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLekhaApiV1(from, to, lastDays, callback)</td>
    <td style="padding:15px">Delete locally stored audit logs based on query params.</td>
    <td style="padding:15px">{base_path}/{version}/lekha/api/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditProfiles(name, testType, typeGroups, auditType, include, callback)</td>
    <td style="padding:15px">Retrieves a list of auditProfiles provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/auditProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditProfileById(auditProfileId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific auditProfiles</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/auditProfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuditTarget(body, callback)</td>
    <td style="padding:15px">Creates an auditTarget</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/auditTargets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditTargetById(auditTargetId, callback)</td>
    <td style="padding:15px">Retrieves a specific auditTarget</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/auditTargets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAudits(auditType, testType, auditName, auditProfileNames, auditRunAuditStateState, include, callback)</td>
    <td style="padding:15px">Retrieves a list of audits provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAudit(body, callback)</td>
    <td style="padding:15px">Creates an audit</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditById(auditId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific audit</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAudit(auditId, callback)</td>
    <td style="padding:15px">Deletes a specific audit</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuditRun(auditId, callback)</td>
    <td style="padding:15px">Creates an audit Run</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}/auditRuns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuditRun(auditId, auditRunId, callback)</td>
    <td style="padding:15px">Deletes a specific auditRun</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}/auditRuns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAuditRun(auditId, auditRunId, body, callback)</td>
    <td style="padding:15px">Patches an audit Run</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}/auditRuns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditFindings(auditId, auditRunId, profileName, testType, fixStateState, include, limit, callback)</td>
    <td style="padding:15px">Retrieves List of Audit Finding for a specific auditRun</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}/auditRuns/{pathv2}/auditFindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fixAuditFindings(auditId, auditRunId, body, callback)</td>
    <td style="padding:15px">Patches an audit findings with a fixState under the given auditRun</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}/auditRuns/{pathv2}/auditFindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditFindingById(auditId, auditRunId, auditFindingId, include, callback)</td>
    <td style="padding:15px">Retrieves auditFinding for a specific audit and a specific auditRun</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/audits/{pathv1}/auditRuns/{pathv2}/auditFindings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestTypes(auditType, callback)</td>
    <td style="padding:15px">Return list of testTypes</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/testTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestTypeById(testTypeId, callback)</td>
    <td style="padding:15px">Retrieves a list of testType provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/audit/api/v1/testTypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackupJobs(callback)</td>
    <td style="padding:15px">List details of all backup processes</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupSolution(body, callback)</td>
    <td style="padding:15px">Trigger Backup Process</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupJob(jobId, callback)</td>
    <td style="padding:15px">List details of a particular backup process</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRestoreJobs(callback)</td>
    <td style="padding:15px">List details of all restore processes</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreSolution(body, callback)</td>
    <td style="padding:15px">Trigger Restore Process</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestoreJob(jobId, callback)</td>
    <td style="padding:15px">List details of a particular restore process</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestoreJob(jobId, callback)</td>
    <td style="padding:15px">Delete a completed restore job</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnapshots(solutionName, callback)</td>
    <td style="padding:15px">List snapshots</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v1/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackupSettings(callback)</td>
    <td style="padding:15px">List backup settings</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/backupSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBackupSettings(id, body, callback)</td>
    <td style="padding:15px">Update backup settings</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/backupSettings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackupDetails(scheduleName, status, scheduleNameContains, ordering, offset, limit, callback)</td>
    <td style="padding:15px">List details of all backup schedules</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackupSchedules(name, state, nameContains, ordering, callback)</td>
    <td style="padding:15px">List all backup schedules</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBackupserviceApiV2Schedules(body, callback)</td>
    <td style="padding:15px">Create a backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupSchedule(id, callback)</td>
    <td style="padding:15px">List a particular backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBackupSchedule(id, callback)</td>
    <td style="padding:15px">Delete a particular backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBackupSchedule(id, body, callback)</td>
    <td style="padding:15px">Update a particular active backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupDetails(id, status, ordering, offset, limit, callback)</td>
    <td style="padding:15px">List details of a particular backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/backupservice/api/v2/schedules/{pathv1}/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Scripts(projectname, devicetype, nodename, shelfId, format, callback)</td>
    <td style="padding:15px">Get commissioning scripts</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/scripts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV1Scripts(projectname, callback)</td>
    <td style="padding:15px">Delete commissioning scripts</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/scripts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1Scripts(projectname, policyid, callback)</td>
    <td style="padding:15px">Generate and return commissioning scripts</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/scripts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1ScriptsSummary(projectname, callback)</td>
    <td style="padding:15px">Get commissioning scripts summary</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/scriptsSummary/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1Equipment(newEquipment, callback)</td>
    <td style="padding:15px">Commission topologies of an equipment</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/equipment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Policy(id, callback)</td>
    <td style="padding:15px">Get policy details</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV1Policy(id, callback)</td>
    <td style="padding:15px">Delete policy details</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1Policy(newPolicy, callback)</td>
    <td style="padding:15px">Create a policy</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCommissioningApiV1Policy(id, policy, callback)</td>
    <td style="padding:15px">Update a policy</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1PolicyTypes(callback)</td>
    <td style="padding:15px">Get all policy types</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policyTypes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1PolicyDefaults(id, type, callback)</td>
    <td style="padding:15px">Get defaults of a policy</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policyDefaults/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV1PolicyDefaults(id, callback)</td>
    <td style="padding:15px">Reset defaults of a policy</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policyDefaults/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1PolicyDefaults(id, defaults, callback)</td>
    <td style="padding:15px">Set custom defaults for a policy</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policyDefaults/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCommissioningApiV1PolicyDefaults(id, defaults, callback)</td>
    <td style="padding:15px">Set custom defaults for a policy</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/policyDefaults/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV2Ipsubnet(callback)</td>
    <td style="padding:15px">Get IP Subnet details</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v2/ipsubnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV2Ipsubnet(data, callback)</td>
    <td style="padding:15px">Delete IP Subnet OspfArea Address</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v2/ipsubnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV2Ipsubnet(iPAddressSubnet, callback)</td>
    <td style="padding:15px">Allocate IP Subnet and ospfArea Addresses</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v2/ipsubnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Ipsubnet(callback)</td>
    <td style="padding:15px">Get IPv4 Subnet details</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/ipsubnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV1Ipsubnet(data, callback)</td>
    <td style="padding:15px">Delete IPv4 OspfArea Address</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/ipsubnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1Ipsubnet(ipsubnet, callback)</td>
    <td style="padding:15px">Allocate IPv4 shelf subnet and ospfArea Addresses</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/ipsubnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV2ReservedIP(tid, callback)</td>
    <td style="padding:15px">Get Reserved IPAddress data of Interface (shelves , crafts)</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v2/reservedIP/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV2ReservedIP(data, callback)</td>
    <td style="padding:15px">Reserved IPAddress Management</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v2/reservedIP/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV2ReservedIP(reservedIP, callback)</td>
    <td style="padding:15px">Reserved IPAddress of the Interface (shelf or craft) data</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v2/reservedIP/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1DeployedIPv4Data(tid, callback)</td>
    <td style="padding:15px">Get deployed shelves IPv4 data</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/deployedIPv4Data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningApiV1DeployedIPv4Data(data, callback)</td>
    <td style="padding:15px">Delete deployed shelf IPv4 data</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/deployedIPv4Data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1DeployedIPv4Data(deployedNetwork, callback)</td>
    <td style="padding:15px">Load deployed shelves IPv4 data</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/deployedIPv4Data/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Ipaddress(tid, subnet, callback)</td>
    <td style="padding:15px">Get IPAddress under a given subnet or tid</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/ipaddress/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Services(callback)</td>
    <td style="padding:15px">Get provisioned services state</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1Services(sessionid, nodename, deviceType, service, callback)</td>
    <td style="padding:15px">Trigger service provisioning</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Uap65001(callback)</td>
    <td style="padding:15px">Determine User Access Level 1 Privilege</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/uap-6500-1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Uap65002(callback)</td>
    <td style="padding:15px">Determine User Access Level 2 Privilege</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/uap-6500-2/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Uap65003(callback)</td>
    <td style="padding:15px">Determine User Access Level 3 Privilege</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/uap-6500-3/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Uap65004(callback)</td>
    <td style="padding:15px">Determine User Access Level 4 Privilege</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/uap-6500-4/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1Uap65005(callback)</td>
    <td style="padding:15px">Determine User Access Level 5 Privilege</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/uap-6500-5/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningApiV1DeploymentData(projectname, callback)</td>
    <td style="padding:15px">Get the deployment data of the planning project</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/deploymentData/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommissioningApiV1DeploymentData(projectname, body, callback)</td>
    <td style="padding:15px">Update the deployment data of the planning project.</td>
    <td style="padding:15px">{base_path}/{version}/commissioning/api/v1/deploymentData/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConfigurations(callback)</td>
    <td style="padding:15px">Get all existing configuration values</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/configResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putConfiguration(body, callback)</td>
    <td style="padding:15px">Updates and add configuration name and value</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/configResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguration(propertyName, callback)</td>
    <td style="padding:15px">Gets the value for the given configName</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/configResource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ConfigResource(callback)</td>
    <td style="padding:15px">Get all existing configuration values</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/configResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV1ConfigResource(callback)</td>
    <td style="padding:15px">Updates and add configuration name and value</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/configResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ConfigResourcePropertyName(propertyName, callback)</td>
    <td style="padding:15px">Gets the value for the given configName</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/configResource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectedServices(alarmId, callback)</td>
    <td style="padding:15px">Search affected services for an alarm</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/affectedServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectedServicesCounts(alarmIds, callback)</td>
    <td style="padding:15px">Get number of affected services for alarms</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/affectedServicesCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAffectedServicesCounts(body, callback)</td>
    <td style="padding:15px">Get number of affected services for alarms</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/affectedServicesCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectedServicesByResource(resourceId, callback)</td>
    <td style="padding:15px">Search affected services for a resource</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/affectedServicesByResource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectedServicesCountsByResources(resourceIds, callback)</td>
    <td style="padding:15px">Get number of affected services for resources</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/affectedServicesCountsByResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCorrelatedAlarms(serviceId, limit, callback)</td>
    <td style="padding:15px">Search correlated alarms for a service</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/correlatedAlarmsForService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeBulkActionsNumber(body, callback)</td>
    <td style="padding:15px">Change the bulk actions number for bulkprocessor to ES</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/debug/bulkload/bulkActionsNumber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeBulkLoadWorkers(body, callback)</td>
    <td style="padding:15px">Change the bulk load workers for bulkprocessor to ES</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/debug/bulkload/bulkLoadWorkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeFlushInterval(body, callback)</td>
    <td style="padding:15px">Change the flushInterval in seconds of bulkprocessor to ES</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/debug/bulkload/flushInterval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogLevel(callback)</td>
    <td style="padding:15px">Retrieves the currently active log level</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/debug/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeLogLevel(body, callback)</td>
    <td style="padding:15px">Changes the log level</td>
    <td style="padding:15px">{base_path}/{version}/events-to-service-correlator/api/v1/debug/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSftpDebugLogs(callback)</td>
    <td style="padding:15px">Retrieves the currently active log level</td>
    <td style="padding:15px">{base_path}/{version}/sftp/debug/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSftpDebugLogs(body, callback)</td>
    <td style="padding:15px">Changes the log level</td>
    <td style="padding:15px">{base_path}/{version}/sftp/debug/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createValue(body, callback)</td>
    <td style="padding:15px">Add credentials for an IP address.</td>
    <td style="padding:15px">{base_path}/{version}/dct/api/v1/bpi/credentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipment(id, networkConstructId, equipmentExpectationsEquipmentIntentId, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipment(body, callback)</td>
    <td style="padding:15px">Creates or updates an equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExpectation(eqpId, body, callback)</td>
    <td style="padding:15px">Create an Equipment expectation given the Equipment id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentById(equipmentId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipment(equipmentId, equipment, callback)</td>
    <td style="padding:15px">Update a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmentById(equipmentId, callback)</td>
    <td style="padding:15px">Deletes a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentExpectations(equipmentId, callback)</td>
    <td style="padding:15px">Retrieves the expectations of the equipment.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/equipmentExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentExpectationById(equipmentId, equipmentExpectationId, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/equipmentExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmentExpectationById(equipmentId, equipmentExpectationId, callback)</td>
    <td style="padding:15px">Delete a specific equipment expectation.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/equipmentExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentPlannedById(equipmentId, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment planned</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/equipmentPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmentPlanned(equipmentId, equipment, callback)</td>
    <td style="padding:15px">Update a specific planned equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/equipmentPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPlannedEquipment(equipmentId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/equipmentPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEquipmentExpectation(equipmentId, equipmentExpectationId, body, callback)</td>
    <td style="padding:15px">PATCH a specific equipment expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMaintenanceMode(equipmentId, body, callback)</td>
    <td style="padding:15px">Creates or updates MaintenanceMode for a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/maintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmentUserData(equipmentId, userDataKey, userDataValue, callback)</td>
    <td style="padding:15px">Create or update an equipment's userData</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmentUserData(equipmentId, userDataKey, callback)</td>
    <td style="padding:15px">Delete a userData from a given equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipment/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentEquipmentIdEquipmentExpectations(equipmentId, callback)</td>
    <td style="padding:15px">Retrieves the expectations of the equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipment/{pathv1}/equipmentExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentEquipmentIdEquipmentExpectationsEquipmentExpectationId(equipmentId, equipmentExpectationId, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipment/{pathv1}/equipmentExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV20EquipmentEquipmentIdUserDataUserDataKey(equipmentId, userDataKey, userDataValue, callback)</td>
    <td style="padding:15px">Create or update an equipment's userData</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipment/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV20EquipmentEquipmentIdUserDataUserDataKey(equipmentId, userDataKey, callback)</td>
    <td style="padding:15px">Delete a userData from a given equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipment/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3Equipment(ncId, networkConstructId, id, state, shelf, slot, subSlot, subShelf, equipmentExpectationsEquipmentIntentId, siteId, searchText, searchFields, include, offset, limit, fields, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3Equipment(body, callback)</td>
    <td style="padding:15px">Creates or updates an equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3EquipmentEqpIdExpectations(eqpId, body, callback)</td>
    <td style="padding:15px">Create an Equipment expectation given the Equipment id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3EquipmentEquipmentId(equipmentId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3EquipmentEquipmentId(equipmentId, equipment, callback)</td>
    <td style="padding:15px">Create or Update a specific equipment with planned attributes, userData, expectations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3EquipmentEquipmentId(equipmentId, callback)</td>
    <td style="padding:15px">Deletes a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3EquipmentEquipmentIdEquipmentExpectationsEquipmentExpectationId(equipmentId, equipmentExpectationId, callback)</td>
    <td style="padding:15px">Delete a specific equipment expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/equipmentExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3EquipmentEquipmentIdEquipmentPlanned(equipmentId, callback)</td>
    <td style="padding:15px">Retrieves  the planned attributes of the given equipment.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/equipmentPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3EquipmentEquipmentIdEquipmentPlanned(equipmentId, equipment, callback)</td>
    <td style="padding:15px">Update a specific planned equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/equipmentPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3EquipmentEquipmentIdEquipmentPlanned(equipmentId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/equipmentPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3EquipmentEquipmentIdExpectationsEquipmentExpectationId(equipmentId, equipmentExpectationId, body, callback)</td>
    <td style="padding:15px">PATCH a specific equipment expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3EquipmentEquipmentIdMaintenanceMode(equipmentId, body, callback)</td>
    <td style="padding:15px">Creates or updates MaintenanceMode for a specific equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipment/{pathv1}/maintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV4Equipment(searchText, searchFields, resourceState, networkConstructName, networkConstructId, equipmentExpectationsEquipmentIntentId, siteId, state, shelf, slot, subSlot, subShelf, availabilityState, reservationState, maintenanceMode, cardType, specificationMismatch, category, neContactState, networkConstructMacAddress, fields, sortBy, offset, limit, metaDataFields, include, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV4Equipment(body, callback)</td>
    <td style="padding:15px">Creates an equipment</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentV5(id, networkConstructId, equipmentExpectationsEquipmentIntentId, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentV6(id, networkConstructId, equipmentExpectationsEquipmentIntentId, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1AutoSlatDataProjectId(projectId, callback)</td>
    <td style="padding:15px">Generate the Auto-SLAT data using project id</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/AutoSlatData/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1AutoSlatDataNameProjectName(projectName, callback)</td>
    <td style="padding:15px">Generate the Auto-SLAT data using project name</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/AutoSlatData/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1EquipmentKitNameMappings(callback)</td>
    <td style="padding:15px">Gets all equipment kit name mappings</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/EquipmentKitNameMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1EquipmentKitNameMappings(body, callback)</td>
    <td style="padding:15px">Creates a new equipment kit name mapping</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/EquipmentKitNameMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEqptKitNameMappingByKitName(kitName, callback)</td>
    <td style="padding:15px">Get the equipment kit name mapping for a given kitName</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/EquipmentKitNameMappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1EquipmentKitNameMappingsKitName(kitName, body, callback)</td>
    <td style="padding:15px">Updates or creates kit name mapping</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/EquipmentKitNameMappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmenttopologyplanningApiV1EquipmentKitNameMappingsKitName(kitName, callback)</td>
    <td style="padding:15px">Deletes equipment kit name mapping for a given kitName</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/EquipmentKitNameMappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1Equipment(body, callback)</td>
    <td style="padding:15px">Adds equipment to a node.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchEquipment(id, equipmentName, searchText, searchFields, searchType = 'wildCard', resourceState, networkConstructName, physicalNeName, networkConstructId, equippedUtilizationTotalCapacity, equippedUtilizationUsedCapacity, equippedUtilizationPercent, equippedStartUtilizationPercent, equippedEndUtilizationPercent, inUseByServiceUtilizationTotalCapacity, inUseByServiceUtilizationUsedCapacity, inUseByServiceUtilizationPercent, inUseByServiceStartUtilizationPercent, inUseByServiceEndUtilizationPercent, servicesUtilizationServiceTotal, servicesUtilizationInfrastructureTotal, equipmentExpectationsEquipmentIntentId, siteId, state, displayState, shelf, slot, subSlot, subsubSlot, subShelf, availabilityState, displayAvailabilityState, reservationState, maintenanceMode, cardType, provisionedSpecPartNumber, specificationMismatch, category, neContactState, networkConstructMacAddress, networkConstructSubnetName, projectName, startDateMin, startDateMax, endDateMin, endDateMax, srlg, secondaryState, serial, customer, namedQuery, namedQueryParam1, fields, sortBy, offset, limit, metaDataFields, metaLimit, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchEquipmentV1(id, equipmentName, searchText, searchFields, searchType = 'wildCard', resourceState, networkConstructName, physicalNeName, networkConstructId, equippedUtilizationTotalCapacity, equippedUtilizationUsedCapacity, equippedUtilizationPercent, equippedStartUtilizationPercent, equippedEndUtilizationPercent, inUseByServiceUtilizationTotalCapacity, inUseByServiceUtilizationUsedCapacity, inUseByServiceUtilizationPercent, inUseByServiceStartUtilizationPercent, inUseByServiceEndUtilizationPercent, servicesUtilizationServiceTotal, servicesUtilizationInfrastructureTotal, equipmentExpectationsEquipmentIntentId, siteId, state, displayState, shelf, slot, subSlot, subsubSlot, subShelf, availabilityState, displayAvailabilityState, reservationState, maintenanceMode, cardType, provisionedSpecPartNumber, specificationMismatch, category, neContactState, networkConstructMacAddress, networkConstructSubnetName, projectName, startDateMin, startDateMax, endDateMin, endDateMax, srlg, secondaryState, serial, customer, namedQuery, namedQueryParam1, fields, sortBy, offset, limit, metaDataFields, metaLimit, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieves the equipment satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/equipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">complexQueryParameters(physicalLocations, projects, projectIds, projectListIds, projectLists, organizeByProject, callback)</td>
    <td style="padding:15px">Generate equipment summary for projects, project lists, or physical locations.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v3/EquipmentSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByQueryParams(physicalLocations, projects, projectId, projectName, projectListId, projectListName, organizeByProject, callback)</td>
    <td style="padding:15px">Generate equipment summary for projects, project lists, or physical locations.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/EquipmentSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1FiberRoutes(body, callback)</td>
    <td style="padding:15px">Create a new fiber route.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/FiberRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1FiberRoutes(body, callback)</td>
    <td style="padding:15px">Roll fiber Route. The method detects the affected fiber route and returns the delta between the aff</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/FiberRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1GlobalConfigurationData(callback)</td>
    <td style="padding:15px">Get all config data</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationData(body, callback)</td>
    <td style="padding:15px">Updates the value for config data item.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1GlobalConfigurationDataItemKey(itemKey, callback)</td>
    <td style="padding:15px">Get a config item corresponding to a given key</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataHeatDissipationLimitThreshold1HeatDissipationLimitThreshold1Value(heatDissipationLimitThreshold1Value, callback)</td>
    <td style="padding:15px">Update config value corresponding to heat dissipation limit per bay threshold 1.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/HeatDissipationLimitThreshold1/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataHeatDissipationLimitThreshold2HeatDissipationLimitThreshold2Value(heatDissipationLimitThreshold2Value, callback)</td>
    <td style="padding:15px">Update config value corresponding to heat dissipation limit per bay threshold 2.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/HeatDissipationLimitThreshold2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataSingleILASiteLabelSingleILASiteLabelValue(singleILASiteLabelValue, callback)</td>
    <td style="padding:15px">Update the single ILA site label</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/SingleILASiteLabel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataDualILASiteLabelDualILASiteLabelValue(dualILASiteLabelValue, callback)</td>
    <td style="padding:15px">Update the dual ILA site label</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/DualILASiteLabel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataDgeWithSingleILASiteLabelDgeWithSingleILASiteLabelValue(dgeWithSingleILASiteLabelValue, callback)</td>
    <td style="padding:15px">Update the DGE with single ILA site label</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/DgeWithSingleILASiteLabel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataDgeWithAtoZDualILAZtoASingleILASiteLabelDgeWithAtoZDualILAZtoASingleILASiteLabelValue(dgeWithAtoZDualILAZtoASingleILASiteLabelValue, callback)</td>
    <td style="padding:15px">Update the DGE with dual ILA in A-Z and single ILA in Z-A site label</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/DgeWithAtoZDualILAZtoASingleILASiteLabel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataDgeWithAtoZSingleILAZtoADualILASiteLabelDgeWithAtoZSingleILAZtoADualILASiteLabelValue(dgeWithAtoZSingleILAZtoADualILASiteLabelValue, callback)</td>
    <td style="padding:15px">Update the DGE with single ILA in A-Z and dualILA in Z-A site label</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/DgeWithAtoZSingleILAZtoADualILASiteLabel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataDgeWithDualILASiteLabelDgeWithDualILASiteLabelValue(dgeWithDualILASiteLabelValue, callback)</td>
    <td style="padding:15px">Update the DGE with dual ILA site label</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/DgeWithDualILASiteLabel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1GlobalConfigurationDataDefaultProfilePreferencesValueDefaultProfilePreferencesValue(defaultProfilePreferencesValue, callback)</td>
    <td style="padding:15px">Update config value corresponding to default preferences profile.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/GlobalConfigurationData/DefaultProfilePreferencesValue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1HeatDissipationReport(physicalLocations, tidNames, nodeIdentities, racks, callback)</td>
    <td style="padding:15px">Get Heat Dissipation Report</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/HeatDissipationReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEquipmenttopologyplanningApiV2PlannedDataFiberLoss(body, callback)</td>
    <td style="padding:15px">Partially updates a list of fibers</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/PlannedData/fiberLoss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV2PlannedDataFiberLoss(freIds, callback)</td>
    <td style="padding:15px">Get fiber loss data for one or more freIds</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/PlannedData/fiberLoss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV2PlannedDataSnrData(freIds, callback)</td>
    <td style="padding:15px">Get planned Oms Snr data for one or more Oms FreIds.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/PlannedData/snrData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV2PlannedDataSnrDataFile(freIds, fileType, callback)</td>
    <td style="padding:15px">Gets the planned Oms Snr data from the database as a compressed file, either zip or gzip format</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/PlannedData/snrData/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV2PlannedDataInsertPlannedOmsSnrData(body, callback)</td>
    <td style="padding:15px">Inserts the planned oms snr info received from oneplanner into the database</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/PlannedData/InsertPlannedOmsSnrData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV2PlannedDataInsertPlannedOmsSnrDataFromFile(contentType, contentDisposition, headers, length, name, fileName, callback)</td>
    <td style="padding:15px">Insert planned Oms Snr data from a gzip (.gz), zip (.zip), JSON (.json) or OnePlanner (.onep) file.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v2/PlannedData/InsertPlannedOmsSnrDataFromFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmenttopologyplanningApiV3PlannedDataDeletePlannedFiberData(callback)</td>
    <td style="padding:15px">Delete all planned fiber data that does not belong to a planning project</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v3/PlannedData/DeletePlannedFiberData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV3PlannedDataFiberLoss(freIds, callback)</td>
    <td style="padding:15px">Get fiber loss data for one or more FreIds</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v3/PlannedData/fiberLoss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV3PlannedDataInsertPlannedOmsSnrData(body, callback)</td>
    <td style="padding:15px">Inserts the planned oms snr info received from oneplanner into the database</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v3/PlannedData/InsertPlannedOmsSnrData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV3PlannedDataInsertPlannedOmsSnrDataFromFile(contentType, contentDisposition, headers, length, name, fileName, callback)</td>
    <td style="padding:15px">Insert planned Oms Snr data from a gzip (.gz), zip (.zip), JSON (.json) or OnePlanner (.onep) file.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v3/PlannedData/InsertPlannedOmsSnrDataFromFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmenttopologyplanningApiV3PlannedDataDeletePlannedOmsSnrDataProjectId(projectId, callback)</td>
    <td style="padding:15px">This API deletes Snr data for any Oms that has not yet been deployed using Project ID.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v3/PlannedData/DeletePlannedOmsSnrData/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1PlanningPolicies(name, callback)</td>
    <td style="padding:15px">Gets planning policies</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/PlanningPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1PlanningPolicies(body, callback)</td>
    <td style="padding:15px">Create a new planning policy</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/PlanningPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyByGuid(id, callback)</td>
    <td style="padding:15px">Get a planning policy by Id</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/PlanningPolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmenttopologyplanningApiV1PlanningPoliciesId(id, body, callback)</td>
    <td style="padding:15px">Updates the specified planning policy</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/PlanningPolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmenttopologyplanningApiV1PlanningPoliciesId(id, callback)</td>
    <td style="padding:15px">Deletes a planning policy</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/PlanningPolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1PostUpgradeUpgradeProjects(body, callback)</td>
    <td style="padding:15px">Updates the projects according to new Onep Object model.</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/PostUpgrade/UpgradeProjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ProjectEquipmentDetailsEquipmentcount(intentId, projectName, callback)</td>
    <td style="padding:15px">get total equipment count for a project intent</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/ProjectEquipmentDetails/equipmentcount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmenttopologyplanningApiV1ProjectEquipmentDetailsEquipmentdetailsdata(intentId, projectName, callback)</td>
    <td style="padding:15px">Deletes equipment details data for a project intent</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/ProjectEquipmentDetails/equipmentdetailsdata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsOmsProjectId(projectId, callback)</td>
    <td style="padding:15px">Generate the OMS report for the project network</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/oms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsEquipmentProjectId(projectId, callback)</td>
    <td style="padding:15px">Generate the equipment report for the project network</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/equipment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsNetworkReportData(formFile, callback)</td>
    <td style="padding:15px">Generate the Network Data</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/networkReportData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsEquipmentProjectIdDelta(projectId, callback)</td>
    <td style="padding:15px">Generate the equipment delta report for the project network</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/equipment/{pathv1}/delta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsNodeandequipmentdataProjectId(projectId, callback)</td>
    <td style="padding:15px">Generate the node and equipment data report for the project network</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/nodeandequipmentdata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsImportvalidationProjectId(projectId, callback)</td>
    <td style="padding:15px">Get the import validation report for a project

 Returns a cached version of the report if it exi</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/importvalidation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmenttopologyplanningApiV1ReportsImportvalidation(projectName, callback)</td>
    <td style="padding:15px">Get the import validation report for a project

 Returns a cached version of the report if it exi</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/importvalidation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1ReportsImportvalidationProjectIdExecute(projectId, callback)</td>
    <td style="padding:15px">Executes import validation on the project</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/importvalidation/{pathv1}/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmenttopologyplanningApiV1ReportsImportvalidationExecute(projectName, callback)</td>
    <td style="padding:15px">Executes import validation on the project</td>
    <td style="padding:15px">{base_path}/{version}/equipmenttopologyplanning/api/v1/Reports/importvalidation/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listReports(cursor, size, callback)</td>
    <td style="padding:15px">Get all current reports</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReport(body, callback)</td>
    <td style="padding:15px">Creates a new report or returns an in progress report ID.</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReport(reportId, meta, callback)</td>
    <td style="padding:15px">Get the report with corresponding reportId value</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentGroup(ncId, networkConstructId, type = 'ROADM', equipmentId, equipmentGroupExpectationsEquipmentIntentId, include, fields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves equipment groups satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmentGroup(body, callback)</td>
    <td style="padding:15px">Creates or updates an equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentGroupById(equipmentGroupId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmentGroupById(equipmentGroupId, callback)</td>
    <td style="padding:15px">Delete a specific equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentGroupExpectations(equipmentGroupId, callback)</td>
    <td style="padding:15px">Retrieves the expectations of an equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups/{pathv1}/equipmentGroupExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEquipmentGroupExpectation(equipmentGroupId, body, callback)</td>
    <td style="padding:15px">Create or update an equipment group expectation under a given equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups/{pathv1}/equipmentGroupExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiEquipmentGroupsEquipmentGroupIdEquipmentGroupExpectationsEquipmentGroupExpectationId(equipmentGroupId, equipmentGroupExpectationId, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment group expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups/{pathv1}/equipmentGroupExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmentGroupExpectationById(equipmentGroupId, equipmentGroupExpectationId, callback)</td>
    <td style="padding:15px">Delete a specific equipment group expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentGroups/{pathv1}/equipmentGroupExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentGroups(ncId, networkConstructId, type = 'ROADM', equipmentId, equipmentGroupExpectationsEquipmentIntentId, include, fields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves equipment groups satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV20EquipmentGroups(body, callback)</td>
    <td style="padding:15px">Creates or updates an equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentGroupsEquipmentGroupId(equipmentGroupId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV20EquipmentGroupsEquipmentGroupId(equipmentGroupId, callback)</td>
    <td style="padding:15px">Delete a specific equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentGroupsEquipmentGroupIdEquipmentGroupExpectations(equipmentGroupId, callback)</td>
    <td style="padding:15px">Retrieves the expectations of an equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups/{pathv1}/equipmentGroupExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV20EquipmentGroupsEquipmentGroupIdEquipmentGroupExpectations(equipmentGroupId, body, callback)</td>
    <td style="padding:15px">Create or update an equipment group expectation under a given equipment group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups/{pathv1}/equipmentGroupExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentGroupsEquipmentGroupIdEquipmentGroupExpectationsEquipmentGroupExpectationId(equipmentGroupId, equipmentGroupExpectationId, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment group expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups/{pathv1}/equipmentGroupExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV20EquipmentGroupsEquipmentGroupIdEquipmentGroupExpectationsEquipmentGroupExpectationId(equipmentGroupId, equipmentGroupExpectationId, callback)</td>
    <td style="padding:15px">Delete a specific equipment group expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentGroups/{pathv1}/equipmentGroupExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3EquipmentGroups(ncId, networkConstructId, type = 'ROADM', equipmentId, equipmentGroupExpectationsEquipmentIntentId, include, fields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves equipment groups satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentHolder(networkConstructId, networkConstructIdQuery, fields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves equipment holders satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentHolders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentHolderById(equipmentHolderId, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment holder</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentHolders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentHolders(networkConstructId, networkConstructIdQuery, fields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves equipment holders satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentHolders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20EquipmentHoldersEquipmentHolderId(equipmentHolderId, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment holder</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/equipmentHolders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3EquipmentHolders(networkConstructId, networkConstructIdQuery, fields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves equipment holders satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/equipmentHolders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clientPortOperation(body, callback)</td>
    <td style="padding:15px">Client port operations create/disable/enable/delete</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/clientPort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalConstraints(customerName, callback)</td>
    <td style="padding:15px">Get controller Parameters Setting</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/controllerParametersSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configGlobalConstraints(body, callback)</td>
    <td style="padding:15px">Configures controller Parameters Setting</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/controllerParametersSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGlobalConstraints(customerName, callback)</td>
    <td style="padding:15px">Delete controller Parameters Setting based on customer name</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/controllerParametersSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllEquipmentIntents(callback)</td>
    <td style="padding:15px">Get all Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEquipmentIntent(body, callback)</td>
    <td style="padding:15px">Create Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEquipmentIntent(body, callback)</td>
    <td style="padding:15px">Update Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeEquipmentIntents(callback)</td>
    <td style="padding:15px">Upgrades Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents/postUpgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeEquipmentIntentsProjectName(callback)</td>
    <td style="padding:15px">Updates Equipment projectName</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents/postUpgrade/projectName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEquipmentIntent(projectName, ignoreEquipmentProvisioning, callback)</td>
    <td style="padding:15px">Delete an Equipment Intent based on project name</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents/projectName/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentIntent(id, callback)</td>
    <td style="padding:15px">Get an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiEquipmentIntentsId(id, ignoreEquipmentProvisioning, callback)</td>
    <td style="padding:15px">Delete an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEquipmentState(equipmentId, body, callback)</td>
    <td style="padding:15px">Updates equipment state for a service intent</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/equipmentState/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegenPorts(location, name, callback)</td>
    <td style="padding:15px">Get Regen Port by Location of Port or Network Construct Name</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/regenPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configRegenPorts(body, callback)</td>
    <td style="padding:15px">Configures Regen Port</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/regenPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpdatedExpectations(intentId, callback)</td>
    <td style="padding:15px">Gets updated expectations for a given intent id</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/updatedExpectations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1ClientPort(body, callback)</td>
    <td style="padding:15px">Client port operations create/disable/enable/delete</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/clientPort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ControllerParametersSetting(customerName, callback)</td>
    <td style="padding:15px">Get controller Parameters Setting</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/controllerParametersSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV1ControllerParametersSetting(body, callback)</td>
    <td style="padding:15px">Configures controller Parameters Setting</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/controllerParametersSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV1ControllerParametersSetting(customerName, callback)</td>
    <td style="padding:15px">Delete controller Parameters Setting based on customer name</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/controllerParametersSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1EquipmentIntents(callback)</td>
    <td style="padding:15px">Get all Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1EquipmentIntents(body, callback)</td>
    <td style="padding:15px">Create Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV1EquipmentIntentsProjectNameProjectName(projectName, ignoreEquipmentProvisioning, callback)</td>
    <td style="padding:15px">Delete an Equipment Intent based on project name</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/equipmentIntents/projectName/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1EquipmentIntentsId(id, callback)</td>
    <td style="padding:15px">Get an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV1EquipmentIntentsId(id, ignoreEquipmentProvisioning, callback)</td>
    <td style="padding:15px">Delete an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV1EquipmentStateEquipmentId(equipmentId, body, callback)</td>
    <td style="padding:15px">Updates equipment state for a service intent</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/equipmentState/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1RegenPorts(location, name, callback)</td>
    <td style="padding:15px">Get Regen Port by Location of Port or Network Construct Name</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/regenPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV1RegenPorts(body, callback)</td>
    <td style="padding:15px">Configures Regen Port</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/regenPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1UpdatedExpectationsIntentId(intentId, callback)</td>
    <td style="padding:15px">Gets updated expectations for a given intent id</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/updatedExpectations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2EquipmentIntents(callback)</td>
    <td style="padding:15px">Get all Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV2EquipmentIntents(body, callback)</td>
    <td style="padding:15px">Create Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV2EquipmentIntents(body, callback)</td>
    <td style="padding:15px">Update Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2EquipmentIntentsId(id, callback)</td>
    <td style="padding:15px">Get an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV2EquipmentIntentsId(id, ignoreEquipmentProvisioning, callback)</td>
    <td style="padding:15px">Delete an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3EquipmentIntents(callback)</td>
    <td style="padding:15px">Get all Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV3EquipmentIntents(body, callback)</td>
    <td style="padding:15px">Create Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/equipmentIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV3EquipmentIntentsPostUpgrade(callback)</td>
    <td style="padding:15px">Updates Equipment Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/equipmentIntents/postUpgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV3EquipmentIntentsPostUpgradeProjectName(callback)</td>
    <td style="padding:15px">Updates Equipment projectName</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/equipmentIntents/postUpgrade/projectName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3EquipmentIntentsId(id, callback)</td>
    <td style="padding:15px">Get an Equipment Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/equipmentIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentStaticSpecs(networkConstructId, equipmentId, attributesCctPackageCctState, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of EquipmentStaticSpec provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentStaticSpecs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentStaticSpec(equipmentStaticSpecId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific equipmentStaticSpec</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/equipmentStaticSpecs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV1EquipmentStaticSpecs(networkConstructId, equipmentId, attributesCctPackageCctState, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of EquipmentStaticSpec provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/equipmentStaticSpecs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentStaticSpecById(equipmentStaticSpecId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific equipment static spec</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/equipmentStaticSpecs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredSiteStatus(callback)</td>
    <td style="padding:15px">Get the current status of the geo-redundant site</td>
    <td style="padding:15px">{base_path}/{version}/geored/site/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredSiteStatus(bodyParams, callback)</td>
    <td style="padding:15px">Change the activity state of the geo-redundant site to ACTIVE, or change the IP address.</td>
    <td style="padding:15px">{base_path}/{version}/geored/site/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredApiV1Sites(callback)</td>
    <td style="padding:15px">Get the current status of all sites in the geo-redundant system</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredClusterStatus(clusterName, callback)</td>
    <td style="padding:15px">Get the current status of a geo-redundant application cluster.</td>
    <td style="padding:15px">{base_path}/{version}/geored/cluster/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredClusterStatus(bodyParams, callback)</td>
    <td style="padding:15px">Change the status of a geo-redundant application cluster.</td>
    <td style="padding:15px">{base_path}/{version}/geored/cluster/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredClusterRemoteStatus(clusterName, siteId, callback)</td>
    <td style="padding:15px">Get the current status of a geo-redundant application cluster on a remote site.</td>
    <td style="padding:15px">{base_path}/{version}/geored/cluster/remote-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredSiteRemotes(callback)</td>
    <td style="padding:15px">Get the current remote site configuration.</td>
    <td style="padding:15px">{base_path}/{version}/geored/site/remotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredSiteRemotes(bodyParams, callback)</td>
    <td style="padding:15px">Add a new remote site. The current version of this app only supports 1 configured remote.</td>
    <td style="padding:15px">{base_path}/{version}/geored/site/remotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeoredSiteRemotes(siteId, callback)</td>
    <td style="padding:15px">Remove an existing remote site.</td>
    <td style="padding:15px">{base_path}/{version}/geored/site/remotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredApiV1SiteRemotesRemoved(callback)</td>
    <td style="padding:15px">Get the last-removed remote site.</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/remotes/removed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeoredApiV1SiteRemotesRemovedId(id, callback)</td>
    <td style="padding:15px">Remove a last-removed site from the data store</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/remotes/removed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredApiV1SiteRemotesRecoveryId(id, type = 'hard', callback)</td>
    <td style="padding:15px">Recover a previously active site as a new standby.</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/remotes/recovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeoredApiV1SiteRemotesRecoveryId(id, callback)</td>
    <td style="padding:15px">Delete a previously executed site recovery task.</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/remotes/recovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredApiV1SiteRemotesRecoveryId(id, callback)</td>
    <td style="padding:15px">Get the recovery status</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/remotes/recovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredSiteResync(body, callback)</td>
    <td style="padding:15px">Request a resync of application data, typically performed after a prolonged inter-site outage.</td>
    <td style="padding:15px">{base_path}/{version}/geored/site/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncCreate(callback)</td>
    <td style="padding:15px">resync</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredApiV1SiteActivate(callback)</td>
    <td style="padding:15px">Request to make the local standby site active. This request will sever the connection to the curren</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoredApiV1SiteActivate(callback)</td>
    <td style="padding:15px">Get the activation status</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeoredApiV1SiteActivate(callback)</td>
    <td style="padding:15px">Delete a previously executed site activation task.</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeoredApiV1SiteGeoClean(callback)</td>
    <td style="padding:15px">De-provisions any existing IPSec tunnel configuration present for the local site's cluster.</td>
    <td style="padding:15px">{base_path}/{version}/geored/api/v1/site/geo-clean?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getValue(application, instance, partition, group, name, sort, tags, callbacks, nofailover, password, callback)</td>
    <td style="padding:15px">Returns the configuration value specified by URL parameters.</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcsApiV1Config(sort, body, callback)</td>
    <td style="padding:15px">Create the configuration specified.</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overwriteValue(sort, body, callback)</td>
    <td style="padding:15px">Create or overwrite the configuration specified.</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateValue(sort, body, callback)</td>
    <td style="padding:15px">Update the configuration specified.</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteValue(sort, body, callback)</td>
    <td style="padding:15px">Delete the configuration specified.
 Note: set 'hard' flag to 'true' in request body to completely</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcsApiV2Config(application, instance, partition, group, name, sort, tags, callbacks, nofailover, password, callback)</td>
    <td style="padding:15px">Returns the configuration value specified by URL parameters. Will retrun emyty list in case no conf</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v2/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCallback(sort, body, callback)</td>
    <td style="padding:15px">Adds the given callbacks to the configuration specified.</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/callback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCallback(sort, body, callback)</td>
    <td style="padding:15px">Removes the given callbacks from the configuration specified</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/callback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInheritable(sort, body, callback)</td>
    <td style="padding:15px">Adds the given inheritable callbacks to the configuration specified.</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/inheritable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInheritable(sort, body, callback)</td>
    <td style="padding:15px">Removes the given inheritable callbacks from the configuration specified and all applicable child c</td>
    <td style="padding:15px">{base_path}/{version}/gcs/api/v1/inheritable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(offset, limit, freId, include, callback)</td>
    <td style="padding:15px">Retrieves groups satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroup(body, callback)</td>
    <td style="padding:15px">Creates or updates a Group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putGroup(groupId, group, callback)</td>
    <td style="padding:15px">Create or Update a specific group with top level attributes</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putGroupPlanned(groupId, group, callback)</td>
    <td style="padding:15px">Update a specific planned group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}/groupPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPlannedGroup(groupId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}/groupPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupById(id, include, callback)</td>
    <td style="padding:15px">Retrieves a group with specific id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupById(id, callback)</td>
    <td style="padding:15px">Delete a group with given id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentifier(id, identifierKey, identifier, callback)</td>
    <td style="padding:15px">Create or update an group's identifier</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupIdentifiers(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an identifier from a given group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiGroupsIdUserDataUserDataKey(id, userDataKey, userDataValue, callback)</td>
    <td style="padding:15px">Create or update an group's userData</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupUserData(id, userDataKey, callback)</td>
    <td style="padding:15px">Delete a specific userData from specific group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/groups/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3Groups(offset, limit, freId, include, callback)</td>
    <td style="padding:15px">Retrieve groups</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3Groups(body, callback)</td>
    <td style="padding:15px">Trigger the creation of group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3GroupsGroupId(groupId, group, callback)</td>
    <td style="padding:15px">Create or Update a specific group with top level attributes</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3GroupsGroupIdGroupPlanned(groupId, group, callback)</td>
    <td style="padding:15px">Update a specific planned group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}/groupPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3GroupsGroupIdGroupPlanned(groupId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}/groupPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3GroupsId(id, include, callback)</td>
    <td style="padding:15px">Retrieve group with specified id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3GroupsId(id, callback)</td>
    <td style="padding:15px">Delete a group with given id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3GroupsIdIdentifiersIdentifierKey(id, identifierKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an identifier to a given group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3GroupsIdIdentifiersIdentifierKey(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an identifier from a given group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserData(id, userDataKey, body, callback)</td>
    <td style="padding:15px">Creates or updates UserData to a given group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3GroupsIdUserDataUserDataKey(id, userDataKey, callback)</td>
    <td style="padding:15px">Delete an userData from a given group</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/groups/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntriesForServiceId(serviceId, from, to, filterCategory, filterEventName, filterUserName, page, resultsPerPage, callback)</td>
    <td style="padding:15px">Retrieves all events for a given service between two dates</td>
    <td style="padding:15px">{base_path}/{version}/inventory-history/v1/query/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceEventCounts(serviceId, callback)</td>
    <td style="padding:15px">Retrieves weekly counts of events for a given service from today to the configured maximum retentio</td>
    <td style="padding:15px">{base_path}/{version}/inventory-history/v1/query/{pathv1}/timeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceEvent(serviceId, eventId, week, callback)</td>
    <td style="padding:15px">Retrieves detailed information on a specific service event, the eventId and week can be obtailed fr</td>
    <td style="padding:15px">{base_path}/{version}/inventory-history/v1/query/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchEquipmentGroups(id, networkConstructId, name, type, subType, equipmentId, equipmentGroupExpectationsEquipmentIntentId, searchText, searchFields, searchType = 'wildCard', resourceState, startDateMin, startDateMax, endDateMin, endDateMax, fields, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieves equipment groups satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchEquipmentGroupsV1(id, networkConstructId, name, type, subType, equipmentId, equipmentGroupExpectationsEquipmentIntentId, searchText, searchFields, searchType = 'wildCard', resourceState, startDateMin, startDateMax, endDateMin, endDateMax, fields, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieves equipment groups satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/equipmentGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchFres(id, searchText, searchFields, searchType = 'wildCard', resourceState, layerRate, networkConstructId, networkConstructIdOperator = 'or', tpeId, tpeIdOperator = 'or', physicalNeName, physicalNeNameOperator = 'or', identifierKey, identifierValue, concrete, modelType, bookingDataLockout, group = 'dwa', freType, userLabel, managementName, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, freExpectationsIntentId, freExpectationsIntentType, signalContentType, srlg, roadmLineId, endpointTpeConcrete, deploymentState, active, directionality, networkRole, type, serviceClass, layerRateQualifier, serviceRate, supportedByFreId, supportedByQualifier = 'one', supportingFreId, customerName, sncgUserlabel, tunnelType, ringType, ringCompleteness, adminState, operationState, lqsDataStatus, lqsDataMarginValid, lqsDataFiberReconciled, lqsDataFiberMethod, lqsDataFiberDeltaAvgToPlannedLoss, lqsDataFiberAvgLoss, lqsDataFiberLastCalculationTime, lqsDataFiberAvgLossStatus, lqsDataFiberPlannedLoss, lqsDataFiberPlannedLossMargin, lqsDataFiberCurrentLoss, lqsDataFiberCurrentLossTime, lqsDataFiberCurrentLossMethod, lqsDataMarginViableAtEol, lqsDataPpgSnrStatus, lqsDataPpgCategorySnrReference, lqsDataPpgDeltaSnrReference, lqsDataPpgStartDeltaSnrReference, lqsDataPpgEndDeltaSnrReference, restorationHealthTotalExplicitRoutes, restorationHealthAvailableExplicitRoutes, restorationHealthUnavailableExplicitRoutes, restorationHealthAvailablePercentage, restorationHealthUnavailablePercentage, restorationHealthHomeAvailable, restorationHealthStartAvailablePercentage, restorationHealthEndAvailablePercentage, coroutedFreId, tags, displayAdminState, displayOperationState, displayTopologySource, utilizationDataTotalCapacity, utilizationDataUsedCapacity, utilizationDataUtilizationPercent, utilizationDataStartUtilizationPercent, utilizationDataEndUtilizationPercent, utilizationDataCapacityUnits, startDateMin, startDateMax, endDateMin, endDateMax, namedQuery, namedQueryParam1, relatedServices, domainTypes, resilienceLevel, partitionFreIds, decomposedFreIds, facilityBypass, childFreId, gneSubnetName, routingConstraintsInclusionConstraintsId, displayDeploymentState, ncTags, displayRecoveryCharacteristicsOnHome, remoteOSRPNodeName, headOSRPNodeName, isUsedByService, isDTLSetUsedAsRoutingConstraint, isOtdrCapable, sourceTraceType, sourceTraceTimestamp, destinationTraceType, destinationTraceTimestamp, sourceOtdrcfgaId, sourceNcId, destinationOtdrcfgaId, destinationNcId, isSubmarineLink, ovpnIds, partitioningFreIds, internalStructure, exportRouteTargets, importRouteTargets, filterType, nativeName, displayName, peerSNCIdentifier, interfaceNameInterfaceIP, dailyThroughputAverageUtilization, dailyThroughputStartAverageUtilization, dailyThroughputEndAverageUtilization, dailyThroughputMaxUtilization, dailyThroughputStartMaxUtilization, dailyThroughputEndMaxUtilization, dailyThroughputNinetyFifthUtilization, dailyThroughputStartNinetyFifthUtilization, dailyThroughputEndNinetyFifthUtilization, weeklyThroughputAverageUtilization, weeklyThroughputStartAverageUtilization, weeklyThroughputEndAverageUtilization, weeklyThroughputMaxUtilization, weeklyThroughputStartMaxUtilization, weeklyThroughputEndMaxUtilization, weeklyThroughputNinetyFifthUtilization, weeklyThroughputStartNinetyFifthUtilization, weeklyThroughputEndNinetyFifthUtilization, completeness, misconfigured, destinationPrefix, selectedRoute, vrfName, nextHopIp, vrfId, statsCollectionOperational, dmmState, slmState, signalingType, sourceEndPoint, destEndPoint, bindingAllocatedSid, cfmAdminState, color, displayServerRefreshState, serviceRefreshState, serviceRefreshStatus, transportPolicy, catalog, srColor, fallback, ringStatus, ringState, fields, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchPostFresV2(body, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreById(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchFresV1(id, searchText, searchFields, resourceState, layerRate, networkConstructId, networkConstructIdQualifier = 'or', tpeId, tpeIdQualifier = 'or', identifierKey, identifierValue, concrete, modelType, bookingDataLockout = 'true', group = 'dwa', freType, userLabel, managementName, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, signalContentType, srlg, roadmLineId, endpointTpeConcrete, deploymentState, active = 'true', directionality = 'unidirectional', networkRole = 'IFRE', type, serviceClass, layerRateQualifier, supportedByFreId, supportedByQualifier = 'one', supportingFreId, customerName, sncgUserlabel, tunnelType, adminState, operationState, lqsDataStatus = 'good', lqsDataMarginValid = 'true', lqsDataFiberReconciled = 'true', lqsDataFiberMethod = 'totalPower', lqsDataMarginViableAtEol, coroutedFreId, tags, displayAdminState, displayOperationState, displayTopologySource, startDateMin, startDateMax, endDateMin, endDateMax, namedQuery, domainTypes, resilienceLevel, partitionFreIds, decomposedFreIds, facilityBypass = 'exclude', childFreId, gneSubnetName, fields, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreByIdV1(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchFresV2(id, searchText, searchFields, searchType = 'wildCard', resourceState, layerRate, networkConstructId, networkConstructIdOperator = 'or', tpeId, tpeIdOperator = 'or', physicalNeName, physicalNeNameOperator = 'or', identifierKey, identifierValue, concrete, modelType, bookingDataLockout, group = 'dwa', freType, userLabel, managementName, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, freExpectationsIntentId, freExpectationsIntentType, signalContentType, srlg, roadmLineId, endpointTpeConcrete, deploymentState, active, directionality, networkRole, type, serviceClass, layerRateQualifier, serviceRate, supportedByFreId, supportedByQualifier = 'one', supportingFreId, customerName, sncgUserlabel, tunnelType, ringType, ringCompleteness, adminState, operationState, lqsDataStatus, lqsDataMarginValid, lqsDataFiberReconciled, lqsDataFiberMethod, lqsDataFiberDeltaAvgToPlannedLoss, lqsDataFiberAvgLoss, lqsDataFiberAvgLossStatus, lqsDataFiberLastCalculationTime, lqsDataFiberPlannedLoss, lqsDataFiberPlannedLossMargin, lqsDataFiberCurrentLoss, lqsDataFiberCurrentLossTime, lqsDataFiberCurrentLossMethod, lqsDataMarginViableAtEol, lqsDataPpgSnrStatus, lqsDataPpgCategorySnrReference, lqsDataPpgDeltaSnrReference, lqsDataPpgStartDeltaSnrReference, lqsDataPpgEndDeltaSnrReference, restorationHealthTotalExplicitRoutes, restorationHealthAvailableExplicitRoutes, restorationHealthUnavailableExplicitRoutes, restorationHealthAvailablePercentage, restorationHealthUnavailablePercentage, restorationHealthHomeAvailable, restorationHealthStartAvailablePercentage, restorationHealthEndAvailablePercentage, coroutedFreId, tags, displayAdminState, displayOperationState, displayTopologySource, utilizationDataTotalCapacity, utilizationDataUsedCapacity, utilizationDataUtilizationPercent, utilizationDataStartUtilizationPercent, utilizationDataEndUtilizationPercent, utilizationDataCapacityUnits, startDateMin, startDateMax, endDateMin, endDateMax, namedQuery, namedQueryParam1, relatedServices, domainTypes, resilienceLevel, partitionFreIds, decomposedFreIds, facilityBypass, childFreId, gneSubnetName, routingConstraintsInclusionConstraintsId, displayDeploymentState, ncTags, displayRecoveryCharacteristicsOnHome, remoteOSRPNodeName, headOSRPNodeName, isUsedByService, isDTLSetUsedAsRoutingConstraint, isOtdrCapable, sourceTraceType, sourceTraceTimestamp, destinationTraceType, destinationTraceTimestamp, sourceOtdrcfgaId, sourceNcId, destinationOtdrcfgaId, destinationNcId, isSubmarineLink, ovpnIds, partitioningFreIds, internalStructure, exportRouteTargets, importRouteTargets, filterType, nativeName, displayName, peerSNCIdentifier, interfaceNameInterfaceIP, dailyThroughputAverageUtilization, dailyThroughputStartAverageUtilization, dailyThroughputEndAverageUtilization, dailyThroughputMaxUtilization, dailyThroughputStartMaxUtilization, dailyThroughputEndMaxUtilization, dailyThroughputNinetyFifthUtilization, dailyThroughputStartNinetyFifthUtilization, dailyThroughputEndNinetyFifthUtilization, weeklyThroughputAverageUtilization, weeklyThroughputStartAverageUtilization, weeklyThroughputEndAverageUtilization, weeklyThroughputMaxUtilization, weeklyThroughputStartMaxUtilization, weeklyThroughputEndMaxUtilization, weeklyThroughputNinetyFifthUtilization, weeklyThroughputStartNinetyFifthUtilization, weeklyThroughputEndNinetyFifthUtilization, completeness, misconfigured, destinationPrefix, selectedRoute, vrfName, nextHopIp, vrfId, statsCollectionOperational, dmmState, slmState, signalingType, sourceEndPoint, destEndPoint, bindingAllocatedSid, cfmAdminState, color, displayServerRefreshState, serviceRefreshState, serviceRefreshStatus, transportPolicy, catalog, srColor, fallback, ringStatus, ringState, fields, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2/search/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV2SearchFres(body, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2/search/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreByIdV2(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2/search/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchGroups(id, searchText, searchFields, searchType = 'wildCard', name, groupType, parentGroup, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the Groups satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV1SearchGroups(id, searchText, searchFields, searchType = 'wildCard', name, groupType, parentGroup, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of Groups provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchNcs(id, searchText, searchFields, searchType = 'wildCard', resourceState, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, name, displayName, identifierKey, identifierValue, concrete, modelType, ipAddress, networkConstructType, resourceType, associationState, syncState, softwareVersion, nativeSoftwareVersion, displaySyncState, displayInventorySyncFailureReason, displayResourceState, displayAssociationState, subnetName, macAddress, typeGroup, slteType, ssteType, equippedSlotUtilizationTotalCapacity, equippedSlotUtilizationUsedCapacity, equippedSlotUtilizationPercent, equippedSlotStartUtilizationPercent, equippedSlotEndUtilizationPercent, equippedPluggableUtilizationTotalCapacity, equippedPluggableUtilizationUsedCapacity, equippedPluggableUtilizationPercent, equippedPluggableStartUtilizationPercent, equippedPluggableEndUtilizationPercent, inUseByServiceSlotUtilizationTotalCapacity, inUseByServiceSlotUtilizationUsedCapacity, inUseByServiceSlotUtilizationPercent, inUseByServiceSlotStartUtilizationPercent, inUseByServiceSlotEndUtilizationPercent, inUseByServicePluggableUtilizationTotalCapacity, inUseByServicePluggableUtilizationUsedCapacity, inUseByServicePluggableUtilizationPercent, inUseByServicePluggableStartUtilizationPercent, inUseByServicePluggableEndUtilizationPercent, resourcePartitionInfo, tags, rrConfigured, namedQuery, namedQueryParam1, geoDistanceQuery, startDateMin, startDateMax, endDateMin, endDateMax, associationStateQualifier, srlg, siteId, siteName, prefixSid, loopbackAddresses, fields, sortBy, offset, limit, metaDataFields, metaLimit, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchNcsV1(id, searchText, searchFields, searchType = 'wildCard', resourceState, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, name, displayName, identifierKey, identifierValue, concrete, modelType, ipAddress, networkConstructType, resourceType, associationState, syncState, softwareVersion, displaySyncState, displayInventorySyncFailureReason, displayResourceState, displayAssociationState, subnetName, macAddress, typeGroup, slteType, ssteType, equippedSlotUtilizationTotalCapacity, equippedSlotUtilizationUsedCapacity, equippedSlotUtilizationPercent, equippedSlotStartUtilizationPercent, equippedSlotEndUtilizationPercent, equippedPluggableUtilizationTotalCapacity, equippedPluggableUtilizationUsedCapacity, equippedPluggableUtilizationPercent, equippedPluggableStartUtilizationPercent, equippedPluggableEndUtilizationPercent, inUseByServiceSlotUtilizationTotalCapacity, inUseByServiceSlotUtilizationUsedCapacity, inUseByServiceSlotUtilizationPercent, inUseByServiceSlotStartUtilizationPercent, inUseByServiceSlotEndUtilizationPercent, inUseByServicePluggableUtilizationTotalCapacity, inUseByServicePluggableUtilizationUsedCapacity, inUseByServicePluggableUtilizationPercent, inUseByServicePluggableStartUtilizationPercent, inUseByServicePluggableEndUtilizationPercent, resourcePartitionInfo, tags, rrConfigured, namedQuery, namedQueryParam1, geoDistanceQuery, startDateMin, startDateMax, endDateMin, endDateMax, associationStateQualifier, srlg, siteId, siteName, prefixSid, fields, sortBy, offset, limit, metaDataFields, metaLimit, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchNcsV2(id, searchText, searchFields, searchType = 'wildCard', resourceState, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, name, displayName, identifierKey, identifierValue, concrete, modelType, ipAddress, networkConstructType, resourceType, associationState, syncState, softwareVersion, nativeSoftwareVersion, displaySyncState, displayInventorySyncFailureReason, displayResourceState, displayAssociationState, subnetName, macAddress, typeGroup, slteType, ssteType, equippedSlotUtilizationTotalCapacity, equippedSlotUtilizationUsedCapacity, equippedSlotUtilizationPercent, equippedSlotStartUtilizationPercent, equippedSlotEndUtilizationPercent, equippedPluggableUtilizationTotalCapacity, equippedPluggableUtilizationUsedCapacity, equippedPluggableUtilizationPercent, equippedPluggableStartUtilizationPercent, equippedPluggableEndUtilizationPercent, inUseByServiceSlotUtilizationTotalCapacity, inUseByServiceSlotUtilizationUsedCapacity, inUseByServiceSlotUtilizationPercent, inUseByServiceSlotStartUtilizationPercent, inUseByServiceSlotEndUtilizationPercent, inUseByServicePluggableUtilizationTotalCapacity, inUseByServicePluggableUtilizationUsedCapacity, inUseByServicePluggableUtilizationPercent, inUseByServicePluggableStartUtilizationPercent, inUseByServicePluggableEndUtilizationPercent, resourcePartitionInfo, tags, rrConfigured, namedQuery, namedQueryParam1, geoDistanceQuery, startDateMin, startDateMax, endDateMin, endDateMax, associationStateQualifier, srlg, siteId, siteName, prefixSid, loopbackAddresses, fields, sortBy, offset, limit, metaDataFields, metaLimit, metaDataQualifiers, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2/search/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrlgAssignableEntities(entityType, riskType, networkConstructId, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves the entities satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/srlgAutoAssignableResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV1SearchSrlgAutoAssignableResources(entityType, riskType, networkConstructId, offset, limit, callback)</td>
    <td style="padding:15px">Retrieves the entities satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/srlgAutoAssignableResources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRLG(resourceId, resourceType, isStructured, plannedStartTime, srlgType, callback)</td>
    <td style="padding:15px">Get Srlg by Resource ID</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">triggerAssignmentOp(body, callback)</td>
    <td style="padding:15px">API to trigger Srlg Batch Operations</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSRLG(body, callback)</td>
    <td style="padding:15px">Updates SRLG on given resource</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrlgAssociations(resourceId, resourceType, srlgSource, includeMetaData, callback)</td>
    <td style="padding:15px">Provide details of SRLGs associated with the given resource</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/associatedSrlgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSrlgsfromLabel(userLabel, isStructured, offset, limit, callback)</td>
    <td style="padding:15px">Query External Assigned Srlgs</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/externalSRLGs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">triggerExternalPoolOp(body, callback)</td>
    <td style="padding:15px">Assign or Release Srlg Values from Pool</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/externalSRLGs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalSrlgFromId(extSRLGId, isStructured, callback)</td>
    <td style="padding:15px">Query External Srlg assignments.</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/externalSRLGs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobOpStatus(jobId, jobType, jobStatus, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/job/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobOpStatusDetails(jobId, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status details</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/job/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrlgMismatchResources(ncId, resourceType = 'fres', resourceId, offset, limit, callback)</td>
    <td style="padding:15px">Search resources which have srlg mismatch</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/srlgMismatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrlgMismatchDetails(mismatchId, callback)</td>
    <td style="padding:15px">Get Srlg Mismatch details for given mismatchId</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/srlgMismatch/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesFromSrlgValue(srlgValue, srlgSource, resourceType, includeMetaData, callback)</td>
    <td style="padding:15px">Search resources which have been assigned with given SRLG value</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlg/{pathv1}/assignmentDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTpes(id, searchText, searchFields, searchType = 'wildCard', resourceState, namedQuery, namedQueryParam1, namedQueryParam2, namedQueryParam3, networkConstructId, physicalNeName, identifierKey, identifierValue, concrete, modelType, bookingDataLockout, layerRate, adjacencyType, active, content = 'summary', location, locationFormat, structureType, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, tpeExpectationsIntentId, tpeExpectationsIntentType, startDateMin, startDateMax, endDateMin, endDateMax, srlg, ipAddress, esiId, fields, gneSubnetName, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, subslot, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/search/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTpesV1(id, searchText, searchFields, searchType = 'wildCard', resourceState, namedQuery, namedQueryParam1, namedQueryParam2, namedQueryParam3, networkConstructId, physicalNeName, identifierKey, identifierValue, concrete, modelType, bookingDataLockout, layerRate, adjacencyType, active, content = 'summary', location, locationFormat, structureType, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, tpeExpectationsIntentId, tpeExpectationsIntentType, startDateMin, startDateMax, endDateMin, endDateMax, gneSubnetName, srlg, ipAddress, esiId, fields, sortBy, offset, limit, metaDataFields, metaDataQualifiers, include, subslot, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/search/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKafkacometApiV1Socket(callback)</td>
    <td style="padding:15px">Get active Kafka topic websocket connections</td>
    <td style="padding:15px">{base_path}/{version}/kafkacomet/api/v1/socket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKafkacometApiV1SocketTopic(topic, callback)</td>
    <td style="padding:15px">Get active Kafka topic websocket connections by topic</td>
    <td style="padding:15px">{base_path}/{version}/kafkacomet/api/v1/socket/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLicenses(callback)</td>
    <td style="padding:15px">Return all MCP licenses and their current state</td>
    <td style="padding:15px">{base_path}/{version}/licensing-feature/api/v1/features/getAllLicenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicense(commonName, featureName, callback)</td>
    <td style="padding:15px">Checks for an mcp license with given name. Other licenses not related to MCP will return a 406 Not</td>
    <td style="padding:15px">{base_path}/{version}/licensing-feature/api/v1/features/getLicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadLicense(file, site, callback)</td>
    <td style="padding:15px">Loads license file to license servers</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/loadLicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">register(callback)</td>
    <td style="padding:15px">Register the license server</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerList(search, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all Pending registrations.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerCreate(firstName, lastName, email, tenant, password, username, callback)</td>
    <td style="padding:15px">Create a new user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerApproveUsers(registrations, callback)</td>
    <td style="padding:15px">Approve registrations for the requested registration uuids.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register/approve_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerConfirmEmail(token, callback)</td>
    <td style="padding:15px">Endpoint to confirm a user's email.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register/confirm_email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerForgotPassword(email, tenant, callback)</td>
    <td style="padding:15px">Endpoint to send a confirmation email link for forgotten password.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register/forgot_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerRejectUsers(registrations, callback)</td>
    <td style="padding:15px">Reject registrations for the requested registration uuids.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register/reject_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerResendConfirmation(email, tenant, callback)</td>
    <td style="padding:15px">Endpoint to resend a confirmation email for registered users.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register/resend_confirmation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerSetPassword(token, password, callback)</td>
    <td style="padding:15px">Endpoint to accept a password for forgotten password.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/register/set_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClients(reportId, features, clients, partNumbers, sortBy, cursor, size, callback)</td>
    <td style="padding:15px">Get Clients within a given report</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportAllClients(reportId, callback)</td>
    <td style="padding:15px">Get all clients for the current report</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports/clients/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatures(reportId, features, partNumbers, sortBy, cursor, size, callback)</td>
    <td style="padding:15px">Get Features within a given report</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportAllFeatures(reportId, callback)</td>
    <td style="padding:15px">Get all Features within a given report</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/reports/features/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceStatus(callback)</td>
    <td style="padding:15px">Get the licensing service status</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">triggerHealthCheck(callback)</td>
    <td style="padding:15px">Trigger an immediate license server health check</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/triggerHealthCheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAll(callback)</td>
    <td style="padding:15px">Trigger an immediate update of all licenses</td>
    <td style="padding:15px">{base_path}/{version}/licensing/api/v1/licensing/updateAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExtendedSubDomainsToApplicationSlice(applicationSliceId, subDomains, callback)</td>
    <td style="padding:15px">Add extended subdomains to the specified application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/add-extended-sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeExtendedSubDomainsFromApplicationSlice(applicationSliceId, subDomains, callback)</td>
    <td style="padding:15px">Remove extended subdomains from the specified application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/remove-extended-sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationSlices(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the application slices in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListApplicationSlices(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the application slices in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationSlice(applicationSliceRequest, callback)</td>
    <td style="padding:15px">Create an application slice in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countApplicationSlices(q, p, limit, callback)</td>
    <td style="padding:15px">Count the application slices in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountApplicationSlices(q, p, limit, callback)</td>
    <td style="padding:15px">Count the application slices in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationSlice(applicationSliceId, callback)</td>
    <td style="padding:15px">Get a specific application slice from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetApplicationSlice(applicationSliceId, callback)</td>
    <td style="padding:15px">Get a specific application slice from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationSlice(applicationSliceId, applicationSliceRequest, callback)</td>
    <td style="padding:15px">Update an application slice in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationSlice(applicationSliceId, callback)</td>
    <td style="padding:15px">Delete an application slice from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApplicationSlice(applicationSliceId, applicationSliceRequest, callback)</td>
    <td style="padding:15px">Patch update an application slice in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listParentApplicationSlices(applicationSliceId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists parent slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListParentApplicationSlices(applicationSliceId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists parent slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listChildrenApplicationSlices(applicationSliceId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all children slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListChildrenApplicationSlices(applicationSliceId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all children slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationSliceSubDomains(applicationSliceId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all non-extended subdomains of the given application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListApplicationSliceSubDomains(applicationSliceId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all non-extended subdomains of the given application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countApplicationSliceSubDomains(applicationSliceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count non-extended subdomains of the given application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/sub-domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountApplicationSliceSubDomains(applicationSliceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count non-extended subdomains of the given application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/sub-domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationSliceExtendedSubDomains(applicationSliceId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all extended subdomains of the given application slice by id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/extended-sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListApplicationSliceExtendedSubDomains(applicationSliceId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all extended subdomains of the given application slice by id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/extended-sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countApplicationSliceExtendedSubDomains(applicationSliceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count extended subdomains of the given application slice by id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/extended-sub-domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountApplicationSliceExtendedSubDomains(applicationSliceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count extended subdomains of the given application slice by id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/extended-sub-domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countChildrenApplicationSlices(applicationSliceId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count children slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/children/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountChildrenApplicationSlices(applicationSliceId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count children slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/children/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countParentApplicationSlices(applicationSliceId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count parent slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/parents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountParentApplicationSlices(applicationSliceId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count parent slices of the given application slice id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/parents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSubDomainsFromApplicationSlice(applicationSliceId, subDomains, callback)</td>
    <td style="padding:15px">Remove subdomains from the specified application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/remove-sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubDomainsToApplicationSlice(applicationSliceId, subDomains, callback)</td>
    <td style="padding:15px">Add subdomains to the specified application slice</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/application-slices/{pathv1}/add-sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDomainTypes(q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the domain types in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domain-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListDomainTypes(q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the domain types in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domain-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countDomainTypes(q, limit, callback)</td>
    <td style="padding:15px">Count all of the domain types in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domain-types/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountDomainTypes(q, limit, callback)</td>
    <td style="padding:15px">Count all of the domain types in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domain-types/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countDomains(q, p, limit, callback)</td>
    <td style="padding:15px">Count the domains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountDomains(q, p, limit, callback)</td>
    <td style="padding:15px">Count the domains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDomains(q, p, offset, limit, pageToken, obfuscate, callback)</td>
    <td style="padding:15px">Get all of the domains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListDomains(q, p, offset, limit, pageToken, obfuscate, callback)</td>
    <td style="padding:15px">Get all of the domains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDomain(domainRequest, validate, obfuscate, callback)</td>
    <td style="padding:15px">Create a domain in the market using the provided properties</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDomainCreate(full, domainRequest, callback)</td>
    <td style="padding:15px">Validate domain creation in market using the provided properties</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomain(domainId, obfuscate, callback)</td>
    <td style="padding:15px">Get a specific domain from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetDomain(domainId, obfuscate, callback)</td>
    <td style="padding:15px">Get a specific domain from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDomain(domainId, domainRequest, obfuscate, callback)</td>
    <td style="padding:15px">Update a domain in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomain(domainId, callback)</td>
    <td style="padding:15px">Delete a domain from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDomain(domainId, domainRequest, obfuscate, callback)</td>
    <td style="padding:15px">Patch update a domain in the market based on the (partial) data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProductsByDomain(domainId, includeInactive, q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the products offered by a specific domain</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListProductsByDomain(domainId, includeInactive, q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the products offered by a specific domain</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncDomain(domainId, full, callback)</td>
    <td style="padding:15px">Execute a resync request for the specified domain</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/domains/{pathv1}/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUnclaimedJobs(type, maxWait, minCount, maxCount, afterJobId, callback)</td>
    <td style="padding:15px">Get unclaimed jobs</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/unclaimed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListUnclaimedJobs(type, maxWait, minCount, maxCount, afterJobId, callback)</td>
    <td style="padding:15px">Get unclaimed jobs</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/unclaimed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countClaimedJobs(type, state = 'executing', limit, callback)</td>
    <td style="padding:15px">Count jobs that have been claimed but not completed for a given job type</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/claimed/{pathv1}/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountClaimedJobs(type, state = 'executing', limit, callback)</td>
    <td style="padding:15px">Count jobs that have been claimed but not completed for a given job type</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/claimed/{pathv1}/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listClaimedJobs(type, state = 'executing', offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get claimed jobs that are not complete yet</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/claimed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListClaimedJobs(type, state = 'executing', offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get claimed jobs that are not complete yet</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/claimed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJob(jobId, callback)</td>
    <td style="padding:15px">Get a job by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetJob(jobId, callback)</td>
    <td style="padding:15px">Get a job by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimJob(jobId, jobClaim, callback)</td>
    <td style="padding:15px">Claim a job for execution</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkJobOnly(jobId, callback)</td>
    <td style="padding:15px">Check execution status of a claimed job with the executor</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/execution-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCheckJobOnly(jobId, callback)</td>
    <td style="padding:15px">Check execution status of a claimed job with the executor</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/execution-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkJobModify(jobId, callback)</td>
    <td style="padding:15px">Check execution status of a claimed job with the executor and fail if out of sync</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/execution-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putJobProgress(jobId, jobProgress, callback)</td>
    <td style="padding:15px">Report job execution progress</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/progress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putJobResult(jobId, jobResult, callback)</td>
    <td style="padding:15px">Report a job execution result</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/result?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putJobSuspended(jobId, jobSuspension, callback)</td>
    <td style="padding:15px">Suspend or resume a job</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/jobs/{pathv1}/suspended?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProducts(includeInactive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the products in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListProducts(includeInactive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the products in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProduct(product, callback)</td>
    <td style="padding:15px">Create a new product in Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countProducts(includeInactive, q, p, limit, callback)</td>
    <td style="padding:15px">Count products in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountProducts(includeInactive, q, p, limit, callback)</td>
    <td style="padding:15px">Count products in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProduct(productId, callback)</td>
    <td style="padding:15px">Get the product from the market based on its product identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetProduct(productId, callback)</td>
    <td style="padding:15px">Get the product from the market based on its product identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProduct(productId, product, callback)</td>
    <td style="padding:15px">Update a product based on the complete data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProduct(productId, callback)</td>
    <td style="padding:15px">Delete the product from the market based on its product identifier</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProduct(productId, product, callback)</td>
    <td style="padding:15px">Patch update a product based on the (partial) data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncProduct(productId, full, callback)</td>
    <td style="padding:15px">Execute a resync request for the specified product</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/products/{pathv1}/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRelationships(q, p, relationshipTypeId, exactRelationshipTypeId, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the relationships registered with the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListRelationships(q, p, relationshipTypeId, exactRelationshipTypeId, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the relationships registered with the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRelationship(relationship, callback)</td>
    <td style="padding:15px">Create a new relationship in Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countRelationships(q, p, relationshipTypeId, exactRelationshipTypeId, limit, callback)</td>
    <td style="padding:15px">Count the relationships registered with the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountRelationships(q, p, relationshipTypeId, exactRelationshipTypeId, limit, callback)</td>
    <td style="padding:15px">Count the relationships registered with the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUnresolvedRelationships(resourceId, q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all unresolved relationships in Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/unresolved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListUnresolvedRelationships(resourceId, q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all unresolved relationships in Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/unresolved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countUnresolvedRelationships(resourceId, q, limit, callback)</td>
    <td style="padding:15px">Count unresolved relationships in Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/unresolved/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountUnresolvedRelationships(resourceId, q, limit, callback)</td>
    <td style="padding:15px">Count unresolved relationships in Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/unresolved/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelationship(relationshipId, callback)</td>
    <td style="padding:15px">Get the relationship from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetRelationship(relationshipId, callback)</td>
    <td style="padding:15px">Get the relationship from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRelationship(relationshipId, callback)</td>
    <td style="padding:15px">Delete a relationship</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/relationships/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceProviders(q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the resource providers in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListResourceProviders(q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the resource providers in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResourceProvider(resourceProviderRequest, callback)</td>
    <td style="padding:15px">Create a resource provider for a domain in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countResourceProviders(q, limit, callback)</td>
    <td style="padding:15px">Count the resource providers in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountResourceProviders(q, limit, callback)</td>
    <td style="padding:15px">Count the resource providers in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceProvider(resourceProviderId, callback)</td>
    <td style="padding:15px">Get a specific resource provider from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetResourceProvider(resourceProviderId, callback)</td>
    <td style="padding:15px">Get a specific resource provider from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourceProvider(resourceProviderId, resourceProviderRequest, callback)</td>
    <td style="padding:15px">Update a resource provider in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceProvider(resourceProviderId, callback)</td>
    <td style="padding:15px">Delete a resource provider from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourceProvider(resourceProviderId, resourceProviderRequest, callback)</td>
    <td style="padding:15px">Patch update a resource provider in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDomainsByRp(resourceProviderId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the domains for a given resource provider</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListDomainsByRp(resourceProviderId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the domains for a given resource provider</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-providers/{pathv1}/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceTypes(q, p, includeAbstract, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the resource types in the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListResourceTypes(q, p, includeAbstract, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the resource types in the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceType(resourceTypeId, callback)</td>
    <td style="padding:15px">Get a resource type by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetResourceType(resourceTypeId, callback)</td>
    <td style="padding:15px">Get a resource type by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countResourceTypes(q, p, includeAbstract, limit, callback)</td>
    <td style="padding:15px">Count the resource types in the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountResourceTypes(q, p, includeAbstract, limit, callback)</td>
    <td style="padding:15px">Count the resource types in the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProductsByResourceType(resourceTypeId, includeInactive, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the products offered for the specific resource type</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types/{pathv1}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListProductsByResourceType(resourceTypeId, includeInactive, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the products offered for the specific resource type</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resource-types/{pathv1}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResource(resourceId, full, obfuscate, minRevision, callback)</td>
    <td style="padding:15px">Get a resource by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetResource(resourceId, full, obfuscate, minRevision, callback)</td>
    <td style="padding:15px">Get a resource by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResource(resourceId, resource, validate, obfuscate, callback)</td>
    <td style="padding:15px">Update a resource based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResource(resourceId, validate, callback)</td>
    <td style="padding:15px">Delete a resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResource(resourceId, resource, validate, obfuscate, callback)</td>
    <td style="padding:15px">Patch update a resource based on the (partial) data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResources(productId, domainId, resourceProviderId, resourceTypeId, providerResourceId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, obfuscate, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the resources created within the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListResources(productId, domainId, resourceProviderId, resourceTypeId, providerResourceId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, obfuscate, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the resources created within the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResource(resource, validate, obfuscate, callback)</td>
    <td style="padding:15px">Create a new resource in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auditResources(productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, q, p, tags, callback)</td>
    <td style="padding:15px">Execute audit requests for all resources in the scope</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countResources(productId, domainId, exactTypeId, limit, callback)</td>
    <td style="padding:15px">Deprecated: (use /count-filtered) Count resources</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountResources(productId, domainId, exactTypeId, limit, callback)</td>
    <td style="padding:15px">Deprecated: (use /count-filtered) Count resources</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBpocoreMarketApiV1ResourcesCountFiltered(productId, domainId, resourceProviderId, resourceTypeId, providerResourceId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, limit, callback)</td>
    <td style="padding:15px">Count resources in the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/count-filtered?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headBpocoreMarketApiV1ResourcesCountFiltered(productId, domainId, resourceProviderId, resourceTypeId, providerResourceId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, limit, callback)</td>
    <td style="padding:15px">Count resources in the Market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/count-filtered?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceHistory(resourceId, after, before, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all history of given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListResourceHistory(resourceId, after, before, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all history of given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignResourceToSubDomain(resourceId, subDomainInfo, callback)</td>
    <td style="padding:15px">Assign the resource to a subdomain</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/sub-domain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceOperations(resourceId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all custom operations invoked for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListResourceOperations(resourceId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all custom operations invoked for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResourceOperation(resourceId, operation, callback)</td>
    <td style="padding:15px">Create an operation for a specific resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDeleteResourceOperations(resourceId, beforeDate, callback)</td>
    <td style="padding:15px">Bulk delete resource operations</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countResourceOperations(resourceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count custom operations invoked for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountResourceOperations(resourceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count custom operations invoked for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceOperation(resourceId, operationId, minRevision, callback)</td>
    <td style="padding:15px">Get details of a specific operation for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetResourceOperation(resourceId, operationId, minRevision, callback)</td>
    <td style="padding:15px">Get details of a specific operation for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourceOperation(resourceId, operationId, operation, callback)</td>
    <td style="padding:15px">Update attributes of a specific operation for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceOperation(resourceId, operationId, callback)</td>
    <td style="padding:15px">Delete a resource operation</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourceOperation(resourceId, operationId, operation, callback)</td>
    <td style="padding:15px">Patch the attributes of a specific operation for a given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">promoteResourceAssembly(resourceId, resource, callback)</td>
    <td style="padding:15px">Promote the Assembly to a managed service</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/promote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateResourceOperationCreation(resourceId, full, operation, callback)</td>
    <td style="padding:15px">Execute a validation request for a proposed resource operation creation</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateResourceOperationChange(resourceId, operationId, method = 'PATCH', operation, callback)</td>
    <td style="padding:15px">Execute a change validation request for the specified resource operation</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/operations/{pathv2}/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listResourceInterfaces(resourceId, q, p, callback)</td>
    <td style="padding:15px">Get all interfaces of given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListResourceInterfaces(resourceId, q, p, callback)</td>
    <td style="padding:15px">Get all interfaces of given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countResourceInterfaces(resourceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count interfaces of given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/interfaces/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountResourceInterfaces(resourceId, q, p, limit, callback)</td>
    <td style="padding:15px">Count interfaces of given resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/interfaces/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reassembleResourceAssembly(resourceId, callback)</td>
    <td style="padding:15px">Reassemble an Assembly</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/reassemble?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDependenciesOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, obfuscate, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all dependencies of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListDependenciesOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, obfuscate, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all dependencies of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDependentsOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, obfuscate, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all dependents of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListDependentsOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, obfuscate, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all dependents of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countDependenciesOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, limit, callback)</td>
    <td style="padding:15px">Count dependencies of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependencies/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountDependenciesOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, limit, callback)</td>
    <td style="padding:15px">Count dependencies of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependencies/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countDependentsOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, limit, callback)</td>
    <td style="padding:15px">Count dependents of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountDependentsOfResource(resourceId, recursive, productId, domainId, resourceProviderId, resourceTypeId, exactTypeId, subDomainId, applicationSliceId, q, p, tags, limit, callback)</td>
    <td style="padding:15px">Count dependents of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/dependents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceObserved(resourceId, obfuscate, callback)</td>
    <td style="padding:15px">Get the observed state of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/observed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetResourceObserved(resourceId, obfuscate, callback)</td>
    <td style="padding:15px">Get the observed state of the resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/observed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putResourceObserved(resourceId, validate, resource, obfuscate, callback)</td>
    <td style="padding:15px">Update the resource to reflect it's observed state</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/observed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourceObserved(resourceId, validate, resource, obfuscate, callback)</td>
    <td style="padding:15px">Update the resource to reflect it's observed state</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/observed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateResourceCreation(full, custom, resource, callback)</td>
    <td style="padding:15px">Execute a validation request for a proposed resource creation</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateResourceChange(resourceId, method = 'PATCH', custom, resource, callback)</td>
    <td style="padding:15px">Execute a change validation request for the specified resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncResource(resourceId, full, callback)</td>
    <td style="padding:15px">Execute a resync request for the specified resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auditResource(resourceId, callback)</td>
    <td style="padding:15px">Execute an audit request for the specified resource</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resources/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourcesList(name, customerModifiable, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all resources.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourcesCreate(resource, name, callback)</td>
    <td style="padding:15px">Create a new resource.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourcesRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific resource by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourcesUpdate(uuid, resource, name, callback)</td>
    <td style="padding:15px">Defines resources for the permission.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourcesPartialUpdate(uuid, resource, name, callback)</td>
    <td style="padding:15px">Defines resources for the permission.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resourcesDelete(uuid, callback)</td>
    <td style="padding:15px">Defines resources for the permission.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResyncStatus(resyncId, callback)</td>
    <td style="padding:15px">Get the status of a resync request</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resyncs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetResyncStatus(resyncId, callback)</td>
    <td style="padding:15px">Get the status of a resync request</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/resyncs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSharingPermissions(q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the sharing permissions in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListSharingPermissions(q, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the sharing permissions in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharingPermission(sharingPermissionRequest, callback)</td>
    <td style="padding:15px">Create a sharing permission in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSharingPermissions(q, limit, callback)</td>
    <td style="padding:15px">Count the sharing permissions in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountSharingPermissions(q, limit, callback)</td>
    <td style="padding:15px">Count the sharing permissions in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharingPermission(sharingPermissionId, callback)</td>
    <td style="padding:15px">Get a specific sharing permission from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetSharingPermission(sharingPermissionId, callback)</td>
    <td style="padding:15px">Get a specific sharing permission from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharingPermission(sharingPermissionId, sharingPermissionRequest, callback)</td>
    <td style="padding:15px">Update a sharing permission in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharingPermission(sharingPermissionId, callback)</td>
    <td style="padding:15px">Delete a sharing permission from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSharingPermission(sharingPermissionId, sharingPermissionRequest, callback)</td>
    <td style="padding:15px">Patch update a sharing permission in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sharing-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubDomains(domainId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the subdomains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListSubDomains(domainId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the subdomains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubDomain(subDomainRequest, callback)</td>
    <td style="padding:15px">Create a subdomain in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubDomain(subDomainId, callback)</td>
    <td style="padding:15px">Get a specific subdomain from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetSubDomain(subDomainId, callback)</td>
    <td style="padding:15px">Get a specific subdomain from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSubDomain(subDomainId, subDomainRequest, callback)</td>
    <td style="padding:15px">Update a subdomain in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubDomain(subDomainId, callback)</td>
    <td style="padding:15px">Delete a subdomain from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSubDomain(subDomainId, subDomainRequest, callback)</td>
    <td style="padding:15px">Patch update a subdomain in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listContainers(subDomainId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all container subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListContainers(subDomainId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all container subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listContainees(subDomainId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all containee subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListContainees(subDomainId, recursive, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Lists all containee subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResourcesToSubDomain(subDomainId, resources, callback)</td>
    <td style="padding:15px">Add resources to a subdomain</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/add-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countContainees(subDomainId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count containee subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containees/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountContainees(subDomainId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count containee subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containees/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countContainers(subDomainId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count container subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containers/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountContainers(subDomainId, recursive, q, p, limit, callback)</td>
    <td style="padding:15px">Count container subdomains of the given subdomain id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/{pathv1}/containers/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countSubDomains(domainId, q, p, limit, callback)</td>
    <td style="padding:15px">Count the subdomains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountSubDomains(domainId, q, p, limit, callback)</td>
    <td style="padding:15px">Count the subdomains in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/sub-domains/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagValues(tagKey, q, callback)</td>
    <td style="padding:15px">Get all of the tag values for a given tag key</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListTagValues(tagKey, q, callback)</td>
    <td style="padding:15px">Get all of the tag values for a given tag key</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTagValue(tagKey, tagValueObject, callback)</td>
    <td style="padding:15px">Create a tag value for a specific tag key</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagValue(tagKey, tagValue, callback)</td>
    <td style="padding:15px">Get a specific tag value from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetTagValue(tagKey, tagValue, callback)</td>
    <td style="padding:15px">Get a specific tag value from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTagValue(tagKey, tagValue, tagValueObject, callback)</td>
    <td style="padding:15px">Update a tag value based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagValue(tagKey, tagValue, callback)</td>
    <td style="padding:15px">Delete a tag value</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTagValue(tagKey, tagValue, tagValueObject, callback)</td>
    <td style="padding:15px">Patch update a tag value based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagKeys(q, callback)</td>
    <td style="padding:15px">Get all of the tag keys in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListTagKeys(q, callback)</td>
    <td style="padding:15px">Get all of the tag keys in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTagKey(tagKeyObject, callback)</td>
    <td style="padding:15px">Create a tag key in the market using the information in the provided tag key object</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countTagKeys(q, limit, callback)</td>
    <td style="padding:15px">Count the tag keys in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountTagKeys(q, limit, callback)</td>
    <td style="padding:15px">Count the tag keys in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagKey(tagKey, callback)</td>
    <td style="padding:15px">Get a specific tag key from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetTagKey(tagKey, callback)</td>
    <td style="padding:15px">Get a specific tag key from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTagKey(tagKey, tagKeyObject, callback)</td>
    <td style="padding:15px">Update a tag key in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagKey(tagKey, callback)</td>
    <td style="padding:15px">Delete a tag key from the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTagKey(tagKey, tagKeyObject, callback)</td>
    <td style="padding:15px">Patch update a tag key in the market based on the data in the provided instance</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countTagValues(tagKey, q, limit, callback)</td>
    <td style="padding:15px">Count tag values for a given tag key</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountTagValues(tagKey, q, limit, callback)</td>
    <td style="padding:15px">Count tag values for a given tag key</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tag-keys/{pathv1}/tag-values/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncTenants(callback)</td>
    <td style="padding:15px">Do a full reset of the tenants in bpocore to mirror the current state of Tron</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tenants/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(tenantId, callback)</td>
    <td style="padding:15px">Get a tenant by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetTenant(tenantId, callback)</td>
    <td style="padding:15px">Get a tenant by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tenantsList(uuid, isMaster, isActive, name, displayName, displayNameContains, displayNameStartswith, search, ordering, page, limit, callback)</td>
    <td style="padding:15px">List all tenants.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tenantsCreate(clientInactivityTime, tokenExpirationTime, dormancyDays, displayName, description, parent, type, concurrentSessionMaxPerTenant, concurrentSessionMax, emailDormancyWeeklyNotifications, emailDormancyDailyNotifications, name, isActive, message, callback)</td>
    <td style="padding:15px">Create a new Tenant.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tenantsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific tenant by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tenantsUpdate(uuid, clientInactivityTime, tokenExpirationTime, dormancyDays, displayName, description, parent, type, concurrentSessionMaxPerTenant, concurrentSessionMax, emailDormancyWeeklyNotifications, emailDormancyDailyNotifications, name, isActive, message, callback)</td>
    <td style="padding:15px">Replace the tenant with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tenantsPartialUpdate(uuid, clientInactivityTime, tokenExpirationTime, dormancyDays, displayName, description, parent, type, concurrentSessionMaxPerTenant, concurrentSessionMax, emailDormancyWeeklyNotifications, emailDormancyDailyNotifications, name, isActive, message, callback)</td>
    <td style="padding:15px">Update some values for the tenant with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTypeLayerVersion(callback)</td>
    <td style="padding:15px">Get information on the type layer itself</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/realm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetTypeLayerVersion(callback)</td>
    <td style="padding:15px">Get information on the type layer itself</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/realm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTypeArtifacts(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the type artifacts in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListTypeArtifacts(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">Get all of the type artifacts in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countTypeArtifacts(q, p, limit, callback)</td>
    <td style="padding:15px">Count the type artifacts in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountTypeArtifacts(q, p, limit, callback)</td>
    <td style="padding:15px">Count the type artifacts in the market</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDefinitionRealm(callback)</td>
    <td style="padding:15px">Validate the current model in the definition realm</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/realm/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTypeArtifact(typeArtifactUri, callback)</td>
    <td style="padding:15px">Get a type artifact by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetTypeArtifact(typeArtifactUri, callback)</td>
    <td style="padding:15px">Get a type artifact by Id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/market/api/v1/type-artifacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBackupImages(nename, netype, callback)</td>
    <td style="padding:15px">Retrieve all available Backups given the Network Element.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/backupImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeBackupRestoreStatus(callback)</td>
    <td style="padding:15px">Retrieve Backup and Restore operation status of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/backupRestoreStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConfigMgmtBatches(callback)</td>
    <td style="padding:15px">Retrieve existing Batches.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/batches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMgmtBatch(batchId, callback)</td>
    <td style="padding:15px">Retrieve Batch given the Batch Id.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/batches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBatches(body, batchId, callback)</td>
    <td style="padding:15px">Updates Batch attributes for a batch</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/batches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMgmtBatch(body, callback)</td>
    <td style="padding:15px">Delete the Batches given the batchRemoveRequest.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/deleteBatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchedNes(batchId, callback)</td>
    <td style="padding:15px">Retrieve Network Elements given the Batch Id.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/nes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPConfiguration(nename, callback)</td>
    <td style="padding:15px">GET bgp configuration</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/bgpconfiguration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBGPSession(body, callback)</td>
    <td style="padding:15px">Create Edit BGP session</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/bgpsessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignSegmentIdentifier(assignmentType = 'index', typeGroup, softwareVersion, networkConstructId, callback)</td>
    <td style="padding:15px">Assign segment identifier to segment routing enabled network elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/bulkOperations/assignSegmentIdentifier?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNEResourcesOfOperation(operationType, startTime, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve network construct resources of a bulk operation</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/bulkOperations/getNEResourcesOfOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveProgressStatusOfOperations(operationType, startTime, callback)</td>
    <td style="padding:15px">Retrieve progress status of bulk operation</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/bulkOperations/getProgressOfOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveStatusOfOperations(id, type, state, fromRecentOperationTime, toRecentOperationTime, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve status of bulk operations</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/bulkOperations/getStatusOfOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningSettings(functionParam, neNames, typeGroup, resourceType, ipAddress, metaDataFields, sortBy, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Commissioning Settings of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyFunctionAndProfile(body, callback)</td>
    <td style="padding:15px">Apply a commissioning profile of selected Function on a list of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningRequest(functionParam = 'ISIS', body, callback)</td>
    <td style="padding:15px">Delete Commissioning Settings from Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioning/{pathv1}/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningFunctions(callback)</td>
    <td style="padding:15px">Get a list of commissioning functions</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioningFunctions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningProfileMetaDataForFunction(functionParam, typeGroups, callback)</td>
    <td style="padding:15px">Get MetaData For Profile from Input Commissioning Function</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioningFunctions/profileMetaData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommissioningProfiles(searchText, searchFields, id, typeGroups, functionParam, profileName, description, profileType, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Get a list of commissioning profiles</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioningProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCommissioningProfile(body, callback)</td>
    <td style="padding:15px">Creates a new commissioning profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioningProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommissioningProfile(commissioningProfileId, callback)</td>
    <td style="padding:15px">Delete the commissioning profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioningProfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCommissioningProfile(commissioningProfileId, body, callback)</td>
    <td style="padding:15px">Perform update of commissioning profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/commissioningProfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomScripts(typeGroup, protocolType = 'cli', callback)</td>
    <td style="padding:15px">Retrieve custom scripts</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/customScripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCustomScript(file, typeGroup, protocolType = 'cli', scriptName, description, callback)</td>
    <td style="padding:15px">Upload a custom script definitions</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/customScripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomScriptsStatus(callback)</td>
    <td style="padding:15px">Retrieve custom script status of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/customScripts/scriptStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCustomScript(id, callback)</td>
    <td style="padding:15px">download a custom script definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/customScripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomScript(id, callback)</td>
    <td style="padding:15px">deleteCustomScript a script definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/customScripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDOCs(networkConstructNames, callback)</td>
    <td style="padding:15px">Retrieve DOC configuration of NEs</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/doc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDOCs(body, callback)</td>
    <td style="padding:15px">Edit DOCs configuration (enable/disable) on NEs</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/doc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConfigMgmtJobs(scriptName, cmdFileName, callback)</td>
    <td style="padding:15px">Retrieve job status of all executed or scheduled jobs.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConfigMgmtJob(body, callback)</td>
    <td style="padding:15px">Execute network element configuration job</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigMgmtJob(jobId, callback)</td>
    <td style="padding:15px">Retrieve the job status given the job id.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigMgmtJob(jobId, callback)</td>
    <td style="padding:15px">Delete a job</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigMgmtJobPatch(jobId, body, callback)</td>
    <td style="padding:15px">Update a job based on the jobId</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lagProvisioning(body, callback)</td>
    <td style="padding:15px">LAG Provisioning operation</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/lag/provisioning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBFDSessions(sourceNE, remoteNE, callback)</td>
    <td style="padding:15px">Get BFD session</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/mhopbfdsessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBFDSession(body, callback)</td>
    <td style="padding:15px">Create BFD session</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/mhopbfdsessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBFDSession(body, callback)</td>
    <td style="padding:15px">Delete BFD session</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/mhopbfdsessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConfigMgmtBackup(body, callback)</td>
    <td style="padding:15px">Execute network element NE Backup</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/neBackups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkElementsMaintenance(searchText, searchFields, id, name, longName, displayName, sessionId, resourceType, typeGroup, ipAddress, associationState, syncState, physicalLocationId, subnetName, softwareActiveVersion, recentOperation, nextOperation, recentOperationState, recentOperationProgressState, softwareAvailableVersion, backupSchedule, upgradeSchedule, customScriptSchedule, fromRecentOperationTime, toRecentOperationTime, fromNextOperationTime, toNextOperationTime, resourcePartitionInfo, associationStateQualifier, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Maintenance details of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/neMaintenanceDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configMgmtRestore(body, callback)</td>
    <td style="padding:15px">Execute network element Restore</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/neRestores?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigmgmtApiV1NeUpgradeDetails(searchText, searchFields, id, name, longName, displayName, sessionId, resourceType, typeGroup, ipAddress, associationState, syncState, physicalLocationId, subnetName, softwareActiveVersion, softwareAvailableVersion, upgradeSchedules, releaseMgmtSchedules, upgradeStage, upgradeStatus, manualInvokeOnCards, resourcePartitionInfo, associationStateQualifier, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve upgrade details of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/neUpgradeDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeUpgradeStatus(ncid, operationtype, callback)</td>
    <td style="padding:15px">Retrieve upgrade operation status of a Network Element based on ncId and operation type</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/neUpgradeStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeStatus(callback)</td>
    <td style="padding:15px">Retrieve upgrade operation status of Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgradeStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">conflictingEntities(identifier, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Conflicting Entities.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/packetmgmt/conflictingEntities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">segmentRoutingEnabledNetworkElements(searchText, searchFields, id, name, displayName, resourceType, typeGroup, ipAddress, softwareActiveVersion, loopbackIpAddress, nodeSid, associationState, recentOperation, recentOperationState, recentOperationProgressState, fromRecentOperationTime, toRecentOperationTime, resourcePartitionInfo, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Segment Routing Enabled Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/packetmgmt/segmentRoutingEnabledNetworkElements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createParentPolicerOperation(body, callback)</td>
    <td style="padding:15px">Create Parent Policer</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/parentPolicer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateParentPolicerOperation(body, callback)</td>
    <td style="padding:15px">Update Parent Policer</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/parentPolicer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteParentPolicerOperation(body, callback)</td>
    <td style="padding:15px">Delete Parent Policer</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/parentPolicer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deProvisionPort(body, callback)</td>
    <td style="padding:15px">De-Provision a port</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/deProvision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPortRateMappings(callback)</td>
    <td style="padding:15px">Retrieve pec code maaping definitions</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPortRateMappingFile(file, name, callback)</td>
    <td style="padding:15px">Upload a pec code mapping definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadPortRateMappping(id, callback)</td>
    <td style="padding:15px">download a pec code mapping definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortRateMapping(id, callback)</td>
    <td style="padding:15px">Delete pec code mapping definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPecCodeMappings(callback)</td>
    <td style="padding:15px">Retrieve pec code definitions</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/pecs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPecCodeFile(file, name, callback)</td>
    <td style="padding:15px">Upload a pec code definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/pecs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadPecCodeMapping(id, callback)</td>
    <td style="padding:15px">Download a pec code definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/pecs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePecCodeMapping(id, callback)</td>
    <td style="padding:15px">Delete pecCode definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/pecs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionPort(body, callback)</td>
    <td style="padding:15px">Port provisioning</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/port/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFtpProfiles(profileType = 'backup_restore', neType, callback)</td>
    <td style="padding:15px">Retrieve profiles</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFtpProfile(body, callback)</td>
    <td style="padding:15px">Trigger the creation of NE Maintenance profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFtpProfileAssociations(profileId, callback)</td>
    <td style="padding:15px">Retrieve profile association information</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/profiles/associations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFtpProfileBasedOnId(profileId, callback)</td>
    <td style="padding:15px">Retrieve profile information</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProfile(profileId, callback)</td>
    <td style="padding:15px">Delete a profile.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFtpProfile(profileId, body, callback)</td>
    <td style="padding:15px">Update profile information</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lagQueueProvisioning(body, callback)</td>
    <td style="padding:15px">Queue Provisioning operation</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/queue/provisioning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSchedulePOST(body, callback)</td>
    <td style="padding:15px">Assign Schedules to NE Ip with partition id</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedule/assignNE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigmgmtApiV1ScheduleAssignNE(body, callback)</td>
    <td style="padding:15px">Delete a schedule assigned to NE during pre-enrollment.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedule/assignNE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedules(searchText, searchFields, scheduleId, name, type, state, profileName, releaseNumber, typeGroup, frequency, weekDays, monday, tuesday, wednesday, thursday, friday, saturday, sunday, fromRecentOperationTime, toRecentOperationTime, fromNextOperationTime, toNextOperationTime, resourcePartitionInfo, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Schedule details</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSchedule(body, callback)</td>
    <td style="padding:15px">Create Schedule</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScheduleNes(body, callback)</td>
    <td style="padding:15px">Updates Schedule Nes</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedules/nes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSchedule(body, scheduleId, callback)</td>
    <td style="padding:15px">Updates Schedule attributes</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigmgmtApiV1SchedulesScheduleId(scheduleId, callback)</td>
    <td style="padding:15px">Delete a schedule with given schedule id.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptExecutionDetails(searchText, searchFields, id, operationType, jobid, scriptName, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Get a list of Script Execution Details</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptExecutionDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigmgmtApiV1ScriptExecutionDetailsJobID(jobID, callback)</td>
    <td style="padding:15px">Download output of custom script</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptExecutionDetails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigmgmtApiV1ScriptProfiles(callback)</td>
    <td style="padding:15px">Retrieve Script Profiles</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadScriptProfile(file, profileName, callback)</td>
    <td style="padding:15px">Upload a script profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadScriptProfile(id, content = 'file', callback)</td>
    <td style="padding:15px">download a script profile definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptProfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScriptProfile(id, body, callback)</td>
    <td style="padding:15px">Update a script profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptProfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScriptProfile(id, callback)</td>
    <td style="padding:15px">Delete script profile definition</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptProfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllScripts(callback)</td>
    <td style="padding:15px">get all script definitions</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptByName(scriptName, callback)</td>
    <td style="padding:15px">Get script by Script Name</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptsByName/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllScriptsByTypeGroup(typeGroup, callback)</td>
    <td style="padding:15px">Get scripts belongs to the given TypeGroup</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/scriptsByTypeGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportResourceByType(types, ncId, callback)</td>
    <td style="padding:15px">Get a next possible id for given resourceType</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/transportResource/generateResourceId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyFPGA(body, callback)</td>
    <td style="padding:15px">Apply FPGA Upgrade</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/applyFPGA?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveFPGADetails(networkConstructName, aID, callback)</td>
    <td style="padding:15px">Retrieve FPGA Details of AID on given NE</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/fpgaDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveFPGAInventory(networkConstructName, callback)</td>
    <td style="padding:15px">Retrieve FPGA Inventory of given NE</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/fpgaInventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualInvoke(body, callback)</td>
    <td style="padding:15px">Manual Invoke of cards</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/manualInvoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualResume(body, callback)</td>
    <td style="padding:15px">Manual resume of upgrade</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveUpgradeStatus(networkConstructName, callback)</td>
    <td style="padding:15px">Retrieve upgrade status of Network Element</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/rtrvUpgradeStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveCircuitPacks(networkConstructName, callback)</td>
    <td style="padding:15px">Retrieve upgrade status of Circuit Packs of Network Element</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopUpgrade(body, callback)</td>
    <td style="padding:15px">Stop NE upgrade</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualSuspend(body, callback)</td>
    <td style="padding:15px">Manual suspend of upgrade</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/upgrade/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnResourceByType(types, ncId, callback)</td>
    <td style="padding:15px">Get a next possible id for given resourceType</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v1/vpnResources/generateResourceId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigmgmtApiV2CustomScripts(searchText, searchFields, scriptId, typeGroup, protocolType, scriptName, description, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Custom Scripts</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/customScripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchedNesByBatchType(batchType = 'BACKUP', callback)</td>
    <td style="padding:15px">Retrieve Network Elements given the Batch Type.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/nes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchedNesByBatchId(batchId, callback)</td>
    <td style="padding:15px">Retrieve Network Elements given the Batch Id.</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/nes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigmgmtApiV2Profiles(searchText, searchFields, profileId, profileType, typeGroup, ipAddress, name, releaseNumber, storageMethod, port, sshPort, protocolType, sortBy, metaDataFields, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve profiles</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigmgmtApiV2Profiles(body, callback)</td>
    <td style="padding:15px">Trigger the creation of NE Maintenance profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfigmgmtApiV2ProfilesProfileId(profileId, body, callback)</td>
    <td style="padding:15px">Update profile information</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptProfiles(searchText, searchFields, id, profileName, sortBy, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve Script Profiles</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/scriptProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScriptProfile(body, callback)</td>
    <td style="padding:15px">Create a script profile</td>
    <td style="padding:15px">{base_path}/{version}/configmgmt/api/v2/scriptProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(neNames, lastOperationState, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve fallback user info for NEs</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create fallback user accounts on NEs</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(body, callback)</td>
    <td style="padding:15px">Delete fallback user accounts on NEs</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordPolicy(callback)</td>
    <td style="padding:15px">Retrieve password generation values</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers/passwordPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePasswordPolicy(body, callback)</td>
    <td style="padding:15px">Update password policy information</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers/passwordPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetFallbackPasswords(body, callback)</td>
    <td style="padding:15px">Reset fallback passwords on NEs</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers/resetPasswords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordResetSchedules(callback)</td>
    <td style="padding:15px">Retrieve password reset schedules</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers/resetPasswords/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordResetSchedule(id, callback)</td>
    <td style="padding:15px">Retrieve specified password reset schedule</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers/resetPasswords/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePasswordResetSchedule(id, callback)</td>
    <td style="padding:15px">Delete specified password reset schedule</td>
    <td style="padding:15px">{base_path}/{version}/fallback-user-mgmt/api/v1/fallbackUsers/resetPasswords/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNEConnectionProfiles(typeGroup, protocol, callback)</td>
    <td style="padding:15px">Get all NE Connection Profiles for a type group</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/neprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNEConnectionProfile(body, callback)</td>
    <td style="padding:15px">Create a NE Connection Profile</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/neprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNEConnectionProfile(id, callback)</td>
    <td style="padding:15px">Get the NE Connection Profile with the specified id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNEConnectionProfile(id, body, callback)</td>
    <td style="padding:15px">Create or update an NE Connection Profile with the specified id.</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNEConnectionProfile(id, callback)</td>
    <td style="padding:15px">Delete the NE Connection Profile with the specified id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryApiV1Neprofiles(typeGroup, protocol, callback)</td>
    <td style="padding:15px">Get all NE Connection Profiles for a type group</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v1/neprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoveryApiV1Neprofiles(body, callback)</td>
    <td style="padding:15px">Create a NE Connection Profile</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v1/neprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryApiV1NeprofilesId(id, callback)</td>
    <td style="padding:15px">Get the NE Connection Profile with the specified id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v1/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDiscoveryApiV1NeprofilesId(id, body, callback)</td>
    <td style="padding:15px">Update the NE Connection Profile with the specified id.</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v1/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDiscoveryApiV1NeprofilesId(id, callback)</td>
    <td style="padding:15px">Delete the NE Connection Profile with the specified id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v1/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDiscoveryApiV2NeprofilesId(id, body, callback)</td>
    <td style="padding:15px">Create or update an NE Connection Profile with the specified id.</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v2/neprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConstructs(states, name, longName, profileName, ipAddress, aliasName, aliasValue, offset, limit, typeGroup, resourceType, shortResourceType, associationState, searchText, searchFields, sortBy, metaDataFields, displayState, displayName, displayPreferredConnectionName, displayPartitions, displayAlternativeConnectionIPs, displayAlternativeConnectionNames, profileId, associationStateQualifier, associationStateQualifierReason, resourcePartitionInfo, callback)</td>
    <td style="padding:15px">Get a list of  management sessions</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/managementSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postManagementSession(body, callback)</td>
    <td style="padding:15px">Creates a new management Session and optionally triggers the enrollment</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/managementSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementSessionById(sessionId, callback)</td>
    <td style="padding:15px">Retrieves a management session by its corresponding id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/managementSessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteManagementSession(sessionId, callback)</td>
    <td style="padding:15px">Triggers the de-enrollment of a network element or deletes a pending enrollment</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/managementSessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOperation(sessionId, body, callback)</td>
    <td style="padding:15px">perform operations of update, connect or resync on a management session resource</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/managementSessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementSessions(states, name, ipAddress, aliasName, aliasValue, offset, limit, callback)</td>
    <td style="padding:15px">Get a list of  management sessions</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoveryApiV3ManagementSessions(body, callback)</td>
    <td style="padding:15px">Creates a new management Session and optionally triggers the enrollment</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementSessionBySessionId(sessionId, callback)</td>
    <td style="padding:15px">Retrieves a management session by its corresponding id</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDiscoveryApiV3ManagementSessionsSessionId(sessionId, callback)</td>
    <td style="padding:15px">Triggers the de-enrollment of a network element or deletes a pending enrollment</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEnroll(sessionId, body, callback)</td>
    <td style="padding:15px">Trigger the enrollment or resynchronization of a management session</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions/{pathv1}/discoveryState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEnrollIP(sessionId, body, callback)</td>
    <td style="padding:15px">Update the IP address of the pending management session</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions/{pathv1}/ipAddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEnrollNeProfile(sessionId, body, callback)</td>
    <td style="padding:15px">Update the NE connection profile id of the pending management session</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v3/managementSessions/{pathv1}/neProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryApiV4ManagementSessions(states, name, longName, profileName, ipAddress, aliasName, aliasValue, offset, limit, typeGroup, resourceType, shortResourceType, associationState, searchText, searchFields, sortBy, metaDataFields, displayState, displayName, displayPreferredConnectionName, displayPartitions, displayAlternativeConnectionIPs, displayAlternativeConnectionNames, profileId, associationStateQualifier, associationStateQualifierReason, resourcePartitionInfo, callback)</td>
    <td style="padding:15px">Get a list of  management sessions</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v4/managementSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDiscoveryApiV4ManagementSessionsSessionId(sessionId, body, callback)</td>
    <td style="padding:15px">perform operations of update, connect or resync on a management session resource</td>
    <td style="padding:15px">{base_path}/{version}/discovery/api/v4/managementSessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiNetworkConstructs(name, ipAddress, identifierKey, identifierValue, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, networkConstructType, ssteType, concrete, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkConstruct(body, callback)</td>
    <td style="padding:15px">Creates or updates network construct with only userData, resourcePartitionInfo and/or networkConstr</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConstruct(id, fields, include, callback)</td>
    <td style="padding:15px">Retrieves a Network Construct given its corresponding identifier.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNCById(id, callback)</td>
    <td style="padding:15px">Deletes a specific network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiNetworkConstructsId(id, body, callback)</td>
    <td style="padding:15px">Performs update operations on a network construct resource</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNCExpectations(id, body, callback)</td>
    <td style="padding:15px">Create an NC Expectation given the nc id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNCExpectations(id, expId, body, callback)</td>
    <td style="padding:15px">Create or update an NC Expectation given the nc id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExpectation(id, expectationId, body, callback)</td>
    <td style="padding:15px">Performs update operations on a network construct expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNCExpectationById(id, networkConstructExpectationId, callback)</td>
    <td style="padding:15px">Retrieve an NC Expectation given the nc id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNCExpectationById(id, networkConstructExpectationId, callback)</td>
    <td style="padding:15px">Delete an NC Expectation given the nc id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">realizeNetworkConstruct(id, networkConstructExpectationId, callback)</td>
    <td style="padding:15px">Realize an NC Expectation given the ncId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNCExpectationAttributeValue(id, networkConstructExpectationId, attributeName, body, callback)</td>
    <td style="padding:15px">Update to an existing NC Expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/expectations/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNCIdentifier(id, identifierRO, callback)</td>
    <td style="padding:15px">Creates or updates a REST identifier on a Network Construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNCIdentifier(id, identifierRO, callback)</td>
    <td style="padding:15px">Deletes a REST identifier on a Network Construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNCIdentifiersByKey(id, identifierKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an NC UserData Identifier to a given network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNCIdentifiers(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an userData Identifier from a  given network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConstructPlan(id, callback)</td>
    <td style="padding:15px">Retrieves a Network Construct planned for the given identifier</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/networkConstructPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPlannedNC(id, body, callback)</td>
    <td style="padding:15px">Performs L2 Nole Role assignment operation on a network construct resource</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/networkConstructPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNCPhysicalLocation(id, physicalLocationId, body, callback)</td>
    <td style="padding:15px">Updates an NC physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/physicalLocation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePhysicalLocationFromNC(id, physicalLocationId, callback)</td>
    <td style="padding:15px">Delete a physical location from a given network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/networkConstructs/{pathv1}/physicalLocation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3NetworkConstructs(name, ipAddress, aliasName, aliasValue, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, include = 'expectations', physicalLocationId, networkConstructType = 'networkElement', concrete, fields, searchText, searchFields, offset, limit, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3NetworkConstructsId(id, fields, include, callback)</td>
    <td style="padding:15px">Retrieves a Network Construct given its corresponding identifier</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3NetworkConstructsId(id, callback)</td>
    <td style="padding:15px">Deletes a specific network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3NetworkConstructsId(id, body, callback)</td>
    <td style="padding:15px">Performs update operations on a network construct resource</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3NetworkConstructsIdExpectations(id, body, callback)</td>
    <td style="padding:15px">Create an NC Expectation given the nc id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3NetworkConstructsIdExpectationsExpectationId(id, expectationId, body, callback)</td>
    <td style="padding:15px">Performs update operations on a network construct expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3NetworkConstructsIdExpectationsNetworkConstructExpectationId(id, networkConstructExpectationId, callback)</td>
    <td style="padding:15px">Retrieve an NC Expectation given the nc id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3NetworkConstructsIdExpectationsNetworkConstructExpectationId(id, networkConstructExpectationId, callback)</td>
    <td style="padding:15px">Delete an NC Expectation given the nc id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3NetworkConstructsIdExpectationsNetworkConstructExpectationIdAttributeName(id, networkConstructExpectationId, attributeName, body, callback)</td>
    <td style="padding:15px">Update to an existing NC Expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/expectations/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNCIdentifiers(id, identifierKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an NC UserData Identifier to a given network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3NetworkConstructsIdIdentifiersIdentifierKey(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an userData Identifier from a  given network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3NetworkConstructsIdNetworkConstructPlanned(id, callback)</td>
    <td style="padding:15px">Retrieves a Network Construct planned for the given identifier</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/networkConstructPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3NetworkConstructsIdNetworkConstructPlanned(id, body, callback)</td>
    <td style="padding:15px">Performs L2 Node Role assignment operation and updates the Physical location on a network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/networkConstructs/{pathv1}/networkConstructPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV4NetworkConstructs(id, searchText, searchFields, resourceState, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, name, displayName, identifierKey, identifierValue, concrete, modelType, ipAddress, networkConstructType, resourceType, associationState, syncState, softwareVersion, displaySyncState, subnetName, macAddress, typeGroup, slteType, ssteType, resourcePartitionInfo, tags, fields, sortBy, offset, limit, metaDataFields, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV4NetworkConstructsId(id, fields, include, callback)</td>
    <td style="padding:15px">Retrieves a Network Construct given its corresponding identifier</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV4NetworkConstructsIdExpectationsExpId(id, expId, body, callback)</td>
    <td style="padding:15px">Create or update an NC Expectation given the nc id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV4NetworkConstructsIdIdentifiers(id, identifierRO, callback)</td>
    <td style="padding:15px">Puts or updates a REST identifier on a Network Construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV4NetworkConstructsIdIdentifiers(id, identifierRO, callback)</td>
    <td style="padding:15px">Deletes a REST identifier on a Network Construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV4NetworkConstructsIdPhysicalLocationPhysicalLocationId(id, physicalLocationId, body, callback)</td>
    <td style="padding:15px">Updates an NC physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs/{pathv1}/physicalLocation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV4NetworkConstructsIdPhysicalLocationPhysicalLocationId(id, physicalLocationId, callback)</td>
    <td style="padding:15px">Delete a physical location from a  given network construct</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/networkConstructs/{pathv1}/physicalLocation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConstructsV5(name, ipAddress, identifierKey, identifierValue, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, networkConstructType, concrete, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV5NetworkConstructs(body, callback)</td>
    <td style="padding:15px">Creates or updates network construct with only userData, resourcePartitionInfo and/or networkConstr</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV5NetworkConstructsIdExpectationsNetworkConstructExpectationIdRealize(id, networkConstructExpectationId, callback)</td>
    <td style="padding:15px">Realize an NC Expectation given the ncId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/networkConstructs/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConstructsV6(name, ipAddress, identifierKey, identifierValue, sessionId, networkConstructExpectationsEquipmentIntentId, networkConstructExpectationsServiceIntentId, physicalLocationId, networkConstructType, ssteType, concrete, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Get list of discovered Network Construct(s) satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/networkConstructs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalLocations(include, offset, limit, callback)</td>
    <td style="padding:15px">Get list of physical locations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPhysicalLocation(body, callback)</td>
    <td style="padding:15px">Creates or updates a physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalLocationById(id, include, callback)</td>
    <td style="padding:15px">Retrieve a physical location with given id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPhysicalLocationExpectations(id, body, callback)</td>
    <td style="padding:15px">Create an physical location expectation given the physical location id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalLocationExpectationById(id, expectationId, callback)</td>
    <td style="padding:15px">Retrieve an physical locations Expectation given the physical locations id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePhysicalLocationExpectationById(id, expectationId, callback)</td>
    <td style="padding:15px">Delete an physical location expectation given the physical location id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePhysicalLocationIdentifiers(id, identifierKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an Identifier to a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePhysicalLocationIdentifiers(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an Identifier from a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiPhysicalLocationsIdUserDataUserDataKey(id, userDataKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an UserData to a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePhysicalLocationUserData(id, userDataKey, callback)</td>
    <td style="padding:15px">Delete a userData from a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePhysicalLocationById(physicalLocationId, callback)</td>
    <td style="padding:15px">Deletes a specific physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/physicalLocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20PhysicalLocations(offset, limit, callback)</td>
    <td style="padding:15px">Get list of physical locations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV20PhysicalLocations(body, callback)</td>
    <td style="padding:15px">Creates or updates a physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV20PhysicalLocationsPhysicalLocationId(physicalLocationId, callback)</td>
    <td style="padding:15px">Deletes a specific physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/physicalLocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3PhysicalLocations(include, offset, limit, callback)</td>
    <td style="padding:15px">Get list of physical locations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3PhysicalLocations(body, callback)</td>
    <td style="padding:15px">Trigger the  creation of physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3PhysicalLocationsId(id, include, callback)</td>
    <td style="padding:15px">Retrieve a physical location with given id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3PhysicalLocationsIdExpectations(id, body, callback)</td>
    <td style="padding:15px">Create an physical location expectation given the physical location id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3PhysicalLocationsIdExpectationsExpectationId(id, expectationId, callback)</td>
    <td style="padding:15px">Retrieve an physical locations Expectation given the physical locations id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3PhysicalLocationsIdExpectationsExpectationId(id, expectationId, callback)</td>
    <td style="padding:15px">Delete an physical location expectation given the physical location id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3PhysicalLocationsIdIdentifiersIdentifierKey(id, identifierKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an Identifier to a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3PhysicalLocationsIdIdentifiersIdentifierKey(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an Identifier from a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3PhysicalLocationsIdUserDataUserDataKey(id, userDataKey, body, callback)</td>
    <td style="padding:15px">Creates or updates an UserData to a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3PhysicalLocationsIdUserDataUserDataKey(id, userDataKey, callback)</td>
    <td style="padding:15px">Delete a userData from a given physical location</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3PhysicalLocationsPhysicalLocationId(physicalLocationId, callback)</td>
    <td style="padding:15px">Delete a physical location with given id.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/physicalLocations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV4PhysicalLocations(include, offset, limit, callback)</td>
    <td style="padding:15px">Get list of physical locations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/physicalLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimingNode(ncId, withLinks, noCache, preemptiveSync = 'IN', callback)</td>
    <td style="padding:15px">Get all timing node data</td>
    <td style="padding:15px">{base_path}/{version}/timing/api/v1/timingNode/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCards(currentId, baselineId, reportType, callback)</td>
    <td style="padding:15px">Get the cards for the current snapshot compared to a baseline</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeltaReport(report, baseline, callback)</td>
    <td style="padding:15px">Get a delta report by report id and baseline snapshot id</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEraApiV1ReportsReportId(reportId, callback)</td>
    <td style="padding:15px">Get a report by reportId</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSectionReport(snapshotId, callback)</td>
    <td style="padding:15px">Get a section report by snapshotId in the legacy format</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/sectionReport/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEraApiV1Schedules(callback)</td>
    <td style="padding:15px">Get all current schedules</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEraApiV1Schedules(body, callback)</td>
    <td style="padding:15px">Create an extending report schedule</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedule(id, callback)</td>
    <td style="padding:15px">Get a schedule by id</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSchedule(id, body, callback)</td>
    <td style="padding:15px">Update an extending report schedule</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedule(id, callback)</td>
    <td style="padding:15px">Delete a schedule</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfiguration(body, callback)</td>
    <td style="padding:15px">Configure SFTP connectivity</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/sftp/connectivityConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnapshots(state, sort, limit, callback)</td>
    <td style="padding:15px">Get a list of all snapshots</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCapture(body, callback)</td>
    <td style="padding:15px">Create a snapshot</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Get a snapshot by snapshotId</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCapture(snapshotId, file, callback)</td>
    <td style="padding:15px">Upload the capture file for an upload snapshot</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Delete a snapshot by snapshotId</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCapture(snapshotId, callback)</td>
    <td style="padding:15px">Download the capture file by snapshotId</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCaptureFile(snapshotId, body, callback)</td>
    <td style="padding:15px">Delete a capture file by snapshotId</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/snapshots/{pathv1}/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEraApiV1TrendsReportName(reportName, callback)</td>
    <td style="padding:15px">Get the trend group corresponding to the specified report name, source, and OMS</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v1/trends/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEraApiV2TrendsReportName(reportName, eNTITYNAME, callback)</td>
    <td style="padding:15px">Get the trend group corresponding to the specified report name, source, and OMS</td>
    <td style="padding:15px">{base_path}/{version}/era/api/v2/trends/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrApiV1Entities(sessionId, callback)</td>
    <td style="padding:15px">Get OTDR entities</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/entities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV1Entities(sessionId, traceEntities, callback)</td>
    <td style="padding:15px">Start/Stop trace on OTDR entities</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/entities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOtdrApiV1Entities(sessionId, traceId, oTDREntity, callback)</td>
    <td style="padding:15px">Edit an OTDR entity</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/entities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrApiV1Tracelist(sessionId, traceId, callback)</td>
    <td style="padding:15px">Get SOR tracelist</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/tracelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrApiV1Spanlength(sessionId, traceId, callback)</td>
    <td style="padding:15px">Get OTDR Spanlength</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/spanlength?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV1Sordownload(sessionId, profileName, data, callback)</td>
    <td style="padding:15px">SFTP sor file</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/sordownload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV2Sordownload(sessionId, profileName, data, callback)</td>
    <td style="padding:15px">SFTP sor file</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v2/sordownload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrApiV1Profile(callback)</td>
    <td style="padding:15px">Get SOR download profiles</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV1Profile(profile, callback)</td>
    <td style="padding:15px">Creates a SOR download profile</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOtdrApiV1Profile(id, profile, callback)</td>
    <td style="padding:15px">Edit a SOR download profile</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOtdrApiV1Profile(id, callback)</td>
    <td style="padding:15px">Delete a SOR download profile</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrApiV1Traceprofiles(sessionId, callback)</td>
    <td style="padding:15px">Get OTDR trace profiles</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/traceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV1Traceprofiles(sessionId, profileId, profileParameters, callback)</td>
    <td style="padding:15px">Create an OTDR trace profile</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/traceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOtdrApiV1Traceprofiles(sessionId, profileId, profileParameters, callback)</td>
    <td style="padding:15px">Edit an OTDR trace profile</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/traceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOtdrApiV1Traceprofiles(sessionId, profileId, callback)</td>
    <td style="padding:15px">Delete an OTDR trace profile</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/traceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrApiV1Sortraces(freId, callback)</td>
    <td style="padding:15px">Get Saved SOR traces</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/sortraces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV1Sortraces(freId, callback)</td>
    <td style="padding:15px">Retrieve SOR traces</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/sortraces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOtdrApiV1Sortraces(sorTraceParameters, callback)</td>
    <td style="padding:15px">Edit Saved SOR trace label</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/sortraces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOtdrApiV1Sortraces(sorTraceParameters, callback)</td>
    <td style="padding:15px">Delete a Saved SOR trace file</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/sortraces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOtdrApiV1SorParse(parseSorTraceFiles, callback)</td>
    <td style="padding:15px">Retrieve parsed SOR traces</td>
    <td style="padding:15px">{base_path}/{version}/otdr/api/v1/sor/parse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanMargins(offset, limit, callback)</td>
    <td style="padding:15px">Get all OTSi services and their margins</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/channelmargins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanMargin(freid, dir = 'Tx', callback)</td>
    <td style="padding:15px">Get detailed channel margin data for a specific service</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/channelmargins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigs(callback)</td>
    <td style="padding:15px">Get the configuration settings of Performance Gauge</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigs(body, callback)</td>
    <td style="padding:15px">Create or change the configuration settings of Performance Gauge</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarginSummary(resourcePartitionIDs, callback)</td>
    <td style="padding:15px">Dashboard API binning all OTSi services according to line rate and margin status</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/marginsummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberLoss(offset, limit, callback)</td>
    <td style="padding:15px">List of fibers and their losses</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/fiberloss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberidLoss(freid, callback)</td>
    <td style="padding:15px">Get specified fiber and its losses</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/fiberloss/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFiberidLoss(freid, fiberData, callback)</td>
    <td style="padding:15px">Patch fiber planned loss</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/fiberloss/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhotonicMediaParams(freIDS, offset, limit, callback)</td>
    <td style="padding:15px">Get the chromatic dispersion, latency and distance for the section(s)</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/photonicdata/mediaparams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhotonicMarginData(freid, filter = 'all', units = 'SNR total [dB]', fillRatio = 'current', callback)</td>
    <td style="padding:15px">Get the photonic sections and their measured values</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/photonicdata/snrdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhotonicSummaryData(resourcePartitionIDs, callback)</td>
    <td style="padding:15px">Get summary of all photonic sections and their measured values</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/photonicdata/snrsummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanmarg(callback)</td>
    <td style="padding:15px">Get list of OTU services and their margins</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/channelmargin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanidmarg(freid, dir, callback)</td>
    <td style="padding:15px">Get detailed channel margin data for a specific service</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/channelmargin/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarginSummaryV1(callback)</td>
    <td style="padding:15px">Dashboard API binning all OTSi services according to line rate and margin status</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/marginsummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSafety(callback)</td>
    <td style="padding:15px">How much greater than upgrade does minmargin need to be in order for it to be flagged as upgradeabl</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/safety?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSafety(factor, callback)</td>
    <td style="padding:15px">How much greater than upgrade does minmargin need to be in order for it to be flagged as upgradeabl</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/safety/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getValidity(callback)</td>
    <td style="padding:15px">How many 15 minute PM bins are required before a margin calculation is considered valid</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/validity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putValidity(factor, callback)</td>
    <td style="padding:15px">How many 15 minute PM bins are required before a margin calculation is considered valid</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/validity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberidloss(freid, callback)</td>
    <td style="padding:15px">Get specified fiber and its losses</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/fiberloss/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFiberidloss(freid, fiberData, callback)</td>
    <td style="padding:15px">Patch fiber planned loss</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/fiberloss/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhotonicMediaParamsV1(freIDS, offset, limit, callback)</td>
    <td style="padding:15px">Get the chromatic dispersion, latency and distance for the section(s)</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/photonicdata/mediaparams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhotonicMarginDataV1(freid, filter = 'all', units = 'SNR total [dB]', fillRatio = 'current', callback)</td>
    <td style="padding:15px">Get the photonic sections and their measured values</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/photonicdata/snrdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhotonicSummaryDataV1(resourcePartitionIDs, callback)</td>
    <td style="padding:15px">Get summary of all photonic sections and their measured values</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/photonicdata/snrsummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetchannelmargindata(freid, callback)</td>
    <td style="padding:15px">Reset Channel Margin data for the given freid</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/resetchannelmargin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetppgdata(freid, callback)</td>
    <td style="padding:15px">Reset Photonic performance guage data for the given freid</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v1/resetppgdata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanMarginsV2(offset, limit, callback)</td>
    <td style="padding:15px">Get all OTSi services and their margins</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/channelmargins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanMarginV2(freid, dir = 'Tx', callback)</td>
    <td style="padding:15px">Get detailed channel margin data for a specific service</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/channelmargins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigsV2(callback)</td>
    <td style="padding:15px">Get the configuration settings of Performance Gauge</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfigsV2(body, callback)</td>
    <td style="padding:15px">Create or change the configuration settings of Performance Gauge</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarginSummaryV2(resourcePartitionIDs, callback)</td>
    <td style="padding:15px">Dashboard API binning all OTSi services according to line rate and margin status</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/marginsummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberLossV2(offset, limit, callback)</td>
    <td style="padding:15px">List of fibers and their losses</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/fiberloss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberidLossV2(freid, callback)</td>
    <td style="padding:15px">Get specified fiber and its losses</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v2/fiberloss/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberLossV3(offset, limit, callback)</td>
    <td style="padding:15px">List of fibers and their losses</td>
    <td style="padding:15px">{base_path}/{version}/perfg/api/v3/fiberloss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV1AutocompleteTag(tag, filter, limit, callback)</td>
    <td style="padding:15px">Find a subset of possible values for a tag matching a filter</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/autocomplete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV1Changeftpcredentials(username, password, callback)</td>
    <td style="padding:15px">Change SFTP credentials for 6500 PM file transfer</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/changeftpcredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">initReg(body, callback)</td>
    <td style="padding:15px">Facility interactions</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV1Locations(callback)</td>
    <td style="padding:15px">Get all possible location fields for performance metrics</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRPM(body, callback)</td>
    <td style="padding:15px">Retrieves real-time metrics from network element</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/queries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHPM(body, callback)</td>
    <td style="padding:15px">Retrieves historical performance metrics from heroic</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/query/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV1QueryMetricsCorrelated(body, ts = 'ms', start, end, cursor, callback)</td>
    <td style="padding:15px">Retrieves historical performance metrics correlated by facility (used for SDMON, CHMON and OPM quer</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/query/metrics/correlated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV1QueryMetricsRollupCalculation(body, calculation = 'ThroughputWithOctets', granularity = '1_Day', callback)</td>
    <td style="padding:15px">Search and filter historical performance metrics, do rollup calculation using the 15 minutes bin ba</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/query/metrics/rollupCalculation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmApiV2Collections(body, callback)</td>
    <td style="padding:15px">Bulk update collection configuration for multiple network elements</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV2CollectionsNetworkelementsIntervalsType(type, configuration = '15_MINUTES', callback)</td>
    <td style="padding:15px">Get the polling interval(s) of the 15 minute or 24 hour bin for all network elements of given type</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/collections/networkelements/intervals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmApiV2CollectionsNetworkelementsTypesType(type, body, callback)</td>
    <td style="padding:15px">Update collection configuration of all network elements of given type</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/collections/networkelements/types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV2CollectionsNetworkelementsNetworkElementName(networkElementName, callback)</td>
    <td style="padding:15px">Return collection configuration of the given network element</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/collections/networkelements/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV2CollectionsNetworkelementsNetworkElementName(networkElementName, configuration, callback)</td>
    <td style="padding:15px">Trigger immediate collection for the specified network element and resolution</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/collections/networkelements/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmApiV2CollectionsNetworkelementsNetworkElementName(networkElementName, body, callback)</td>
    <td style="padding:15px">Update collection configuration of the given network element</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/collections/networkelements/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV2QueryMeta(body, callback)</td>
    <td style="padding:15px">Retrieves meta data according to the specified criteria</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/query/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV2QueryMetrics(body, ts = 'ms', start, end, page, callback)</td>
    <td style="padding:15px">Retrieves historical performance metrics</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/query/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV2QueryTags(tag, search, limit, callback)</td>
    <td style="padding:15px">Returns a collection of existing values for the provided tag</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v2/query/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV3Collections(networkElementName, callback)</td>
    <td style="padding:15px">Return collection configuration of the given network element</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV3Collections(networkElementName, configuration, callback)</td>
    <td style="padding:15px">Trigger immediate collection for the specified network element and resolution</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmApiV3Collections(networkElementName, body, callback)</td>
    <td style="padding:15px">Update collection configuration of the given network element</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV3CollectionsCollectioncount(type, configuration = '15_MINUTES', duration = '15 MINUTES', callback)</td>
    <td style="padding:15px">Get the collection summary of 15_MINUTES or 24_HOURS bin for given time duration and network elemen</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/collections/collectioncount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmApiV3CollectionsNetworkelementsIntervalsType(type, typegroup, configuration = '15_MINUTES', callback)</td>
    <td style="padding:15px">Get the polling interval(s) of the 15 minute or 24 hour bin for all network elements of given type</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/collections/networkelements/intervals/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmApiV3CollectionsNetworkelementsTypesType(type, typegroup, body, callback)</td>
    <td style="padding:15px">Update collection configuration of all network elements of given type</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/collections/networkelements/types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV3QueryMetrics(body, ts = 'ms', start, end, cursor, backwards, callback)</td>
    <td style="padding:15px">Retrieves historical performance metrics</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v3/query/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmApiV1QueryOssMetrics(body, ts = 'ms', start, end, cursor, backwards, callback)</td>
    <td style="padding:15px">Retrieves historical performance metrics for oss system</td>
    <td style="padding:15px">{base_path}/{version}/pm/api/v1/query/ossMetrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTraces(callback)</td>
    <td style="padding:15px">Retrieve all the active otdr traces</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/activetraces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(include, callback)</td>
    <td style="padding:15px">Get list of all fiberpaths</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/fiberpaths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">post(body, callback)</td>
    <td style="padding:15px">Create a fiberpath</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/fiberpaths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getById(id, callback)</td>
    <td style="padding:15px">Retrieve a fiberpath with a given identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/fiberpaths/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(id, callback)</td>
    <td style="padding:15px">Deletes fiberpath by the given identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/fiberpaths/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinks(id, callback)</td>
    <td style="padding:15px">Retrieve the links associated with the given fiberpath identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/fiberpaths/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoordinatesOfDistances(id, ncid, body, callback)</td>
    <td style="padding:15px">Retrieve list of coordinates for distances on fiberpath</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/fiberpaths/{pathv1}/startNode/{pathv2}/distances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllKmlFiles(callback)</td>
    <td style="padding:15px">Get info of all the uploaded kml files</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteByFileId(id, callback)</td>
    <td style="padding:15px">Deletes all the file elements</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlacemarksByFileId(id, callback)</td>
    <td style="padding:15px">Get all placemarks of the given file identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/files/{pathv1}/placemarks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPinpointApiV1KmlPlacemarks(ids, offset, limit, callback)</td>
    <td style="padding:15px">Get list of the kml elements</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/placemarks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPinpointApiV1KmlPlacemarksId(id, include, callback)</td>
    <td style="padding:15px">Retrieve a kml element with the given identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/placemarks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">put(id, body, callback)</td>
    <td style="padding:15px">Updates kml attributes</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/placemarks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePinpointApiV1KmlPlacemarksId(id, callback)</td>
    <td style="padding:15px">Deletes the specific kml element and all its association if any</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/placemarks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateNe(id, body, callback)</td>
    <td style="padding:15px">Associates the kml element with the managed element</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/placemarks/{pathv1}/ne?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssociation(id, resourceType, relatedId, callback)</td>
    <td style="padding:15px">Deletes association given the kml element and related idenitifer</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/placemarks/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(file, callback)</td>
    <td style="padding:15px">Uploads the kml file</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/kml/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUnassociatedNes(lat, longParam, distance, offset, limit, callback)</td>
    <td style="padding:15px">Get list of unassociated network elements</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtdrConfigDataById(ncid, aid, callback)</td>
    <td style="padding:15px">Retrieve otdr facility config data with the given network element ncid and facility aid</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes/{pathv1}/otdr/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBaseline(ncid, aid, body, callback)</td>
    <td style="padding:15px">Set the current trace to baseline</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes/{pathv1}/otdr/{pathv2}/setbaseline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSorFiles(ncid, aid, traceType, callback)</td>
    <td style="padding:15px">Retrieve the sor files associated with the given network element and otdr aid identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes/{pathv1}/otdr/{pathv2}/sorfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startTrace(ncid, aid, body, callback)</td>
    <td style="padding:15px">Edit the OTDR facility config parameters and start trace</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes/{pathv1}/otdr/{pathv2}/starttrace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopTrace(ncid, aid, callback)</td>
    <td style="padding:15px">Stop the already running OTDR trace</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes/{pathv1}/otdr/{pathv2}/stoptrace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceStatus(ncid, aid, callback)</td>
    <td style="padding:15px">Retrieve the active trace</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/nes/{pathv1}/otdr/{pathv2}/tracestatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPinpointApiV1OthersType(type, body, callback)</td>
    <td style="padding:15px">Creates a spool or waypoint and associates with kml element</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/others/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPinpointApiV1SorId(id, callback)</td>
    <td style="padding:15px">Retrieve a sor trace with the given identifier</td>
    <td style="padding:15px">{base_path}/{version}/pinpoint/api/v1/sor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlanningnetworkexportApiV1NetworksTpeFre(captureTimeOutInMins, callback)</td>
    <td style="padding:15px">Download raw tpe fre data as .mcpc file</td>
    <td style="padding:15px">{base_path}/{version}/planningnetworkexport/api/v1/Networks/TpeFre?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryCalculationContext(rootfreid, calculation, callback)</td>
    <td style="padding:15px">Queries active CalculationContext instances</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/calculationcontexts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllResourceProfiles(callback)</td>
    <td style="padding:15px">Retrieve all resource profiles</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/resource_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourceProfile(body, callback)</td>
    <td style="padding:15px">Update resource profile(s)</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/resource_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResourceProfile(body, callback)</td>
    <td style="padding:15px">Create resource profile for one or more resources</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/resource_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceProfileById(resourceProfileId, callback)</td>
    <td style="padding:15px">Retrieve resource profile by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/resource_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourceProfileById(resourceProfileId, body, callback)</td>
    <td style="padding:15px">Update a specific resource profile by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/resource_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceProfileById(resourceProfileId, callback)</td>
    <td style="padding:15px">Delete a resource profile by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/resource_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllThresholdGroups(callback)</td>
    <td style="padding:15px">Retrieve all threshold groups</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/threshold_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThresholdGroup(body, callback)</td>
    <td style="padding:15px">Update threshold group(s)</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/threshold_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThresholdGroup(body, callback)</td>
    <td style="padding:15px">Create threshold group(s) containing one or more pre-existing thresholds</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/threshold_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdGroupById(thresholdGroupId, callback)</td>
    <td style="padding:15px">Retrieve threshold group by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/threshold_groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThresholdGroupById(thresholdGroupId, body, callback)</td>
    <td style="padding:15px">Update threshold group by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/threshold_groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdGroupById(thresholdGroupId, callback)</td>
    <td style="padding:15px">Delete a threshold group by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/threshold_groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllThresholds(callback)</td>
    <td style="padding:15px">Retrieve all thresholds</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThreshold(body, callback)</td>
    <td style="padding:15px">Update one or more thresholds</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThreshold(body, callback)</td>
    <td style="padding:15px">Create threshold(s) with associated alert</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdById(thresholdId, callback)</td>
    <td style="padding:15px">Retrieve threshold by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThresholdById(thresholdId, body, callback)</td>
    <td style="padding:15px">Update threshold by ID</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdById(thresholdId, callback)</td>
    <td style="padding:15px">Delete a threshold by ID.</td>
    <td style="padding:15px">{base_path}/{version}/pmprocessor/api/v1/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCondition(conditionId, callback)</td>
    <td style="padding:15px">Get a condition by condition id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetCondition(conditionId, callback)</td>
    <td style="padding:15px">Get a condition by condition id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCondition(conditionId, callback)</td>
    <td style="padding:15px">Delete a condition from the policies database</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConditions(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the conditions defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListConditions(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the conditions defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCondition(conditionRequest, callback)</td>
    <td style="padding:15px">Create a new condition that may be used in policies</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countConditions(q, p, limit, callback)</td>
    <td style="padding:15px">Count conditions defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountConditions(q, p, limit, callback)</td>
    <td style="padding:15px">Count conditions defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConditionsByTenantId(tenantId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the conditions defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/tenants/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListConditionsByTenantId(tenantId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the conditions defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/tenants/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countConditionsByTenantId(tenantId, q, p, limit, callback)</td>
    <td style="padding:15px">Count the conditions defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/tenants/{pathv1}/conditions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountConditionsByTenantId(tenantId, q, p, limit, callback)</td>
    <td style="padding:15px">Count the conditions defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/conditions/tenants/{pathv1}/conditions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicies(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the policies defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListPolicies(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the policies defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(policyRequest, callback)</td>
    <td style="padding:15px">Create a policy in the policies</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countPoliciesByTenantId(tenantId, q, p, limit, callback)</td>
    <td style="padding:15px">Count the policies defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/tenants/{pathv1}/policies/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountPoliciesByTenantId(tenantId, q, p, limit, callback)</td>
    <td style="padding:15px">Count the policies defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/tenants/{pathv1}/policies/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(policyId, callback)</td>
    <td style="padding:15px">Get a policy by policy id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetPolicy(policyId, callback)</td>
    <td style="padding:15px">Get a policy by policy id</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(policyId, callback)</td>
    <td style="padding:15px">Delete a policy from the policies</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countPolicies(q, p, limit, callback)</td>
    <td style="padding:15px">Count policies defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountPolicies(q, p, limit, callback)</td>
    <td style="padding:15px">Count policies defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPoliciesByTenantId(tenantId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the policies defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/tenants/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListPoliciesByTenantId(tenantId, q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the policies defined for a specific tenant</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/policies/tenants/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRealms(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the realms defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headListRealms(q, p, offset, limit, pageToken, callback)</td>
    <td style="padding:15px">List all of the realms defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRealm(realmRequest, callback)</td>
    <td style="padding:15px">Create a realm in the policies database</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countRealms(q, p, limit, callback)</td>
    <td style="padding:15px">Count realms defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headCountRealms(q, p, limit, callback)</td>
    <td style="padding:15px">Count realms defined in the Policy Manager</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealm(realmName, callback)</td>
    <td style="padding:15px">Get a realm by realm name</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headGetRealm(realmName, callback)</td>
    <td style="padding:15px">Get a realm by realm name</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRealm(realmName, callback)</td>
    <td style="padding:15px">Delete a realm from the policies database</td>
    <td style="padding:15px">{base_path}/{version}/bpocore/policies/api/v1/realms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpes(id, networkConstructId, location, locationFormat, structureType, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, concrete, tpeExpectationsIntentId, content = 'summary', fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTpe(body, callback)</td>
    <td style="padding:15px">Trigger the creation of a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTpeTti(body, callback)</td>
    <td style="padding:15px">Get TTI snapshot from RA</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/trailtrace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTpeIdentifier(id, identifierRO, callback)</td>
    <td style="padding:15px">Puts or updates a REST identifier on a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTpeIdentifier(id, identifierRO, callback)</td>
    <td style="padding:15px">Deletes a REST identifier on a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpe(tpeId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTpeById(tpeId, tpe, callback)</td>
    <td style="padding:15px">Updates or creates a TPE with an id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTpeById(tpeId, callback)</td>
    <td style="padding:15px">De-provision an actual TPEResource or delete a root TPEResource.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiTpesTpeIdExpectations(tpeId, body, callback)</td>
    <td style="padding:15px">Create an TPEResource expectation given the TPEResource id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionOperations(tpeId, expectationId, callback)</td>
    <td style="padding:15px">Perform a provision operation on a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTpeExpectation(tpeId, tpeExpectationId, body, callback)</td>
    <td style="padding:15px">PATCH a specific Tpe expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTpeSuppressionPort(tpeId, body, callback)</td>
    <td style="padding:15px">Sets a alarm suppression value in a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/suppressionPort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpeExpectations(tpeId, callback)</td>
    <td style="padding:15px">Retrieves the expectations of the parent TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/tpeExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpeExpectation(tpeId, tpeExpectationId, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/tpeExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTpeExpectation(tpeId, tpeExpectationId, callback)</td>
    <td style="padding:15px">Delete TPE Expectation given tpeId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/tpeExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpePlanned(tpeId, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTpePlanned(tpeId, body, callback)</td>
    <td style="padding:15px">Creates or updates a planned TPE given the TPE id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPlannedTpe(tpeId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTpeUserData(tpeId, userDataKey, body, callback)</td>
    <td style="padding:15px">Puts a new key value pair into the user data</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTpeUserData(tpeId, userDataKey, callback)</td>
    <td style="padding:15px">Delete a TPEs userData key and value</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/tpes/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20TpesTpeIdTpeExpectations(tpeId, callback)</td>
    <td style="padding:15px">Retrieves the expectations of the parent TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/tpes/{pathv1}/tpeExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV20TpesTpeIdTpeExpectationsTpeExpectationId(tpeId, tpeExpectationId, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/tpes/{pathv1}/tpeExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV20TpesTpeIdTpeExpectationsTpeExpectationId(tpeId, tpeExpectationId, callback)</td>
    <td style="padding:15px">Delete TPE Expectation given tpeId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/tpes/{pathv1}/tpeExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3Tpes(networkConstructId, id, location, structureType, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, concrete, include, content = 'summary', offset, limit, fields, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3Tpes(body, callback)</td>
    <td style="padding:15px">Trigger the creation of a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3TpesTpeId(tpeId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3TpesTpeId(tpeId, callback)</td>
    <td style="padding:15px">De-provision an actual TPE or delete a root TPE.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3TpesTpeIdExpectations(tpeId, body, callback)</td>
    <td style="padding:15px">Create an TPE expectation given the TPE id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3TpesTpeIdExpectationsExpectationIdRealize(tpeId, expectationId, callback)</td>
    <td style="padding:15px">Perform a provision operation on a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3TpesTpeIdExpectationsTpeExpectationId(tpeId, tpeExpectationId, body, callback)</td>
    <td style="padding:15px">PATCH a specific Tpe expectation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3TpesTpeIdTpeExpectationsTpeExpectationId(tpeId, tpeExpectationId, callback)</td>
    <td style="padding:15px">Delete TPE Expectation given tpeId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/tpeExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3TpesTpeIdTpePlanned(tpeId, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3TpesTpeIdTpePlanned(tpeId, body, callback)</td>
    <td style="padding:15px">Creates or updates a planned TPE given the TPE id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3TpesTpeIdTpePlanned(tpeId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3TpesTpeIdUserDataUserDataKey(tpeId, userDataKey, body, callback)</td>
    <td style="padding:15px">Puts a new key value pair into the user data</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3TpesTpeIdUserDataUserDataKey(tpeId, userDataKey, callback)</td>
    <td style="padding:15px">Delete a TPEs userData key and value</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/tpes/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV4Tpes(id, searchText, searchFields, resourceState, namedQuery, networkConstructId, identifierKey, identifierValue, concrete, modelType, bookingDataLockout = 'true', active = 'true', content = 'summary', location, locationFormat, structureType, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, gneSubnetName, fields, sortBy, offset, limit, metaDataFields, include, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV4Tpes(body, callback)</td>
    <td style="padding:15px">Trigger the creation of a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV4TpesIdIdentifiers(id, identifierRO, callback)</td>
    <td style="padding:15px">Puts or updates a REST identifier on a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV4TpesIdIdentifiers(id, identifierRO, callback)</td>
    <td style="padding:15px">Deletes a REST identifier on a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV4TpesTpeId(tpeId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV4TpesTpeId(tpeId, tpeRO, callback)</td>
    <td style="padding:15px">Updates or creates a TPE with an id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV4TpesTpeIdExpectations(tpeId, body, callback)</td>
    <td style="padding:15px">Create an TPE expectation given the TPE id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV4TpesTpeIdTpePlanned(tpeId, body, callback)</td>
    <td style="padding:15px">PATCH a specific planned TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/tpes/{pathv1}/tpePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpesV5(id, networkConstructId, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, concrete, content = 'summary', fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV5Tpes(body, callback)</td>
    <td style="padding:15px">Trigger the creation of a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV5TpesTpeId(tpeId, tpeRO, callback)</td>
    <td style="padding:15px">Updates or creates a TPE with an id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/tpes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTpesV6(id, networkConstructId, location, locationFormat, structureType, equipmentId, tpeExpectationsEquipmentIntentId, tpeExpectationsServiceIntentId, concrete, tpeExpectationsIntentId, content = 'summary', fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieves the TPEs satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV6Tpes(body, callback)</td>
    <td style="padding:15px">Trigger the creation of a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/tpes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV6TpesTrailtrace(body, callback)</td>
    <td style="padding:15px">Get TTI snapshot from RA</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/tpes/trailtrace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV6TpesTpeIdSuppressionPort(tpeId, body, callback)</td>
    <td style="padding:15px">Sets a alarm suppression value in a TPE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/tpes/{pathv1}/suppressionPort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProblems(body, callback)</td>
    <td style="padding:15px">Get all problems matching the search filters, or all problems if no search filters provided</td>
    <td style="padding:15px">{base_path}/{version}/ats/api/v1/problems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProblem(id, include, callback)</td>
    <td style="padding:15px">Get a problem by its ID, optionally include the related alarm resources</td>
    <td style="padding:15px">{base_path}/{version}/ats/api/v1/problems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProblem(id, body, callback)</td>
    <td style="padding:15px">Patch a problem by ID</td>
    <td style="padding:15px">{base_path}/{version}/ats/api/v1/problems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectmanagementApiV1PlanningProjects(name, projectState, projectType, completionDateStart, completionDateEnd, archivedStatus, callback)</td>
    <td style="padding:15px">Gets planning projects.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjects(profileName, body, callback)</td>
    <td style="padding:15px">Create a planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectmanagementApiV1PlanningProjects(name, checkDependencies, callback)</td>
    <td style="padding:15px">Deletes planning projects.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByGuid(id, callback)</td>
    <td style="padding:15px">Get a planning project by ID.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProjectmanagementApiV1PlanningProjectsId(id, body, callback)</td>
    <td style="padding:15px">Updates the specified planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectmanagementApiV1PlanningProjectsId(id, checkDependencies, callback)</td>
    <td style="padding:15px">Deletes a planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsIdCommit(id, callback)</td>
    <td style="padding:15px">Commits the planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}/commit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsIdCommitOperation(id, callback)</td>
    <td style="padding:15px">Commits the planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}/commitOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsCommit(name, callback)</td>
    <td style="padding:15px">Commits planning projects.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/commit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsCommitOperation(name, callback)</td>
    <td style="padding:15px">Commits planning projects.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/commitOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsIdCancel(id, ignoreEquipmentProvisioning, checkDependencies, callback)</td>
    <td style="padding:15px">Cancels a planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsIdCancelOperation(id, ignoreEquipmentProvisioning, checkDependencies, callback)</td>
    <td style="padding:15px">Cancels a planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/{pathv1}/cancelOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsCancel(name, ignoreEquipmentProvisioning, checkDependencies, callback)</td>
    <td style="padding:15px">Cancels planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsCancelOperation(name, ignoreEquipmentProvisioning, checkDependencies, callback)</td>
    <td style="padding:15px">Cancels planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/cancelOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsUpdateArchivedStatus(body, callback)</td>
    <td style="padding:15px">Updates the specified archived status of planning projects.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/updateArchivedStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1PlanningProjectsNameChangeOperation(name, body, callback)</td>
    <td style="padding:15px">Changes the name of a planning project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/nameChangeOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProjectmanagementApiV1PlanningProjectsNameProjectName(projectName, body, callback)</td>
    <td style="padding:15px">Partially updates the project.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/PlanningProjects/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectmanagementApiV2ProjectLists(name, callback)</td>
    <td style="padding:15px">Get all project lists.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV2ProjectLists(body, callback)</td>
    <td style="padding:15px">Create a project list.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectmanagementApiV2ProjectListsId(id, callback)</td>
    <td style="padding:15px">Get a project list by ID.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV2ProjectListsProjectListIdAddProject(projectListId, body, callback)</td>
    <td style="padding:15px">Append a project to the list and validate the project list.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists/{pathv1}/addProject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV2ProjectListsNameProjectListNameAddProject(projectListName, body, callback)</td>
    <td style="padding:15px">Append a project to the list and validate the project list.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists/name/{pathv1}/addProject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectmanagementApiV2ProjectListsProjectListId(projectListId, callback)</td>
    <td style="padding:15px">Delete a project list by ID.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectmanagementApiV2ProjectListsNameProjectListName(projectListName, callback)</td>
    <td style="padding:15px">Delete a project list by name.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v2/ProjectLists/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectmanagementApiV1ProjectLists(name, callback)</td>
    <td style="padding:15px">Get all project lists.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1ProjectLists(body, callback)</td>
    <td style="padding:15px">Create a project list.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectmanagementApiV1ProjectListsId(id, callback)</td>
    <td style="padding:15px">Get a project list by ID.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1ProjectListsProjectListIdAddProject(projectListId, body, callback)</td>
    <td style="padding:15px">Append a project to the list and validate the project list.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists/{pathv1}/addProject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProjectmanagementApiV1ProjectListsNameProjectListNameAddProject(projectListName, body, callback)</td>
    <td style="padding:15px">Append a project to the list and validate the project list.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists/name/{pathv1}/addProject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectmanagementApiV1ProjectListsProjectListId(projectListId, callback)</td>
    <td style="padding:15px">Delete a project list by ID.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectmanagementApiV1ProjectListsNameProjectListName(projectListName, callback)</td>
    <td style="padding:15px">Delete a project list by name.</td>
    <td style="padding:15px">{base_path}/{version}/projectmanagement/api/v1/ProjectLists/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnDemandReport(body, callback)</td>
    <td style="padding:15px">API to generate an on demand report</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReports(fromDate, toDate, reportType, callback)</td>
    <td style="padding:15px">Delete reports on date range and report type basis.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/report/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportForResourceId(resourceId, callback)</td>
    <td style="padding:15px">Delete a report associated with resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/report/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDetailsOnResourceId(resourceId, callback)</td>
    <td style="padding:15px">Fetch report's meta data information against a resource id</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/report/reportinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDetails(fromDate, toDate, reportType, callback)</td>
    <td style="padding:15px">Fetch meta data information for all/specific report type.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/report/reportsinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportForResourceId(resourceId, callback)</td>
    <td style="padding:15px">Fetch report data for a report which is associated with user provided resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/report/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportScheduleForReportType(reportType, callback)</td>
    <td style="padding:15px">Fetch all schedules for a single/list of report types.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReportSchedule(body, callback)</td>
    <td style="padding:15px">Create a new schedule for a report type.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/schedule/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportScheduleForResourceId(resourceId, callback)</td>
    <td style="padding:15px">Delete a specific report's schedule associated with resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/schedule/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportScheduleOnResourceId(resourceId, callback)</td>
    <td style="padding:15px">Fetch a specific schedule associated with resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v1/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReportingApiV2Report(body, callback)</td>
    <td style="padding:15px">API to generate an on demand report</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReportingApiV2ReportCreatereporttype(reportType, description, uploadWorkflow, joltFile, callback)</td>
    <td style="padding:15px">creates user-defined custom report-type</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/createreporttype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportingApiV2ReportDelete(fromDate, toDate, reportType, callback)</td>
    <td style="padding:15px">Delete reports on date range and report type basis.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportType(reportType, callback)</td>
    <td style="padding:15px">Delete a report type.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/delete/reporttype/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportingApiV2ReportDeleteResourceId(resourceId, callback)</td>
    <td style="padding:15px">Delete a report associated with resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertCircleTypeForRingName(uploadPropertyFile, callback)</td>
    <td style="padding:15px">Add/Update circle, type, ring type corresponding to a Ring Name.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/insertCircleTypeForLldpLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOneTouchReport(body, callback)</td>
    <td style="padding:15px">API to generate and return report in one-touch.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/onetouch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportingApiV2ReportReportinfoResourceId(resourceId, callback)</td>
    <td style="padding:15px">Fetch report's meta data information against a resource id</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/reportinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportsMetaInfo(fromDate, toDate, reportType, reportStatus, reportGenerationMode, sortBy, callback)</td>
    <td style="padding:15px">Fetch meta data information for all/specific report type.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/reportsinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSupportedReportTypes(callback)</td>
    <td style="padding:15px">Fetch list of report types supported.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/supported/reporttype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportingApiV2ReportResourceId(resourceId, reportFormat, utcOffset, callback)</td>
    <td style="padding:15px">Fetch report data for a report which is associated with user provided resource id. The data is enco</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/report/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportingApiV2Schedule(reportType, callback)</td>
    <td style="padding:15px">Fetch all schedules for report types.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReportingApiV2ScheduleCreate(body, callback)</td>
    <td style="padding:15px">Create a new schedule for a report type.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/schedule/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportingApiV2ScheduleDeleteResourceId(resourceId, callback)</td>
    <td style="padding:15px">Delete a specific report's schedule associated with a resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/schedule/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportingApiV2ScheduleResourceId(resourceId, callback)</td>
    <td style="padding:15px">Fetch a specific schedule associated with resource id.</td>
    <td style="padding:15px">{base_path}/{version}/reporting/api/v2/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllers(networkConstructId, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of controllers provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postController(body, callback)</td>
    <td style="padding:15px">Creates a controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getController(controllerId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteControllerById(controllerId, callback)</td>
    <td style="padding:15px">Delete a root Controller.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerExpectationsById(controllerId, callback)</td>
    <td style="padding:15px">Retrieves the expectations from Controller with specific ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}/controllerExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteControllerExpectation(controllerId, controllerExpectationId, callback)</td>
    <td style="padding:15px">Delete Controller Expectation given controllerId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}/controllerExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerPlannedById(controllerId, callback)</td>
    <td style="padding:15px">Retrieves the planned Controller by specific Controller ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}/controllerPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiControllersControllerIdExpectations(controllerId, body, callback)</td>
    <td style="padding:15px">Create an Controller expectation given the Controller id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiControllersControllerIdExpectationsControllerExpectationIdRealize(controllerId, controllerExpectationId, callback)</td>
    <td style="padding:15px">Perform a provision operation on a Controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putControllerOperations(controllerId, operation = 'manualSwitchToWork', callback)</td>
    <td style="padding:15px">Perform a Controller Operation on NE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/controllers/{pathv1}/operation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV1Controllers(networkConstructId, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of controllers provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV1Controllers(body, callback)</td>
    <td style="padding:15px">Creates a controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV1ControllersControllerId(controllerId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV1ControllersControllerId(controllerId, callback)</td>
    <td style="padding:15px">Delete a root Controller.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerExpectations(controllerId, callback)</td>
    <td style="padding:15px">Retrieves the expectations from Controller with specific ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}/controllerExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV1ControllersControllerIdControllerExpectationsControllerExpectationId(controllerId, controllerExpectationId, callback)</td>
    <td style="padding:15px">Delete Controller Expectation given controllerId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}/controllerExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV1ControllersControllerIdControllerPlanned(controllerId, callback)</td>
    <td style="padding:15px">Retrieves the planned Controller by specific Controller ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}/controllerPlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV1ControllersControllerIdExpectations(controllerId, body, callback)</td>
    <td style="padding:15px">Create an Controller expectation given the Controller id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV1ControllersControllerIdExpectationsControllerExpectationIdRealize(controllerId, controllerExpectationId, callback)</td>
    <td style="padding:15px">Perform a provision operation on a Controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV1ControllersControllerIdOperationOperation(controllerId, operation = 'manualSwitchToWork', callback)</td>
    <td style="padding:15px">Perform a Controller Operation on NE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/controllers/{pathv1}/operation/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV2Controllers(networkConstructId, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of controllers provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV2ControllersControllerId(controllerId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific controller</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2/controllers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllJsonSchemas(callback)</td>
    <td style="padding:15px">Retrieve JSON Schemas provided by managed RAs</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/jsonSchemas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJsonSchemaByTitle(title, callback)</td>
    <td style="padding:15px">Get information for one JSON Schema.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/jsonSchemas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRas(callback)</td>
    <td style="padding:15px">Retrieve information about RAs managed by discovery.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/ras?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRaById(raId, callback)</td>
    <td style="padding:15px">Get information for one RA.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/ras/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebalanceSessionsPost(callback)</td>
    <td style="padding:15px">Rebalance sessions across all RA instances.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/rebalanceSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllResourceTypes(callback)</td>
    <td style="padding:15px">Retrieve resourceTypes provided by the managed RAs.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/resourceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceTypeById(resourceTypeId, callback)</td>
    <td style="padding:15px">Get information for one resourceType.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/resourceTypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSessions(callback)</td>
    <td style="padding:15px">Retrieve sessions on managed RAs.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionById(sessionId, callback)</td>
    <td style="padding:15px">Get information for one RA session.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTypeGroups(callback)</td>
    <td style="padding:15px">Retrieve information for typeGroups provided by the managed RAs.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/typeGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTypeGroupById(typeGroupId, callback)</td>
    <td style="padding:15px">Retrieve information for one typeGroup provided by the managed RAs.</td>
    <td style="padding:15px">{base_path}/{version}/ractrl/api/v1/typeGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjects(srcFre, callback)</td>
    <td style="padding:15px">Retrieves the SMO Projects satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/smo/api/v1/smoProjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProject(body, callback)</td>
    <td style="padding:15px">Creates an SMO Project</td>
    <td style="padding:15px">{base_path}/{version}/smo/api/v1/smoProjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificProject(projectId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific SMO Project</td>
    <td style="padding:15px">{base_path}/{version}/smo/api/v1/smoProjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteById(projectId, callback)</td>
    <td style="padding:15px">Deletes a specific SMO project</td>
    <td style="padding:15px">{base_path}/{version}/smo/api/v1/smoProjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invokeServiceOperations(projectId, body, callback)</td>
    <td style="padding:15px">Update attributes on the services tied to the specific project Id</td>
    <td style="padding:15px">{base_path}/{version}/smo/api/v1/smoProjects/{pathv1}/smoServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificService(serviceId, include, callback)</td>
    <td style="padding:15px">Retrieves a specific SMO Service</td>
    <td style="padding:15px">{base_path}/{version}/smo/api/v1/smoServices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIntents(label, layerRates, roadmLineName, supportingServiceName, callback)</td>
    <td style="padding:15px">Get all Service Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntent(body, callback)</td>
    <td style="padding:15px">Not Supported - Create Service Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceIntent(id, callback)</td>
    <td style="padding:15px">For INTERNAL USE ONLY: Deletes a Service Intent based on its identifier from IFD DB only</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/deleteIntent/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntent(id, callback)</td>
    <td style="padding:15px">Get a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntent(id, callback)</td>
    <td style="padding:15px">Delete a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployIntent(id, callback)</td>
    <td style="padding:15px">Not Supported - Deploy a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceState(id, callback)</td>
    <td style="padding:15px">Force State the Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/forceFailState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">realignState(id, callback)</td>
    <td style="padding:15px">Not Supported - Realign Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/realignState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceResourceTrackers(id, callback)</td>
    <td style="padding:15px">Get Service Intent Resource Trackers based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/resourceTrackers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceRoute(id, callback)</td>
    <td style="padding:15px">Get Service Intent Route based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undeployIntent(id, ignoreClientServiceDependencies, callback)</td>
    <td style="padding:15px">Begins undeployment of a service intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/undeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntent(id, body, callback)</td>
    <td style="padding:15px">Updates a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/serviceIntents/{pathv1}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ServiceIntents(label, layerRates, roadmLineName, supportingServiceName, callback)</td>
    <td style="padding:15px">Get all Service Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1ServiceIntents(body, callback)</td>
    <td style="padding:15px">Not Supported - Create Service Intents</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV1ServiceIntentsDeleteIntentId(id, callback)</td>
    <td style="padding:15px">For INTERNAL USE ONLY: Deletes a Service Intent based on its identifier from IFD DB only</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/deleteIntent/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeasibleRoute(body, callback)</td>
    <td style="padding:15px">Get Feasible Route</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/getFeasibleRoute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ServiceIntentsId(id, callback)</td>
    <td style="padding:15px">Get a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV1ServiceIntentsId(id, callback)</td>
    <td style="padding:15px">Delete a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1ServiceIntentsIdDeploy(id, callback)</td>
    <td style="padding:15px">Not Supported - Deploy a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1ServiceIntentsIdForceFailState(id, callback)</td>
    <td style="padding:15px">Force State the Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/forceFailState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1ServiceIntentsIdRealignState(id, callback)</td>
    <td style="padding:15px">Not Supported - Realign Service Intent State based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/realignState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ServiceIntentsIdResourceTrackers(id, callback)</td>
    <td style="padding:15px">Get Service Intent Resource Trackers based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/resourceTrackers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1ServiceIntentsIdRoute(id, callback)</td>
    <td style="padding:15px">Get Service Intent Route based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV1ServiceIntentsIdUndeploy(id, ignoreClientServiceDependencies, callback)</td>
    <td style="padding:15px">Begins undeployment of a service intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/undeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV1ServiceIntentsIdUpdate(id, body, callback)</td>
    <td style="padding:15px">Updates a Service Intent based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/serviceIntents/{pathv1}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2ServiceIntentsIdRoute(id, callback)</td>
    <td style="padding:15px">Get Service Intent Route based on its identifier</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/serviceIntents/{pathv1}/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployServiceIntent(serviceOperationInput, callback)</td>
    <td style="padding:15px">Deploys a Service to the network</td>
    <td style="padding:15px">{base_path}/{version}/bpoifdnbi/api/v1/serviceIntent/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undeployServiceIntent(serviceOperationInput, callback)</td>
    <td style="padding:15px">Undeploys a Service from the network</td>
    <td style="padding:15px">{base_path}/{version}/bpoifdnbi/api/v1/serviceIntent/undeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceTrailV2(freId, startTpeId, endTpeId, traversalScope, include, floors, additionalFlags, callback)</td>
    <td style="padding:15px">Return Service Trail for a given FRE Id</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v2/serviceTrails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceTopologyV2(freId, traversalScope, include, floors, callback)</td>
    <td style="padding:15px">Return Service Topology for a given FRE Id</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v2/serviceTopology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pstServiceTrailV2(body, additionalFlags, callback)</td>
    <td style="padding:15px">Returns a Service Trail</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v2/serviceTopology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pstServiceTopologyV2(body, callback)</td>
    <td style="padding:15px">Returns a Service Topology</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v2/serviceTopology/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simplePortTrailV2(freId, callback)</td>
    <td style="padding:15px">Return Simple Port Trail a given FRE Id</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v2/simplePortTrail/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceTrailV1(freId, callback)</td>
    <td style="padding:15px">Return Service Trail for a given FRE Id</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v1/serviceTrails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pstServiceTrailV1(body, callback)</td>
    <td style="padding:15px">Returns a Service Trail</td>
    <td style="padding:15px">{base_path}/{version}/revell/api/v1/serviceTrails/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextConnection(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.connection.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/connection/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextConnectivityContextConnection(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.Connection</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/connection/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextConnectivityContextConnectionUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.Connection</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnectionUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.Connection</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTapiDataContextConnectivityContextProvisioningConnectivityService(tapiConnectivityConnectivitycontextConnectivityServiceBodyParam, callback)</td>
    <td style="padding:15px">creates tapi.connectivity.connectivitycontext.ConnectivityService (note: operation unavailable on s</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/provisioning/connectivity-service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTapiDataContextConnectivityContextProvisioningConnectivityServiceUuid(uuid, callback)</td>
    <td style="padding:15px">removes tapi.connectivity.connectivitycontext.ConnectivityService (note: operation unavailable on s</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/provisioning/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTapiDataContextConnectivityContextProvisioningConnectivityServiceUuidName(uuid, tapiCommonNameAndValueBodyParam, callback)</td>
    <td style="padding:15px">creates tapi.common.NameAndValue</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/provisioning/{pathv1}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnectivityServiceUuidName(uuid, tapiCommonNameAndValueBodyParam, callback)</td>
    <td style="padding:15px">creates tapi.common.NameAndValue</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/{pathv1}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextConnectivityService(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.connectivity-service.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/connectivity-service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextConnectivityContextConnectivityService(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.connectivitycontext.ConnectivityService</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/connectivity-service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnectivityService(tapiConnectivityConnectivitycontextConnectivityServiceBodyParam, callback)</td>
    <td style="padding:15px">creates tapi.connectivity.connectivitycontext.ConnectivityService (note: operation unavailable on s</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/connectivity-service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextConnectivityContextConnectivityServiceUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.connectivitycontext.ConnectivityService</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/connectivity-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnectivityServiceUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.connectivitycontext.ConnectivityService</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnectivityServiceUuid(uuid, callback)</td>
    <td style="padding:15px">removes tapi.connectivity.connectivitycontext.ConnectivityService (note: operation unavailable on s</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextTopologyMcpBaseTopologyConnectionEndPoint(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.connection-end-point.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/topology/mcp-base-topology/connection-end-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidConnectionEndPoint(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.context.topologycontext.topology.node.ownednodeedgepoint.CepList</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/connection-end-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidConnectionEndPointConnectionEndPointUuid(topologyUuid, connectionEndPointUuid, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.ceplist.ConnectionEndPoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnectivityService(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.connectivitycontext.ConnectivityService</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/connectivity-service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiConnectivityConnectivityContextConnection(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.Connection</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-connectivity:connectivity-context/connection/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTapiDataContextOamContextOamJob(tapiOamOamBodyParam, callback)</td>
    <td style="padding:15px">creates tapi.oam.OamData</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/oam-context/oam-job/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextServiceInterfacePoint(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.service-interface-point.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/service-interface-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextServiceInterfacePoint(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.common.context.ServiceInterfacePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/service-interface-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextServiceInterfacePoint(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.common.context.ServiceInterfacePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/service-interface-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextServiceInterfacePointUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.common.context.ServiceInterfacePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextServiceInterfacePointUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.common.context.ServiceInterfacePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopology(callback)</td>
    <td style="padding:15px">returns tapi.topology.topologycontext.Topology</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopology(callback)</td>
    <td style="padding:15px">returns tapi.topology.topologycontext.Topology</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/topology/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.topologycontext.Topology</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.topologycontext.Topology</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextTopologyMcpBaseTopologyLink(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.link.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/topology/mcp-base-topology/link/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidLink(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.topology.Link</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/link/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyTopologyUuidLink(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.topology.Link</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}/link/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidLinkLinkUuid(topologyUuid, linkUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.Link</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyTopologyUuidLinkLinkUuid(topologyUuid, linkUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.Link</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextTopologyMcpBaseTopologyNode(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.node.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/topology/mcp-base-topology/node/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidNode(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.topology.topology.Node</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/node/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyTopologyUuidNode(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.topology.topology.Node</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}/node/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidNodeNodeUuid(topologyUuid, nodeUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.topology.Node</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyTopologyUuidNodeNodeUuid(topologyUuid, nodeUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.topology.Node</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidNodeNodeUuidOwnedNodeEdgePointOwnedNodeEdgePointUuid(topologyUuid, nodeUuid, ownedNodeEdgePointUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.node.OwnedNodeEdgePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextTopologyMcpBaseTopologyNodeEdgePoint(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.node-edge-point.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/topology/mcp-base-topology/node-edge-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidNodeEdgePoint(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.context.topologycontext.topology.node.ownednodeedgepoint.CepList</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/node-edge-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyTopologyUuidNodeEdgePoint(topologyUuid, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.connectivity.context.topologycontext.topology.node.ownednodeedgepoint.CepList</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}/node-edge-point/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextTopologyContextTopologyTopologyUuidNodeEdgePointNodeEdgePointUuid(topologyUuid, nodeEdgePointUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.node.OwnedNodeEdgePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiTopologyTopologyContextTopologyTopologyUuidNodeEdgePointNodeEdgePointUuid(topologyUuid, nodeEdgePointUuid, callback)</td>
    <td style="padding:15px">returns tapi.topology.node.OwnedNodeEdgePoint</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-topology:topology-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextPhysicalContextDevice(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.device.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/physical-context/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextPhysicalSpan(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.PhysicalSpan</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/physical-span/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextPhysicalSpan(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.PhysicalSpan</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/physical-span/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextPhysicalSpanUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.PhysicalSpan</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextPhysicalSpanUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.PhysicalSpan</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextDevice(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Device</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextDevice(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Device</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextDeviceUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Device</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextDeviceUuid(uuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Device</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextPhysicalContextEquipment(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.equipment.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/physical-context/equipment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextEquipment(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Equipment</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/equipment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextEquipment(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Equipment</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/equipment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextEquipmentEquipmentUuid(equipmentUuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Equipment</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextEquipmentEquipmentUuid(equipmentUuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Equipment</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextDeviceUuidEquipmentEquipmentUuid(uuid, equipmentUuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Equipment</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiCoreContextPhysicalContextEquipmentholder(token, limit, callback)</td>
    <td style="padding:15px">returns deeppaging.equipmentholder.Response</td>
    <td style="padding:15px">{base_path}/{version}/tapi/core/context/physical-context/equipmentholder/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextEquipmentholder(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Holder</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/equipmentholder/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextEquipmentholder(pageNumber, pageSize, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Holder</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/equipmentholder/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextPhysicalContextEquipmentholderEquipmentholderUuid(equipmentholderUuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Holder</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataTapiCommonContextTapiEquipmentPhysicalContextEquipmentholderEquipmentholderUuid(equipmentholderUuid, callback)</td>
    <td style="padding:15px">returns tapi.equipment.Holder</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/tapi-common:context/tapi-equipment:physical-context/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextStreamContextAvailableStream(callback)</td>
    <td style="padding:15px">returns tapi.streaming.AvailableStream</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/stream-context/available-stream/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTapiDataContextStreamContextSupportedStreamType(callback)</td>
    <td style="padding:15px">returns tapi.streaming.streamcontext.SupportedStreamType</td>
    <td style="padding:15px">{base_path}/{version}/tapi/data/context/stream-context/supported-stream-type/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestconfDataIetfRestconfMonitoringRestconfStateCapabilities(callback)</td>
    <td style="padding:15px">return tapi.restconf.capabilities.Response</td>
    <td style="padding:15px">{base_path}/{version}/restconf/data/ietf-restconf-monitoring:restconf-state/capabilities/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFres(tpeId, networkConstructId, layerRate, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, childFreId, endpointTpeConcrete, freExpectationsIntentId, identifierKey, identifierValue, freType, exclude, signalContentType, roadmLineId, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFre(body, callback)</td>
    <td style="padding:15px">Creates a FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFrebyId(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreById(freId, fre, callback)</td>
    <td style="padding:15px">Update a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unprovision(freId, callback)</td>
    <td style="padding:15px">De-provision an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiFresFreId(freId, body, callback)</td>
    <td style="padding:15px">Performs update operations on an FRE resource</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreAdminState(freId, adminStateValue, callback)</td>
    <td style="padding:15px">Set the AdminState of an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/adminState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreBookingData(freId, bookingDataKey, bookingDataValue, callback)</td>
    <td style="padding:15px">Create or update a bookingData attribute on a specified FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/bookingData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreBookingData(freId, bookingDataKey, callback)</td>
    <td style="padding:15px">Delete a bookingData attribute from an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/bookingData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreExpectationMismatches(freId, callback)</td>
    <td style="padding:15px">Retrieves expectation mismatches, if any, for the specified FRE ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/expectationMismatches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFREExpectation(freId, body, callback)</td>
    <td style="padding:15px">Post an FRE Expectation given the FRE id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiFresFreIdExpectationsExpectationIdRealize(freId, expectationId, callback)</td>
    <td style="padding:15px">Perform a provision operation on a FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFreExpectation(freId, freExpId, body, callback)</td>
    <td style="padding:15px">Update attributes on an expectation on the FRE by specific FRE ID and FRE Expectation ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreExpectation(freId, callback)</td>
    <td style="padding:15px">Retrieve FRE Expectations with freId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/freExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreExpectationWithExpId(freId, freExpectationId, callback)</td>
    <td style="padding:15px">Retrieve FRE Expectations with freId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/freExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreExpectationById(freId, freExpectationId, callback)</td>
    <td style="padding:15px">Delete a FRE Expectaion given the fre id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/freExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlannedAttributesFrebyId(freId, include, callback)</td>
    <td style="padding:15px">Retrieves the planned FRE by specific FRE ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/frePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFrePlanned(freId, body, callback)</td>
    <td style="padding:15px">Creates and updates a planned FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/frePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPlannedFre(freId, body, callback)</td>
    <td style="padding:15px">Update attributes on the planned FRE by specific FRE ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/frePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFreIdentifier(freId, identifierRO, callback)</td>
    <td style="padding:15px">Post an FRE Identifier to a given FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreIdentifier(freId, identifierRO, callback)</td>
    <td style="padding:15px">Deletes the FRE identifier from a given FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreOperationV6(freId, operation = 'revert', provisioningAttributes, callback)</td>
    <td style="padding:15px">execute FRE Operation on NE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreUserData(freId, userDataKey, userDataValue, callback)</td>
    <td style="padding:15px">Create or update a userData attribute on an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreUserData(freId, userDataKey, callback)</td>
    <td style="padding:15px">Delete a userData attribute from an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFreValidationExpectation(freId, body, callback)</td>
    <td style="padding:15px">Creates or updates FRE validation expectation for a deployed/discovered FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/fres/{pathv1}/validationExpectation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFre(tpeId, ncId, networkConstructId, type, group = 'dwa', offset, limit, include, layerRate, freType, userLabel, managementName, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, childFreId, exclude = 'actual', signalContentType, srlg, roadmLineId, searchText, includeMetaData, endpointTpeConcrete, identifierKey, identifierValue, fields, directionality = 'unidirectional', callback)</td>
    <td style="padding:15px">Retrieve FRE satisfying the provided parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFreV2(body, callback)</td>
    <td style="padding:15px">Creates or updates a FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreBookingDataV2(freId, bookingDataKey, bookingDataValue, callback)</td>
    <td style="padding:15px">Create or update a bookingData attribute on a specified FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/bookingData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreBookingDataV2(freId, bookingDataKey, callback)</td>
    <td style="padding:15px">Delete a bookingData attribute from an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/bookingData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreExpectationV2(freId, callback)</td>
    <td style="padding:15px">Retrieve FRE Expectations with freId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/freExpectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreExpectationWithExpIdV2(freId, freExpectationId, callback)</td>
    <td style="padding:15px">Retrieve FRE Expectations with freId and expectationId</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/freExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreExpectationByIdV2(freId, freExpectationId, callback)</td>
    <td style="padding:15px">Delete a FRE Expectaion given the fre id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/freExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionOperationsV2(freId, body, callback)</td>
    <td style="padding:15px">Perform a provision operation on a FRE(Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/provisioningOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreUserDataV2(freId, userDataKey, userDataValue, callback)</td>
    <td style="padding:15px">Create or update a userData attribute on an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreUserDataV2(freId, userDataKey, callback)</td>
    <td style="padding:15px">Delete a userData attribute from an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/userData/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFreIdentifiers(id, identifierKey, body, callback)</td>
    <td style="padding:15px">Post an FRE Identifier to a given FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreIdentifiers(id, identifierKey, callback)</td>
    <td style="padding:15px">Delete an Identifier from a given identifier</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v2_0/fres/{pathv1}/identifiers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFresV3(tpeId, networkConstructId, group = 'dwa', freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, childFreId, endpointTpeConcrete, identifierKey, identifierValue, type, offset, limit, include, layerRate, freType, userLabel, managementName, exclude = 'actual', signalContentType, srlg, roadmLineId, searchText, includeMetaData, fields, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreDeploymentState(freExpectationsServiceIntentId, freId, deploymentStateValue, callback)</td>
    <td style="padding:15px">Set the deploymentState of FREs of the specified intent or children FREs of the specified top-level</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/deploymentState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3FresFreId(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unprovisionV3(freId, callback)</td>
    <td style="padding:15px">De-provision an actul FRE or  delete a root FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreAdminStateV3(freId, adminStateValue, callback)</td>
    <td style="padding:15px">Set the AdminState of an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/adminState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDiscoveredAgainstPlanned(freId, callback)</td>
    <td style="padding:15px">Validate discovered FRE(s) against planned FRE(s) hierarchy for a parent FRE resource identified by</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/discovered/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFREExpectationV3(freId, body, callback)</td>
    <td style="padding:15px">Post an FRE Expectation given the FRE id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/expectations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionOperationsV3(freId, expectationId, callback)</td>
    <td style="padding:15px">Perform a provision operation on a FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/expectations/{pathv2}/realize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreExpectationByIdV3(freId, freExpectationId, callback)</td>
    <td style="padding:15px">Delete a FRE Expectaion given the fre id and expectation id</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/freExpectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFreIdentifierV3(freId, identifierRO, callback)</td>
    <td style="padding:15px">Posts or updates a FRE identifier to a given FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFreIdentifierV3(freId, identifierRO, callback)</td>
    <td style="padding:15px">Deletes the FRE identifier from a given FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/identifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFrePlannedWithExpectation(freId, body, callback)</td>
    <td style="padding:15px">Creates or updates FRE validation expectation for a deployed/discovered FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/fres/{pathv1}/validationExpectation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFresV4(id, searchText, searchFields, resourceState, layerRate, networkConstructId, tpeId, identifierKey, identifierValue, concrete, modelType, bookingDataLockout = 'true', group = 'dwa', freType, userLabel, managementName, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, childFreId, exclude = 'actual', signalContentType, srlg, roadmLineId, endpointTpeConcrete, deploymentState, active = 'true', namedQuery, directionality = 'unidirectional', networkRole = 'IFRE', type, serviceClass = 'EVC', layerRateQualifier, supportedByFreId, supportingFreId, customerName, sncgUserlabel, tunnelType, adminState, operationState, lqsDataStatus = 'good', lqsDataMarginValid = 'true', lqsDataFiberReconciled = 'true', lqsDataFiberMethod = 'totalPower', lqsDataMarginViableAtEol, restorationHealthTotalExplicitRoutes, restorationHealthAvailableExplicitRoutes, restorationHealthUnavailableExplicitRoutes, restorationHealthAvailablePercentage, restorationHealthUnavailablePercentage, restorationHealthHomeAvailable, coroutedFreId, tags, displayAdminState, displayOperationState, displayTopologySource, fields, sortBy, offset, limit, metaDataFields, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFreV4(body, callback)</td>
    <td style="padding:15px">Creates a FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreByIdV4(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFreByIdV4(freId, fre, callback)</td>
    <td style="padding:15px">Updates an FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOperationV4(freId, body, callback)</td>
    <td style="padding:15px">Performs update operations on an FRE resource</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreExpectationMismatchesV4(freId, callback)</td>
    <td style="padding:15px">Retrieves expectation mismatches, if any, for the specified FRE ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}/expectationMismatches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV4FresFreIdExpectationsFreExpId(freId, freExpId, body, callback)</td>
    <td style="padding:15px">Update attributes on an expectation on the FRE by specific FRE ID and FRE Expectation ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}/expectations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFrePlannedById(freId, include, callback)</td>
    <td style="padding:15px">Retrieves the planned FRE by specific FRE ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}/frePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFrePlannedV4(freId, body, callback)</td>
    <td style="padding:15px">Creates and updates a planned FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}/frePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV4FresFreIdFrePlanned(freId, body, callback)</td>
    <td style="padding:15px">Update attributes on the planned FRE by specific FRE ID</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/fres/{pathv1}/frePlanned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFresV5(tpeId, networkConstructId, layerRate, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, childFreId, endpointTpeConcrete, freExpectationsIntentId, identifierKey, identifierValue, freType, exclude, signalContentType, roadmLineId, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreByIdV5(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v5/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFresV6(tpeId, networkConstructId, layerRate, freExpectationsServiceIntentId, freExpectationsEquipmentIntentId, childFreId, endpointTpeConcrete, freExpectationsIntentId, identifierKey, identifierValue, freType, exclude, signalContentType, roadmLineId, fields, offset, limit, include, callback)</td>
    <td style="padding:15px">Retrieve a list of FREs provided with the satisfying parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/fres?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreByIdV6(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV6FresFreIdOperationsOperation(freId, operation = 'revert', provisioningAttributes, callback)</td>
    <td style="padding:15px">execute FRE Operation on NE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v6/fres/{pathv1}/operations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFreByIdV7(freId, include, fields, callback)</td>
    <td style="padding:15px">Retrieves a specific FRE</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v7/fres/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkdisk(body, callback)</td>
    <td style="padding:15px">check if disk space is critical</td>
    <td style="padding:15px">{base_path}/{version}/sftp/api/v1/checkdisk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkkafka(body, callback)</td>
    <td style="padding:15px">check if kafka is up</td>
    <td style="padding:15px">{base_path}/{version}/sftp/api/v1/checkkafka?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkpithos(body, callback)</td>
    <td style="padding:15px">check if pithos is up</td>
    <td style="padding:15px">{base_path}/{version}/sftp/api/v1/checkpithos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cleanup(body, callback)</td>
    <td style="padding:15px">CleanUp the data directory used for sftp/sftp</td>
    <td style="padding:15px">{base_path}/{version}/sftp/api/v1/cleanup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentGraphic(siteId, ncId, ipAddress, callback)</td>
    <td style="padding:15px">Retrieves equipment graphics satisfying input parameters</td>
    <td style="padding:15px">{base_path}/{version}/slv-support/api/v1/equipmentGraphics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEquipmentLayout(siteId, ncId, resourceType, ipAddress, callback)</td>
    <td style="padding:15px">Retrieves equipment layouts for requested resource type.</td>
    <td style="padding:15px">{base_path}/{version}/slv-support/api/v1/equipmentLayouts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getShelfGraphic(siteId, ncId, ipAddress, callback)</td>
    <td style="padding:15px">Retrieves shelf graphics to satisfy input parameters</td>
    <td style="padding:15px">{base_path}/{version}/slv-support/api/v1/shelfGraphics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getShelfLayout(siteId, ncId, resourceType, ipAddress, callback)</td>
    <td style="padding:15px">Retrieves shelf layouts for requested resource type.</td>
    <td style="padding:15px">{base_path}/{version}/slv-support/api/v1/shelfLayouts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobs(fromTime, toTime, jobType, scheduleName, scope, jobStatus, scheduledOnly, includeDetails, metaFields, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobDetailsV3(jobId, downloadReport, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status details</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgJobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiSrlgJobsJobId(jobId, callback)</td>
    <td style="padding:15px">Delete Srlg Job for given Id</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgJobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSchedules(scheduleName, scope, jobType, frequency, metaFields, callback)</td>
    <td style="padding:15px">Get all SRLG Job schedules</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiSrlgSchedules(body, callback)</td>
    <td style="padding:15px">SRLG Job Schedule api to create schedules</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllowedJobs(callback)</td>
    <td style="padding:15px">Get display models for SRLG Scheduler UI</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgSchedules/allowedJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiSrlgSchedulesScheduleId(scheduleId, callback)</td>
    <td style="padding:15px">Delete Srlg Job Schedule for schedule Id</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/srlgSchedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV1Srlg(body, callback)</td>
    <td style="padding:15px">Updates SRLG on roadm lines</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV1SrlgResourceId(resourceId, callback)</td>
    <td style="padding:15px">Get SRLG by ID</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v1/srlg/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2Srlg(resourceId, resourceType, isStructured, plannedStartTime, srlgType, callback)</td>
    <td style="padding:15px">Get Srlg by Resource ID</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV2Srlg(body, callback)</td>
    <td style="padding:15px">API to trigger Srlg Batch Operations</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIfdApiV2Srlg(body, callback)</td>
    <td style="padding:15px">Updates SRLG on given resource</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2SrlgExternalSRLGs(userLabel, isStructured, offset, limit, callback)</td>
    <td style="padding:15px">Query External Assigned Srlgs</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/externalSRLGs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV2SrlgExternalSRLGs(body, callback)</td>
    <td style="padding:15px">Assign or Release Srlg Values from Pool</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/externalSRLGs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2SrlgExternalSRLGsExtSRLGId(extSRLGId, isStructured, callback)</td>
    <td style="padding:15px">Query ExternalSrlg assignments.</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/externalSRLGs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2SrlgJobStatus(jobId, jobType, jobStatus, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/job/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2SrlgJobJobIdDetails(jobId, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status details</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/job/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2SrlgSrlgMismatch(ncId, resourceType, resourceId, callback)</td>
    <td style="padding:15px">Search resources which have srlg mismatch</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/srlgMismatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV2SrlgSrlgValueAssignmentDetails(srlgValue, srlgType, resourceType, callback)</td>
    <td style="padding:15px">Search resources which have been assigned with given SRLG value</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v2/srlg/{pathv1}/assignmentDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3Srlg(resourceId, resourceType, isStructured, plannedStartTime, srlgType, callback)</td>
    <td style="padding:15px">Get Srlg by Resource ID</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgAssociatedSrlgs(resourceId, resourceType, srlgSource, includeMetaData, callback)</td>
    <td style="padding:15px">Provide details of SRLGs associated with the given resource</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlg/associatedSrlgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgJobStatus(jobId, jobType, jobStatus, callback)</td>
    <td style="padding:15px">Get SRLG Job operation status</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlg/job/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgSrlgMismatch(ncId, resourceType = 'fres', resourceId, offset, limit, callback)</td>
    <td style="padding:15px">Search resources which have srlg mismatch</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlg/srlgMismatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgSrlgMismatchMismatchIdDetails(mismatchId, callback)</td>
    <td style="padding:15px">Get Srlg Mismatch details for given mismatchId</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlg/srlgMismatch/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgSrlgValueAssignmentDetails(srlgValue, srlgSource, resourceType, includeMetaData, callback)</td>
    <td style="padding:15px">Search resources which have been assigned with given SRLG value</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlg/{pathv1}/assignmentDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgJobs(fromTime, toTime, jobType, scheduleName, scope, jobStatus, scheduledOnly, includeDetails, metaFields, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgJobsJobId(jobId, downloadReport, callback)</td>
    <td style="padding:15px">GET SRLG Job operation status details</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgJobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSrlgJob(jobId, callback)</td>
    <td style="padding:15px">Delete Srlg Job for given Id</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgJobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgSchedules(scheduleName, scope, jobType, frequency, metaFields, callback)</td>
    <td style="padding:15px">Get all SRLG Job schedules</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIfdApiV3SrlgSchedules(body, callback)</td>
    <td style="padding:15px">SRLG Job Schedule api to create schedules</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIfdApiV3SrlgSchedulesAllowedJobs(callback)</td>
    <td style="padding:15px">Get display models for SRLG Scheduler UI</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgSchedules/allowedJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIfdApiV3SrlgSchedulesScheduleId(scheduleId, callback)</td>
    <td style="padding:15px">Delete Srlg Job Schedule for schedule Id</td>
    <td style="padding:15px">{base_path}/{version}/ifd/api/v3/srlgSchedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrlgFormat(callback)</td>
    <td style="padding:15px">Retrieves the srlg Format</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSrlgFormat(body, callback)</td>
    <td style="padding:15px">PATCH a srlg Format</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssignableEntityConfigurations(entityType, riskType, callback)</td>
    <td style="padding:15px">Retrieves the srlg Assignable Entity Configurations satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/assignableEntityConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAssignableEntityConfig(assignableEntityConfigId, body, callback)</td>
    <td style="padding:15px">PATCH the Criteria for Auto-assignable Entities to be considered</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/assignableEntityConfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRiskTypes(entityType, callback)</td>
    <td style="padding:15px">Retrieves the srlg riskTypes satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/assignableEntityConfigurations/{pathv1}/riskTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSrlgPropagatableServiceConfig(body, callback)</td>
    <td style="padding:15px">POST SRLG Format propagatable service configurations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/propagatableServiceConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSrlgPropagationServiceConfig(body, callback)</td>
    <td style="padding:15px">Deletes SRLG Format propagatable service configurations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/propagatableServiceConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPropagatableEntityConfig(propagatableServiceConfigId, body, callback)</td>
    <td style="padding:15px">PATCH the Criteria for Propagatable Services Config to be considered</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/propagatableServiceConfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">transformSrlg(body, callback)</td>
    <td style="padding:15px">Encode/Decode Srlg values from Native To Structured SRLG as per the configured Srlg format</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlg/transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateSrlg(body, callback)</td>
    <td style="padding:15px">Validate a set of given Srlg against the configured format</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlg/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSrlgAutoAssignmentBehavior(isAutoAssignmentOn, callback)</td>
    <td style="padding:15px">Sets the Srlg auto assignment behavior</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlgAutoAssignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrlgFormatMappings(type, callback)</td>
    <td style="padding:15px">Retrieves the srlg Format Mappings satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlgFormatMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSrlgFormatMappings(body, callback)</td>
    <td style="padding:15px">POST SRLG Format mapping definitions</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlgFormatMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSrlgFormatMappings(body, callback)</td>
    <td style="padding:15px">Deletes SRLG Format mapping definitions</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlgFormatMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSrlgPropagationBehavior(isPropagationOn, callback)</td>
    <td style="padding:15px">Sets the Propagation behavior</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgformat/srlgPropagation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3Srlgformat(callback)</td>
    <td style="padding:15px">Retrieves the Srlg Format</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3Srlgformat(body, callback)</td>
    <td style="padding:15px">PATCH a srlg Format</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3SrlgformatAssignableEntityConfigurations(entityType, riskType, callback)</td>
    <td style="padding:15px">Retrieves the srlg Assignable Entity Configurations satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/assignableEntityConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3SrlgformatAssignableEntityConfigurationsAssignableEntityConfigId(assignableEntityConfigId, body, callback)</td>
    <td style="padding:15px">PATCH the Criteria for Auto-assignable Entities to be considered</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/assignableEntityConfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3SrlgformatAssignableEntityConfigurationsEntityTypeRiskTypes(entityType, callback)</td>
    <td style="padding:15px">Retrieves the srlg riskTypes satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/assignableEntityConfigurations/{pathv1}/riskTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3SrlgformatPropagatableServiceConfigurations(body, callback)</td>
    <td style="padding:15px">POST SRLG Format propagatable service configurations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/propagatableServiceConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3SrlgformatPropagatableServiceConfigurations(body, callback)</td>
    <td style="padding:15px">Deletes SRLG Format propagatable service configurations</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/propagatableServiceConfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNsiApiV3SrlgformatPropagatableServiceConfigurationsPropagatableServiceConfigId(propagatableServiceConfigId, body, callback)</td>
    <td style="padding:15px">PATCH the Criteria for Propagatable Services Config to be considered</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/propagatableServiceConfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3SrlgformatSrlgTransform(body, callback)</td>
    <td style="padding:15px">POST a srlg value Transform</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlg/transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3SrlgformatSrlgValidate(body, callback)</td>
    <td style="padding:15px">POST a srlg Validation</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlg/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3SrlgformatSrlgAutoAssignment(isAutoAssignmentOn, callback)</td>
    <td style="padding:15px">Sets the auto assignment behavior</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlgAutoAssignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsiApiV3SrlgformatSrlgFormatMappings(type, callback)</td>
    <td style="padding:15px">Retrieves the srlg Format Mappings satisfying the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlgFormatMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV3SrlgformatSrlgFormatMappings(body, callback)</td>
    <td style="padding:15px">POST SRLG Format mapping definitions</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlgFormatMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsiApiV3SrlgformatSrlgFormatMappings(body, callback)</td>
    <td style="padding:15px">Deletes SRLG Format mapping definitions</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlgFormatMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsiApiV3SrlgformatSrlgPropagation(isPropagationOn, callback)</td>
    <td style="padding:15px">Sets the Propagation behavior</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v3/srlgformat/srlgPropagation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV4SrlgformatSrlgTransform(body, callback)</td>
    <td style="padding:15px">POST a srlg value Transform</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v4/srlgformat/srlg/transform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performExternalSRLGOperation(body, callback)</td>
    <td style="padding:15px">API to get next available SRLG or release SRLG for external pool.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/srlgpool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsiApiV1Srlgpool(body, callback)</td>
    <td style="padding:15px">API to get next available SRLG or release SRLG for external pool.</td>
    <td style="padding:15px">{base_path}/{version}/nsi/api/v1/srlgpool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsScanRepeaters(scanId, rptrSysFamily, band, callback)</td>
    <td style="padding:15px">Get Scan Details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scanRepeaters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsScanRepeaters(ncId, scanParams, callback)</td>
    <td style="padding:15px">Initatiate new scan</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scanRepeaters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubmarineApiV1SrsScanRepeaters(ids, callback)</td>
    <td style="padding:15px">Delete Scan Record</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scanRepeaters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsHistoryScans(ncId, callback)</td>
    <td style="padding:15px">Get History scan details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/historyScans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsScheduleScan(scheduleId, ncId, callback)</td>
    <td style="padding:15px">Get Schedule Details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scheduleScan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsScheduleScan(scheduleScan, callback)</td>
    <td style="padding:15px">Schedule a scan</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scheduleScan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubmarineApiV1SrsScheduleScan(ids, callback)</td>
    <td style="padding:15px">Delete Schedule Scan Record</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scheduleScan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsConfigUbmd2Loss(ncId, facilityAid, callback)</td>
    <td style="padding:15px">Get config loss Details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/configUbmd2Loss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsConfigUbmd2Loss(configLosses, callback)</td>
    <td style="padding:15px">Configure UBMD2 loss</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/configUbmd2Loss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsScanThreshold(scanId, callback)</td>
    <td style="padding:15px">Get baseline details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scanThreshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsScanThreshold(scanThreshold, callback)</td>
    <td style="padding:15px">Save baseline details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/scanThreshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsUpload(importType, file, name, reImport, callback)</td>
    <td style="padding:15px">Import scan result, cable system, repeater system and slte files</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsUpload(detailsType, name, callback)</td>
    <td style="padding:15px">Get cable system, repeater system, slte details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsCableSystemDetails(resourceType, cableSystem, cableStation, segment, customer, fiberPair, callback)</td>
    <td style="padding:15px">Get cable system topology view, attributes and repeaters details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/cableSystemDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubmarineApiV1SrsUpload(deleteType, data, callback)</td>
    <td style="padding:15px">Delete repeater, slte and cable descriptor files</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsExport(ids, exportAll, callback)</td>
    <td style="padding:15px">Export data</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SrsCommitCableSystem(data, callback)</td>
    <td style="padding:15px">Commit cable system configuration for a customer</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/commitCableSystem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SrsTrendData(scanId, repeaterAddress, startTime, endTime, band = 'C', callback)</td>
    <td style="padding:15px">Get trend details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/srs/trendData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1WetplantComponents(resourceType, cableSystem, temsServer, componentName, fromDate, toDate, callback)</td>
    <td style="padding:15px">Get wetplant details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/wetplant/components?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubmarineApiV1WetplantComponents(cableSystem, callback)</td>
    <td style="padding:15px">Delete optical power data</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/wetplant/components?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1SpectrumSharingApp(callback)</td>
    <td style="padding:15px">Get terminal data</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/spectrumSharingApp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SpectrumSharingDetails(spectrumSharingDetails, callback)</td>
    <td style="padding:15px">Get details for a spectrum sharing terminal</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/spectrumSharingDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1ProvisionSpectrum(provisionSpectrum, callback)</td>
    <td style="padding:15px">Provision spectrum for a customer</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/provisionSpectrum?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubmarineApiV1ProvisionSpectrum(ncId, smdAid, wssAid, wssToSmdPort, wssToAsePort, callback)</td>
    <td style="padding:15px">Get available CHC index and frequency range</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/provisionSpectrum?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubmarineApiV1ProvisionSpectrum(provisionSpectrum, callback)</td>
    <td style="padding:15px">Remove spectrum for a customer</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/provisionSpectrum?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubmarineApiV1SpectrumFacility(nmccDetails, callback)</td>
    <td style="padding:15px">Fetch NMCC details</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/spectrumFacility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSubmarineApiV1SpectrumFacility(updateSwitchSelector, callback)</td>
    <td style="padding:15px">Update switch selector of CHC</td>
    <td style="padding:15px">{base_path}/{version}/submarine/api/v1/spectrumFacility?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProps(callback)</td>
    <td style="padding:15px">Retrieve all properties.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/configProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUpdateProp(body, callback)</td>
    <td style="padding:15px">Add/update a property by property name.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/configProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPropValueByName(propertyName, callback)</td>
    <td style="padding:15px">Retrieve property by property name.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/configProperties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePropByName(propertyName, callback)</td>
    <td style="padding:15px">Delete a property by property name.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/configProperties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFiberLossData(freIds, callback)</td>
    <td style="padding:15px">Retrieve measured fiber loss data on the path of one service</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/diag/fiberloss?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkPrefecBer(freIds, failThreshold, degradeThreshold, resetIdf, callback)</td>
    <td style="padding:15px">Test pre-FEC BER for the given list of provisioned services or infrastructure links</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/diag/pms/prfbers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testPrefecBer(freId, targetThreshold, callback)</td>
    <td style="padding:15px">Test pre-FEC BER for a provisioned service or infrastructure link and provide complex test results</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/diag/pms/prfbers/complex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpectralDiagData(freIds, callback)</td>
    <td style="padding:15px">Retrieve spectral data on the path of one service</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/diag/spectral?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpectralAllocData(freIds, callback)</td>
    <td style="padding:15px">Retrieve spectral allocation data for path</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/diag/spectral/allocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperStatus(freId, osTypeString = 'ccStatus', callback)</td>
    <td style="padding:15px">Retrieve operational status.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/operationalStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageLoadInformation(freId, includedInformation, callback)</td>
    <td style="padding:15px">Retrieve test capabilities and operational status (ccm, pw, lsp, tdm) of a specific FRE.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/pageLoad/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestList(testType = 'benchmark', freId, tpeIds, ncIds, userAnnotation, callback)</td>
    <td style="padding:15px">Retrieve test results list.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestCapabilities(freId, callback)</td>
    <td style="padding:15px">Retrieve test capabilities of a specific FRE.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/Description?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestCapabilitiesL3(operationType, ncId, callback)</td>
    <td style="padding:15px">Retrieve test capabilities of a specific FRE.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/DescriptionL3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startBmTest(body, callback)</td>
    <td style="padding:15px">Start a benchmark test</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/benchmarkOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startCfmTest(body, callback)</td>
    <td style="padding:15px">Start a CFM test: linktrace, loopback</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/cfmOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">itsInjectError(body, callback)</td>
    <td style="padding:15px">Inject Error on running Integrated Test Set test</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/itsInjectError?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startItsTest(body, callback)</td>
    <td style="padding:15px">Start a Integrated Test Set test: raceTrack, testSet, networkMode.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/itsOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startLdpTest(body, callback)</td>
    <td style="padding:15px">Start a LDP test: ping</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/ldpOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startLbTest(body, callback)</td>
    <td style="padding:15px">Start a photonics loopback test</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/loopbackOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startLspTest(body, callback)</td>
    <td style="padding:15px">Start a LSP test: ping, traceroute</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/lspOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPwTest(body, callback)</td>
    <td style="padding:15px">Start a Pseudowire test: ping</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/pwOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reflectorOp(body, callback)</td>
    <td style="padding:15px">Create/Delete Reflector</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/reflectorOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSrTest(body, callback)</td>
    <td style="padding:15px">Start a SR test: ping, traceroute</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/srOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSrPolicyTest(body, callback)</td>
    <td style="padding:15px">Start a VRF test: ping</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/srPolicyOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startTdmTest(body, callback)</td>
    <td style="padding:15px">Start a tdm test</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/tdmOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startVrfTest(body, callback)</td>
    <td style="padding:15px">Start a VRF test: ping</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/vrfOperations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestRecordById(testId, callback)</td>
    <td style="padding:15px">Retrieve test record for a specific testId</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelInProgressTest(testId, callback)</td>
    <td style="padding:15px">Cancel an in progress test</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configure(body, callback)</td>
    <td style="padding:15px">Configures Integrated Test Set configuration</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/its/itsConfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItsTestDescription(freId, testType, callback)</td>
    <td style="padding:15px">Retrieve Integrated Test Set Description data.</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/its/itsDescription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unConfigure(body, callback)</td>
    <td style="padding:15px">Unconfigures Integrated Test Set configuration</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v1/tests/its/itsUnconfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startRouteDiag(body, callback)</td>
    <td style="padding:15px">Retrieves current operational measurements for a broadband service or infrastructure service</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v2/diag/operMeas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startFacilityDiag(body, callback)</td>
    <td style="padding:15px">Retrieves historical operational measurements data for a given facility</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v2/diag/operMeas/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startLatencyFacilityDiag(body, callback)</td>
    <td style="padding:15px">Retrieves latency data for a given resource ID</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v2/diag/operMeas/latency/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPowerSnapshotAttributes(body, callback)</td>
    <td style="padding:15px">Retrieves Power Snapshot data for a given service</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v2/diag/operMeas/powerAttributes/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPowerDiag(body, callback)</td>
    <td style="padding:15px">Retrieves Optical Power PM data for a given service</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v2/diag/operMeas/powerPms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPms(body, callback)</td>
    <td style="padding:15px">Reset current operational measurements for a broadband service or infrastructure service</td>
    <td style="padding:15px">{base_path}/{version}/tdc/api/v2/diag/operMeas/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopic(topicName, callback)</td>
    <td style="padding:15px">Returns a topic and its metadata</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/topics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alterTopic(topicName, body, callback)</td>
    <td style="padding:15px">ONLY USE DURING MAINTENANCE WINDOW!! Alters a topic and its metadata</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/topics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopic(topicName, callback)</td>
    <td style="padding:15px">ONLY USE DURING MAINTENANCE WINDOW!! Deletes a topic and its metadata</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/topics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopic(body, callback)</td>
    <td style="padding:15px">Creates a topic with the provided configuration</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/topics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopicState(topicName, callback)</td>
    <td style="padding:15px">Returns topic state</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/monitor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopicConsumer(topicName, consumerGroup, callback)</td>
    <td style="padding:15px">Returns a topic state with respect to a consumergroup</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/monitor/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopicConsumerProcessState(topicName, consumerGroup, callback)</td>
    <td style="padding:15px">Returns process state of a consumergroup for a topic</td>
    <td style="padding:15px">{base_path}/{version}/kafka-rest/api/v1/monitor/process-state/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUpgrades(callback)</td>
    <td style="padding:15px">List details of all upgrade processes</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/api/v1/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeSolutions(body, callback)</td>
    <td style="padding:15px">Trigger Upgrade Process</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/api/v1/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUpgradeJob(jobId, callback)</td>
    <td style="padding:15px">List details of a particular upgrade process</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/api/v1/upgrades/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopUpgradeJob(jobId, callback)</td>
    <td style="padding:15px">Stop a particular upgrade process</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/api/v1/upgrades/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commit(jobId, callback)</td>
    <td style="padding:15px">Trigger cleanup of previous solution</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/api/v1/upgrades/{pathv1}/commit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rollback(jobId, callback)</td>
    <td style="padding:15px">Trigger rollback of upgrade job</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/api/v1/upgrades/{pathv1}/rollback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiKeysList(owner, keyId, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all api-keys.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiKeysCreate(owner, callback)</td>
    <td style="padding:15px">Create a new api-key.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiKeysRead(keyId, callback)</td>
    <td style="padding:15px">Get a specific api-key.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/api-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiKeysDelete(keyId, callback)</td>
    <td style="padding:15px">Delete an api-key.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/api-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsList(name, displayName, isInternal, uuid, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all applications.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsCreate(displayName, description, username, isInternal, name, callback)</td>
    <td style="padding:15px">Create a new application entry.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific application by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsUpdate(uuid, displayName, description, username, isInternal, name, callback)</td>
    <td style="padding:15px">Replace the application with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsPartialUpdate(uuid, displayName, description, username, isInternal, name, callback)</td>
    <td style="padding:15px">Update some values for the application with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationsDelete(uuid, callback)</td>
    <td style="padding:15px">Delete an application.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authCreate(authType, username, password, tenant, token, keyId, apps, callback)</td>
    <td style="padding:15px">Create a new authentication.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">currentUserList(page, limit, callback)</td>
    <td style="padding:15px">Detailed information about the current user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/current-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">currentUserLoginInfo(callback)</td>
    <td style="padding:15px">Returns SessionId and Token Info including tenant_context for the current user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/current-user/login_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheckList(page, limit, callback)</td>
    <td style="padding:15px">Health check</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/health-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ldapConfigsList(page, limit, callback)</td>
    <td style="padding:15px">List of all LDAP configurations.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/ldap-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ldapConfigsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific LDAP config by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/ldap-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ldapConfigsUpdate(uuid, description, name, enabled, serverIp, enableSsl, sslLevel, domainSearchUser, domainSearchPassword, enableReferrals, baseDn, userNameAttribute, tenantAttribute, accessibleTenantsAttribute, groupNameAttribute, groupObjectFilter, memberAttr, roleMap, callback)</td>
    <td style="padding:15px">Replace the LDAP config with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/ldap-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ldapConfigsPartialUpdate(uuid, description, name, enabled, serverIp, enableSsl, sslLevel, domainSearchUser, domainSearchPassword, enableReferrals, baseDn, userNameAttribute, tenantAttribute, accessibleTenantsAttribute, groupNameAttribute, groupObjectFilter, memberAttr, roleMap, callback)</td>
    <td style="padding:15px">Update some values for the LDAP config with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/ldap-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginDetailList(userUuid, isSuccessful, user, ordering, page, limit, callback)</td>
    <td style="padding:15px">Lists the login details associated user's last sessions. For admins/sysadmins,</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/login-detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginInfoList(page, limit, callback)</td>
    <td style="padding:15px">Login Info.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/login-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginMessageList(ordering, page, limit, callback)</td>
    <td style="padding:15px">Get the one and only pre-login message.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/login-message?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginMessageRead(uuid, callback)</td>
    <td style="padding:15px">Get the one and only pre-login message.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/login-message/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginMessageUpdate(uuid, message, callback)</td>
    <td style="padding:15px">Replace the permission with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/login-message/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginMessagePartialUpdate(uuid, message, callback)</td>
    <td style="padding:15px">Update some values for the permission with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/login-message/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutCreate(callback)</td>
    <td style="padding:15px">API endpoint for ui app users to logout.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationConfigList(page, limit, callback)</td>
    <td style="padding:15px">Get the Tron notification configuration.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/notification-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationConfigRead(uuid, callback)</td>
    <td style="padding:15px">Get the Tron notification configuration.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/notification-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationConfigUpdate(uuid, enabled, emailLinkServer, resetLinkPath, confirmLinkPath, callback)</td>
    <td style="padding:15px">Update the Tron notification configuration.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/notification-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notificationConfigPartialUpdate(uuid, enabled, emailLinkServer, resetLinkPath, confirmLinkPath, callback)</td>
    <td style="padding:15px">Update certain fields of the Tron notification configuration.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/notification-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oauth2TokensCreate(username, tenant, tenantContext, password, expiresIn, grantType, inactiveExpirationTime, isSuccessful, userTenantUuid, radiusState, callback)</td>
    <td style="padding:15px">Create an OAuth2 Token.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/oauth2/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oauth2TokensDelete(token, callback)</td>
    <td style="padding:15px">Delete an OAuth2 token.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/oauth2/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oauth2TokensChangeTenantContext(token, username, tenant, tenantContext, password, expiresIn, grantType, inactiveExpirationTime, isSuccessful, userTenantUuid, radiusState, callback)</td>
    <td style="padding:15px">Change to the tenant context specified via tenant_context.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/oauth2/tokens/{pathv1}/change_tenant_context?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partitionsList(uuid, name, displayName, displayNameContains, displayNameStartswith, isActive, search, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all partitions.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/partitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partitionsCreate(displayName, description, isActive, name, callback)</td>
    <td style="padding:15px">Create a new partition.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/partitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partitionsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific partition by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/partitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partitionsUpdate(uuid, displayName, description, isActive, name, callback)</td>
    <td style="padding:15px">Replace the partition with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/partitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partitionsPartialUpdate(uuid, displayName, description, isActive, name, callback)</td>
    <td style="padding:15px">Update some values for the partition with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/partitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partitionsDelete(uuid, callback)</td>
    <td style="padding:15px">Delete the partition with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/partitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordpoliciesList(name, description, uuid, enableLockout, historySize, allowCommonPasswords, allowDictionaryWords, lockoutNonChangedDays, minimumChangeDays, minimumDigits, minimumDistance, minimumLowercase, minimumPasswordLength, minimumSpecialChars, minimumStrength, minimumUppercase, passwordChangeDays, requirePeriodicChange, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all password policies.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/passwordpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordpoliciesCreate(tenant, description, passwordExpirationWarningPeriod, emailPasswordExpirationWeeklyNotifications, emailPasswordExpirationDailyNotifications, enableLockout, failedLoginAttempts, historySize, lockoutDurationMinutes, lockoutNonChangedDays, minimumChangeDays, minimumDigits, minimumLowercase, minimumPasswordLength, minimumSpecialChars, minimumUppercase, passwordChangeDays, requirePeriodicChange, name, callback)</td>
    <td style="padding:15px">Create a new password policy.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/passwordpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordpoliciesRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific password policy by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/passwordpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordpoliciesUpdate(uuid, tenant, description, passwordExpirationWarningPeriod, emailPasswordExpirationWeeklyNotifications, emailPasswordExpirationDailyNotifications, enableLockout, failedLoginAttempts, historySize, lockoutDurationMinutes, lockoutNonChangedDays, minimumChangeDays, minimumDigits, minimumLowercase, minimumPasswordLength, minimumSpecialChars, minimumUppercase, passwordChangeDays, requirePeriodicChange, name, callback)</td>
    <td style="padding:15px">Replace the password policy with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/passwordpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordpoliciesPartialUpdate(uuid, tenant, description, passwordExpirationWarningPeriod, emailPasswordExpirationWeeklyNotifications, emailPasswordExpirationDailyNotifications, enableLockout, failedLoginAttempts, historySize, lockoutDurationMinutes, lockoutNonChangedDays, minimumChangeDays, minimumDigits, minimumLowercase, minimumPasswordLength, minimumSpecialChars, minimumUppercase, passwordChangeDays, requirePeriodicChange, name, callback)</td>
    <td style="padding:15px">Update some values for the password policy with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/passwordpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsList(uuid, isDefault, customerModifiable, category, categoryContains, categoryStartswith, displayName, displayNameContains, displayNameStartswith, name, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all permissions.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsCreate(displayName, category, operation, description, standby, name, callback)</td>
    <td style="padding:15px">Create a new permission.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific permission by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsUpdate(uuid, displayName, category, operation, description, standby, name, callback)</td>
    <td style="padding:15px">Defines permissions for the roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsPartialUpdate(uuid, displayName, category, operation, description, standby, name, callback)</td>
    <td style="padding:15px">Defines permissions for the roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsDelete(uuid, callback)</td>
    <td style="padding:15px">Defines permissions for the roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsAddResources(uuid, resources, callback)</td>
    <td style="padding:15px">Defines permissions for the roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions/{pathv1}/add_resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">permissionsRemoveResources(uuid, resources, callback)</td>
    <td style="padding:15px">Defines permissions for the roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/permissions/{pathv1}/remove_resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">radiusConfigsList(page, limit, callback)</td>
    <td style="padding:15px">List of all RADIUS configurations.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/radius-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">radiusConfigsCreate(description, tenant, serverIp, retries, authport, name, enabled, serverSecret, roleMap, authoritativeRoleSource, timeout, heartbeatUser, heartbeatPwd, callback)</td>
    <td style="padding:15px">Stop user from creating more than two configurations per tenant.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/radius-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">radiusConfigsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific RADIUS config by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/radius-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">radiusConfigsUpdate(uuid, description, tenant, serverIp, retries, authport, name, enabled, serverSecret, roleMap, authoritativeRoleSource, timeout, heartbeatUser, heartbeatPwd, callback)</td>
    <td style="padding:15px">Replace the RADIUS configuration with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/radius-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">radiusConfigsPartialUpdate(uuid, description, tenant, serverIp, retries, authport, name, enabled, serverSecret, roleMap, authoritativeRoleSource, timeout, heartbeatUser, heartbeatPwd, callback)</td>
    <td style="padding:15px">Update some values for the RADIUS config with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/radius-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesList(applicationUuid, applicationName, uuid, user, application, isInternal, customerModifiable, bypassDormancy, name, displayName, displayNameContains, displayNameStartswith, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesCreate(displayName, description, application, reservedSessions, name, allPartitions, parents, callback)</td>
    <td style="padding:15px">Create a new role.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesTotalReservedSessions(callback)</td>
    <td style="padding:15px">Return the total number of reserved sessions across all the Roles</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/total_reserved_sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific role by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesUpdate(uuid, displayName, description, application, reservedSessions, name, allPartitions, parents, callback)</td>
    <td style="padding:15px">Replace the role with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesPartialUpdate(uuid, displayName, description, application, reservedSessions, name, allPartitions, parents, callback)</td>
    <td style="padding:15px">Update some values for the role with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesDelete(uuid, callback)</td>
    <td style="padding:15px">Defines roles of an application.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesAddParents(uuid, parents, callback)</td>
    <td style="padding:15px">Add parents to this role.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/add_parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesAddPartitions(uuid, partitions, callback)</td>
    <td style="padding:15px">Defines roles of an application.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/add_partitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesAddPermissions(uuid, permissions, callback)</td>
    <td style="padding:15px">Defines roles of an application.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/add_permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesAddUsers(uuid, users, callback)</td>
    <td style="padding:15px">Add users to this role.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/add_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRemoveParents(uuid, parents, callback)</td>
    <td style="padding:15px">Remove parents from this role.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/remove_parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRemovePartitions(uuid, partitions, callback)</td>
    <td style="padding:15px">Defines roles of an application.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/remove_partitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRemovePermissions(uuid, permissions, callback)</td>
    <td style="padding:15px">Defines roles of an application.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/remove_permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesRemoveUsers(uuid, users, callback)</td>
    <td style="padding:15px">Remove users from this role.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/remove_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesSetReservedSessions(uuid, reservedSessions, callback)</td>
    <td style="padding:15px">Allow admin and sysadmin to change the role's reserved_sessions.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/roles/{pathv1}/set_reserved_sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessionsList(sessionId, username, ipAddress, sessionType, usernameContains, usernameStartswith, ipAddressContains, ipAddressStartswith, sessionTypeContains, sessionTypeStartswith, search, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all sessions.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessionsRead(sessionId, callback)</td>
    <td style="padding:15px">Get a specific session.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessionsDelete(sessionId, callback)</td>
    <td style="padding:15px">Delete a session.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tokensCreate(username, tenant, tenantContext, password, inactiveExpirationTime, isSuccessful, timeout, userTenantUuid, radiusState, callback)</td>
    <td style="padding:15px">Create a new base token.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tokensDelete(token, callback)</td>
    <td style="padding:15px">defines tokens of a user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tokensChangeTenantContext(token, tenantContext, callback)</td>
    <td style="padding:15px">Change to the tenant context specified via tenant_context.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/tokens/{pathv1}/change_tenant_context?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsList(name, user, uuid, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of all UserGroups.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsCreate(clientInactivityTime, tokenExpirationTime, dormancyDays, description, concurrentSessionMax, name, parents, callback)</td>
    <td style="padding:15px">Create a new UserGroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific usergroup by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsUpdate(uuid, clientInactivityTime, tokenExpirationTime, dormancyDays, description, concurrentSessionMax, name, parents, callback)</td>
    <td style="padding:15px">Replace the UserGroup with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsPartialUpdate(uuid, clientInactivityTime, tokenExpirationTime, dormancyDays, description, concurrentSessionMax, name, parents, callback)</td>
    <td style="padding:15px">Update some values for the UserGroup with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsDelete(uuid, callback)</td>
    <td style="padding:15px">Delete a UserGroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsAddParents(uuid, parents, callback)</td>
    <td style="padding:15px">Add parent groups to this usergroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}/add_parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsAddRoles(uuid, roles, callback)</td>
    <td style="padding:15px">Add roles to this usergroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}/add_roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsAddUsers(uuid, users, callback)</td>
    <td style="padding:15px">Add users to this usergroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}/add_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsRemoveParents(uuid, parents, callback)</td>
    <td style="padding:15px">Remove parent groups from this usergroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}/remove_parents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsRemoveRoles(uuid, roles, callback)</td>
    <td style="padding:15px">Remove roles from this usergroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}/remove_roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usergroupsRemoveUsers(uuid, users, callback)</td>
    <td style="padding:15px">Remove users from this usergroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/usergroups/{pathv1}/remove_users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersList(uuid, role, usergroup, tenant, currentLoggedIn, currentWithConcurrentSessionCountGreater, isActive, isInternal, isLocked, isStaff, isDormant, isExpired, email, emailStartswith, emailContains, firstName, firstNameStartswith, firstNameContains, lastName, lastNameStartswith, lastNameContains, username, usernameStartswith, usernameContains, search, ordering, page, limit, callback)</td>
    <td style="padding:15px">List of users.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersCreate(clientInactivityTime, tokenExpirationTime, dormancyDays, firstName, lastName, description, email, tenant, accessibleTenants, roles, password, passwordChangeRequired, concurrentSessionMax, isVerboseLogging, username, isActive, isLocked, unlockTime, initDormancyTime, isInternal, directory, lastLoginDetailUuid, failedLoginAttempts, passwordExpirationDays, callback)</td>
    <td style="padding:15px">Create a new user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersRead(uuid, callback)</td>
    <td style="padding:15px">Get a specific user by UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersUpdate(uuid, clientInactivityTime, tokenExpirationTime, dormancyDays, firstName, lastName, description, email, tenant, accessibleTenants, roles, password, passwordChangeRequired, concurrentSessionMax, isVerboseLogging, username, isActive, isLocked, unlockTime, initDormancyTime, isInternal, directory, lastLoginDetailUuid, failedLoginAttempts, passwordExpirationDays, callback)</td>
    <td style="padding:15px">Replace the user with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersPartialUpdate(uuid, clientInactivityTime, tokenExpirationTime, dormancyDays, firstName, lastName, description, email, tenant, accessibleTenants, roles, password, passwordChangeRequired, concurrentSessionMax, isVerboseLogging, username, isActive, isLocked, unlockTime, initDormancyTime, isInternal, directory, lastLoginDetailUuid, failedLoginAttempts, passwordExpirationDays, callback)</td>
    <td style="padding:15px">Update some values for the user with this UUID.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersDelete(uuid, callback)</td>
    <td style="padding:15px">Delete a user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersAddAccessibleTenants(uuid, accessibleTenants, callback)</td>
    <td style="padding:15px">Grant this user additional accessible tenants.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/add_accessible_tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersAddRoles(uuid, roles, callback)</td>
    <td style="padding:15px">Grant this user additional roles.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/add_roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersAddUsergroups(uuid, usergroups, callback)</td>
    <td style="padding:15px">Add this user to a UserGroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/add_usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersContextAccessibleTenants(uuid, callback)</td>
    <td style="padding:15px">Return a list of all active subtenants of current tenant.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/context_accessible_tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersRemoveAccessibleTenants(uuid, accessibleTenants, callback)</td>
    <td style="padding:15px">Remove accessible tenants from this user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/remove_accessible_tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersRemoveRoles(uuid, roles, callback)</td>
    <td style="padding:15px">Remove roles from this user.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/remove_roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersRemoveUsergroups(uuid, usergroups, callback)</td>
    <td style="padding:15px">Remove this user from a UserGroup.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/remove_usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersResetPassword(uuid, currentPassword, newPassword, newPasswordConfirm, force, callback)</td>
    <td style="padding:15px">"Set the given users password (ADMIN).</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/reset_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersSetPassword(uuid, currentPassword, newPassword, newPasswordConfirm, callback)</td>
    <td style="padding:15px">Set the given users password.</td>
    <td style="padding:15px">{base_path}/{version}/tron/api/v1/users/{pathv1}/set_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViabilityApiV1CacheRecompute(deleteOnly, callback)</td>
    <td style="padding:15px">Recomputes the viability cache</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v1/Cache/Recompute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViabilityApiV1ViableRoutesViabilityData(body, callback)</td>
    <td style="padding:15px">Inserts the VP info received from oneplanner into the database.</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v1/ViableRoutes/ViabilityData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViabilityApiV1ViableRoutesViabilityData(freIds, callback)</td>
    <td style="padding:15px">Gets the VP info from the database.</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v1/ViableRoutes/ViabilityData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putViabilityApiV1ViableRoutesViabilityData(body, callback)</td>
    <td style="padding:15px">Inserts or updates the VP info received from the user into the database.</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v1/ViableRoutes/ViabilityData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViabilityApiV1ViableRoutesViabilityDataFile(contentType, contentDisposition, headers, length, name, fileName, callback)</td>
    <td style="padding:15px">Inserts the Verification Paths received from a gzip (.gz), zip (.zip), JSON (.json) or OnePlanner (</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v1/ViableRoutes/ViabilityData/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViabilityApiV1ViableRoutesViabilityDataFile(freIds, fileType, callback)</td>
    <td style="padding:15px">Gets the VP info from the database as a compressed file, either zip or gzip format</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v1/ViableRoutes/ViabilityData/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postViabilityApiV2ViableRoutesViabilityDataFile(contentType, contentDisposition, headers, length, name, fileName, callback)</td>
    <td style="padding:15px">Inserts the Verification Paths received from a gzip (.gz), zip (.zip), JSON (.json) or OnePlanner (</td>
    <td style="padding:15px">{base_path}/{version}/viability/api/v2/ViableRoutes/ViabilityData/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readConfig(callback)</td>
    <td style="padding:15px">Retrieve App configuration.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfig(body, callback)</td>
    <td style="padding:15px">Update AppConfig.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importOneC(body, callback)</td>
    <td style="padding:15px">Import watchers in CSV file from  OneControl.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/import/onecontrol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readLogs(callback)</td>
    <td style="padding:15px">Read logging configuration.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeLogs(body, callback)</td>
    <td style="padding:15px">Update logging configuration.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findMeters(uuid, force, callback)</td>
    <td style="padding:15px">Retrieve available meters for a particular resource</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/meters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceAttrs(uuid, callback)</td>
    <td style="padding:15px">Retrieve all possible attributes of a resource.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/resource-attrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">soundFilenames(callback)</td>
    <td style="padding:15px">List all sound file available in the server.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/sounds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSound(filename, callback)</td>
    <td style="padding:15px">List all sound file available in the server.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/sounds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readWatchers(searchText, sort, filter, paging, facets, callback)</td>
    <td style="padding:15px">Retrieve all watchers.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWatcher(body, callback)</td>
    <td style="padding:15px">Create new Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWatcher(uuid, callback)</td>
    <td style="padding:15px">Delete Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/watchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readWatcher(uuid, callback)</td>
    <td style="padding:15px">Retrieve single Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/watchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWatcher(body, uuid, callback)</td>
    <td style="padding:15px">Update existing Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/watchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readWatcherTriggers(uuid, callback)</td>
    <td style="padding:15px">Retrieve IDs of items which triggered the Watcher.</td>
    <td style="padding:15px">{base_path}/{version}/watcher/api/v1/watchers/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForeignSystems(callback)</td>
    <td style="padding:15px">Get Foreign Systems</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createForeignSystem(name, vendorNeType, nodes, links, connections, callback)</td>
    <td style="padding:15px">Create Foreign System</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addForeignSystemsToInventory(foreignSystemName, callback)</td>
    <td style="padding:15px">Add Foreign System to Inventory</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems/add/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeForeignSystemsFromInventory(foreignSystemName, callback)</td>
    <td style="padding:15px">Remove Foreign System from Inventory</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems/remove/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForeignSystemsDetail(foreignSystemName, callback)</td>
    <td style="padding:15px">Get Foreign Systems Details</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForeignSystemsFile(foreignSystemName, deleteType, callback)</td>
    <td style="padding:15px">Delete Foreign Systems</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateForeignSystem(foreignSystemName, name, vendorNeType, nodes, links, connections, callback)</td>
    <td style="padding:15px">Edit Foreign System</td>
    <td style="padding:15px">{base_path}/{version}/foreign-systems/api/v1/foreignSystems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
