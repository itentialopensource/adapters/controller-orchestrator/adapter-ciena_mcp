
## 0.3.3 [07-27-2022]

* Change schemas to escape . so that objectization is not done

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!2

---

## 0.3.2 [06-03-2022]

* Fix Datatype on token

See merge request itentialopensource/adapters/controller-orchestrator/adapter-ciena_mcp!1

---

## 0.3.1 [05-26-2022]

* Bug fixes and performance improvements

See commit 64ba74c

---
